# Use the official image as a parent image.
FROM openjdk:14.0.1-oracle

# Set the working directory.
WORKDIR /opt/gnuob/

# Copy the file from your host to your current location.
COPY build/libs/*.jar /opt/gnuob/gnuob-spring-boot.jar

# Inform Docker that the container is listening on the specified port at runtime.
EXPOSE 8000

# Create volumes for log and database files
VOLUME ["/opt/gnuob/logs", "/opt/gnuob/db"]

# Run the specified command within the container.
CMD [ "java", "-Dspring.profiles.active=prod", "-jar", "/opt/gnuob/gnuob-spring-boot.jar" ]