#!/usr/bin/env bash
set -e

function tag_branch()
{
  # The name of our new tag
  TAG_NAME="v1.0.${CI_JOB_ID}"
  # Extract the host where the server is running, and add the URL to the APIs
  [[ $CI_PROJECT_URL =~ ^https?://[^/]+ ]] && HOST="${BASH_REMATCH[0]}/api/v4/projects/"
  # Require a tag and take a look if there is already one with the same name
  LISTTAG=`curl --silent "${HOST}${CI_PROJECT_ID}/repository/tags/${TAG_NAME}" --header "PRIVATE-TOKEN:${GITLAB_PRIVATE_TOKEN}"`;
  COUNTTAGS=`echo ${LISTTAG} | grep -o "\"name\":\"${TAG_NAME}\"" | wc -l`;
  # No tag found, let's create a new one
  if [ ${COUNTTAGS} -eq "0" ]; then
      curl -X POST "${HOST}${CI_PROJECT_ID}/repository/tags?tag_name=${TAG_NAME}&ref=${CI_COMMIT_REF_NAME}" \
          --header "PRIVATE-TOKEN:${GITLAB_PRIVATE_TOKEN}";
      echo "Creating a new tag: ${TAG_NAME}";
  else
    echo "No new tag: ${TAG_NAME} created";
  fi
}

if [ -z "$GITLAB_PRIVATE_TOKEN" ]; then
  echo "GITLAB_PRIVATE_TOKEN not set"
  echo "Please set the GitLab Private Token as GITLAB_PRIVATE_TOKEN"
  exit 1
fi

tag_branch