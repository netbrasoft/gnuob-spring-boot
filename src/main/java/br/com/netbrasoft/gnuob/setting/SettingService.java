package br.com.netbrasoft.gnuob.setting;

import static org.springframework.messaging.core.AbstractMessageSendingTemplate.CONVERSION_HINT_HEADER;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.netbrasoft.gnuob.type.AbstractTypeService;
import br.com.netbrasoft.gnuob.type.ITypeRepository;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@Service
public class SettingService<S extends Setting> extends AbstractTypeService<S> {
  @Autowired(required = true)
  protected ISettingRepository<S> settingRepository;

  @Override
  public ITypeRepository<S> getTypeRepository() {
    return settingRepository;
  }

  @Override
  public String getPath() {
    return Setting.class.getSimpleName().toLowerCase();
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "setting", condition = "#type.getId() > 0"),
      @CacheEvict(value = "settings", allEntries = true, condition = "#type.getId() > 0")
    }
  )
  //@formatter:on
  public void delete(S type) {
    super.delete(type);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "setting", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "settings", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public void deleteAll(List<S> types) {
    super.deleteAll(types);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "settings", allEntries = true, condition = "#type.getId() > 0"),
    },
    put = {
      @CachePut(value = "setting", condition = "#type.getId() > 0"),
    }
  )
  //@formatter:on
  public S save(S type) {
    return super.save(type);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "setting", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "settings", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public List<S> saveAll(List<S> types) {
    return super.saveAll(types);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "setting", sync = true, condition = "#type.getId() > 0")
  //@formatter:on
  public S findOne(S type) {
    return super.findOne(type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "settings", sync = true)
  //@formatter:on
  public Page<S> findAll(int page, int size, Direction direction, List<String> properties, S type) {
    return super.findAll(page, size, direction, properties, type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "settings", sync = true)
  //@formatter:on
  public Page<S> findAll(int page, int size, Direction direction, List<String> properties, TypeFilter<S> filter) {
    return super.findAll(page, size, direction, properties, filter);
  }

  @Override
  public Map<String, Object> getHeaders() {
    final Map<String, Object> headers = super.getHeaders();
    headers.replace(CONVERSION_HINT_HEADER, SettingView.Transfer.class);
    return headers;
  }
}
