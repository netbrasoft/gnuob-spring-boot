package br.com.netbrasoft.gnuob.setting;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.netbrasoft.gnuob.type.IController;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@RestController
@CrossOrigin(maxAge = 3600)
public class SettingController<S extends Setting> implements IController<S> {
  @Autowired(required = true)
  private ITypeService<S> settingService;

  @Override
  public ITypeService<S> getService() {
    return settingService;
  }

  @Override
  @DeleteMapping(value = "/setting")
  public void delete(@RequestBody final S type) {
    IController.super.delete(type);
  }

  @Override
  @DeleteMapping(value = "/setting/all")
  public void deleteAll(@RequestBody final List<S> types) {
    IController.super.deleteAll(types);
  }

  @Override
  @PostMapping(value = "/setting")
  @JsonView({SettingView.Transfer.class})
  public S save(@RequestBody final S type) {
    return IController.super.save(type);
  }

  @Override
  @PostMapping(value = "/setting/all")
  @JsonView({SettingView.Transfer.class})
  public List<S> saveAll(@RequestBody final List<S> types) {
    return IController.super.saveAll(types);
  }

  @Override
  @PostMapping(value = "/setting/of/one")
  @JsonView({SettingView.Transfer.class})
  public S findOne(@RequestBody final S type) {
    return IController.super.findOne(type);
  }

  @Override
  @JsonView({SettingView.Transfer.class})
  @PostMapping(value = "/setting/of/all")
  public List<S> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final S type, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, type, response);
  }

  @Override
  @JsonView({SettingView.Transfer.class})
  @PostMapping(value = "/setting/of/all/filter")
  public List<S> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final TypeFilter<S> filter, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, filter, response);
  }
}
