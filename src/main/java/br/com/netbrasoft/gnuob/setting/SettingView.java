package br.com.netbrasoft.gnuob.setting;

import br.com.netbrasoft.gnuob.type.TypeView;

public interface SettingView extends TypeView {
  public interface Transfer extends TypeView.Transfer {
  }
}
