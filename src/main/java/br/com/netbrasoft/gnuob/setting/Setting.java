package br.com.netbrasoft.gnuob.setting;

import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.type.AbstractActiveType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "code")})
public class Setting extends AbstractActiveType {
  private static final long serialVersionUID = -3797283401579047534L;
  protected static final String[] SETTING_EXCLUDE_FIELDS = addAll(TYPE_ACTIVE_EXCLUDE_FIELDS, "value", "type");
  private String value;
  private Type type;

  public enum Type {
    BOOLEAN, DATE, DOUBLE, EMAIL, INTEGER, MENU, STRING, URL
  }

  public static Setting getInstance() {
    return new Setting();
  }

  public static Setting getInstance(final String code) {
    final Setting setting = getInstance();
    setting.setCode(code);
    return setting;
  }

  public static Setting getInstance(final String code, final Boolean active) {
    final Setting setting = getInstance(code);
    setting.setActive(active);
    return setting;
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, SETTING_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, SETTING_EXCLUDE_FIELDS);
  }

  @Column
  @Size(max = 255)
  @JsonView({SettingView.Transfer.class})
  public String getValue() {
    return value;
  }

  @Column
  @Enumerated(EnumType.STRING)
  @JsonView({SettingView.Transfer.class})
  public Type getType() {
    return type;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, SETTING_EXCLUDE_FIELDS);
  }
}
