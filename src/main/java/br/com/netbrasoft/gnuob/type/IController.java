package br.com.netbrasoft.gnuob.type;

import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;

import br.com.netbrasoft.gnuob.GnuobReloadableResourceBundleMessageSource;

public interface IController<T extends AbstractType> {

  default void delete(final T type) {
    try {
      getLogger(getClass()).info("request [{}]", type);
      getService().delete(type);
    } catch (Exception e) {
      getLogger(getClass()).warn(e.getMessage(), e);
      throw new TypeRuntimeException(identifyExceptionAndGetMessage((ConstraintViolationException) e.getCause()), e);
    } finally {
      getLogger(getClass()).info("response [void]");
    }
  }

  default void deleteAll(final List<T> types) {
    try {
      getLogger(getClass()).info("request [{}]", types);
      getService().deleteAll(types);
    } catch (Exception e) {
      getLogger(getClass()).warn(e.getMessage(), e);
      throw new TypeRuntimeException(identifyExceptionAndGetMessage((ConstraintViolationException) e.getCause()), e);
    } finally {
      getLogger(getClass()).info("response [void]");
    }
  }

  default T save(final T type) {
    T response = null;
    try {
      getLogger(getClass()).info("request [{}]", type);
      response = getService().save(type);
      return response;
    } catch (Exception e) {
      getLogger(getClass()).warn(e.getMessage(), e);
      throw new TypeRuntimeException(identifyExceptionAndGetMessage((ConstraintViolationException) e.getCause()), e);
    } finally {
      getLogger(getClass()).info("response [{}]", response);
    }
  }

  default List<T> saveAll(final List<T> types) {
    List<T> response = null;
    try {
      getLogger(getClass()).info("request [{}]", types);
      response = getService().saveAll(types);
      return response;
    } catch (Exception e) {
      getLogger(getClass()).warn(e.getMessage(), e);
      throw new TypeRuntimeException(identifyExceptionAndGetMessage((ConstraintViolationException) e.getCause()), e);
    } finally {
      getLogger(getClass()).info("response [{}]", response);
    }
  }

  default T findOne(final T type) {
    T response = null;
    try {
      getLogger(getClass()).info("request [{}]", type);
      response = getService().findOne(type);
      return response;
    } catch (Exception e) {
      getLogger(getClass()).warn(e.getMessage(), e);
      throw new TypeRuntimeException(identifyExceptionAndGetMessage((ConstraintViolationException) e.getCause()), e);
    } finally {
      getLogger(getClass()).info("response [{}]", response);
    }
  }

  default List<T> findAll(final int page, final int size, final Direction direction, final List<String> properties, final T type, final HttpServletResponse httpServletResponse) {
    List<T> response = null;
    try {
      getLogger(getClass()).info("page [{}] size [{}] direction [{}] properties [{}] request [{}]", page, size, direction, properties, type);
      final Page<T> typePage = getService().findAll(page, size, direction, properties, type);
      httpServletResponse.addHeader("X-Total-Count", String.valueOf(typePage.getTotalElements()));
      response = typePage.getContent();
      return response;
    } catch (Exception e) {
      getLogger(getClass()).warn(e.getMessage(), e);
      throw new TypeRuntimeException(identifyExceptionAndGetMessage((ConstraintViolationException) e.getCause()), e);
    } finally {
      getLogger(getClass()).info("response [{}]", response);
    }
  }

  default List<T> findAll(final int page, final int size, final Direction direction, final List<String> properties, final TypeFilter<T> filter, final HttpServletResponse httpServletResponse) {
    List<T> response = null;
    try {
      getLogger(getClass()).info("page [{}] size [{}] direction [{}] properties [{}] request [{}]", page, size, direction, properties, filter);
      final Page<T> typePage = getService().findAll(page, size, direction, properties, filter);
      httpServletResponse.addHeader("X-Total-Count", String.valueOf(typePage.getTotalElements()));
      response = typePage.getContent();
      return response;
    } catch (Exception e) {
      getLogger(getClass()).warn(e.getMessage(), e);
      throw new TypeRuntimeException(identifyExceptionAndGetMessage((ConstraintViolationException) e.getCause()), e);
    } finally {
      getLogger(getClass()).info("response [{}]", response);
    }
  }

  private String identifyExceptionAndGetMessage(final ConstraintViolationException constraintViolationException) {
    final GnuobReloadableResourceBundleMessageSource messageSource = new GnuobReloadableResourceBundleMessageSource();
    messageSource.setBasename("classpath:messages");
    messageSource.setDefaultEncoding("UTF-8");
    final Optional<Object> optionalKey = messageSource.getKeySet().stream().filter(key -> constraintViolationException.getConstraintName().contains(key.toString().replace(".", "_").toUpperCase())).findFirst();
    if (optionalKey.isPresent()) {
      return messageSource.getMessage(optionalKey.get().toString(), new Object[0], getLocale());
    }
    return constraintViolationException.getConstraintName();
  }

  ITypeService<T> getService();
}
