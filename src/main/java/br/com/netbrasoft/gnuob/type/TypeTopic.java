package br.com.netbrasoft.gnuob.type;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

import lombok.Setter;

@Setter
public class TypeTopic<T extends AbstractType> {
  public enum Action {
    DELETE, DELETE_ALL, SAVE, SAVE_ALL, NONE
  }

  private Action action = Action.NONE;
  private T type;
  private List<T> types;

  public TypeTopic() {
    setAction(Action.NONE);
  }

  public TypeTopic(final Action action, final T type) {
    setAction(action);
    setType(type);
  }

  public TypeTopic(final Action action, final List<T> types) {
    setAction(action);
    setTypes(types);
  }

  @JsonView(TypeView.Transfer.class)
  public Action getAction() {
    return action;
  }

  @JsonView(TypeView.Transfer.class)
  public T getType() {
    return type;
  }

  @JsonView(TypeView.Transfer.class)
  public List<T> getTypes() {
    return types;
  }
}
