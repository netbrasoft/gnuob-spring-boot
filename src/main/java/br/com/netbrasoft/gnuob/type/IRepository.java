package br.com.netbrasoft.gnuob.type;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IRepository<T extends AbstractType, I extends Serializable> extends JpaRepository<T, I>, JpaSpecificationExecutor<T> {

  <S extends T> List<S> saveAndFlushAll(Iterable<S> entities);

  T refresh(T type);

  List<T> refreshAll(List<T> types);
}
