package br.com.netbrasoft.gnuob.type;

import static br.com.netbrasoft.gnuob.Utils.getUsername;
import static java.lang.System.currentTimeMillis;
import static javax.persistence.GenerationType.IDENTITY;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Setter;

@Setter
@MappedSuperclass
public abstract class AbstractType implements IType {
  private static final long serialVersionUID = 2743591570784179362L;
  protected static final String[] TYPE_EXCLUDE_FIELDS = {"id", "version", "dtype", "created", "modified", "userCreated", "userModified"};
  private long id;
  private Integer version;
  private String dtype;
  private Timestamp created;
  private Timestamp modified;
  private String userCreated;
  private String userModified;

  @Override
  public int hashCode() {
    return reflectionHashCode(this, TYPE_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, TYPE_EXCLUDE_FIELDS);
  }

  @PrePersist
  private void prePersistType() {
    setDtype(getClass().getSimpleName().toLowerCase());
    setCreated(new Timestamp(currentTimeMillis()));
    setModified(new Timestamp(currentTimeMillis()));
    setUserCreated(getUsername());
    setUserModified(getUsername());
    prePersist();
  }

  @PreUpdate
  private void preUpdateType() {
    setDtype(getClass().getSimpleName().toLowerCase());
    setModified(new Timestamp(currentTimeMillis()));
    setUserModified(getUsername());
    preUpdate();
  }

  @Column(unique = true, nullable = false, updatable = false)
  @GeneratedValue(strategy = IDENTITY)
  @Id
  @JsonView(TypeView.Transfer.class)
  public long getId() {
    return id;
  }

  @Column(nullable = false)
  @Version
  @JsonView(TypeView.Transfer.class)
  public Integer getVersion() {
    return version;
  }

  @Column(updatable = false)
  @JsonView(TypeView.Transfer.class)
  public String getDtype() {
    return dtype;
  }

  @Column(updatable = false)
  @JsonIgnore
  public Timestamp getCreated() {
    return created;
  }

  @JsonIgnore
  public Timestamp getModified() {
    return modified;
  }

  @Column(updatable = false)
  @JsonIgnore
  public String getUserCreated() {
    return userCreated;
  }

  @Column
  @JsonIgnore
  public String getUserModified() {
    return userModified;
  }

  @JsonIgnore
  @Transient
  public boolean flagDetached() {
    return id > 0;
  }

  @JsonIgnore
  @Transient
  public boolean flagNotDetached() {
    return !flagDetached();
  }
}
