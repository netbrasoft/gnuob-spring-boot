package br.com.netbrasoft.gnuob.type;

import static br.com.netbrasoft.gnuob.type.AbstractType.TYPE_EXCLUDE_FIELDS;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static javax.validation.Validation.buildDefaultValidatorFactory;
import static org.apache.commons.lang3.StringUtils.join;
import static org.springframework.data.domain.Example.of;
import static org.springframework.data.domain.ExampleMatcher.matchingAny;
import static org.springframework.data.domain.ExampleMatcher.StringMatcher.CONTAINING;
import static org.springframework.data.jpa.convert.QueryByExamplePredicateBuilder.getPredicate;
import static org.springframework.data.jpa.domain.Specification.where;
import static org.springframework.messaging.core.AbstractMessageSendingTemplate.CONVERSION_HINT_HEADER;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;

public interface ITypeService<T extends AbstractType> {

  default void refreshReference(final T type) {}

  default T verifySave(final T type) {
    refreshReference(type);
    return type;
  }

  default T validate(final T type) {
    final Set<String> violationMessages = newHashSet();
    buildDefaultValidatorFactory().getValidator().validate(type).forEach(cv -> violationMessages.add(cv.getPropertyPath() + ": " + cv.getMessage()));
    if (!violationMessages.isEmpty()) {
      throw new TypeRuntimeException(join(violationMessages, "\n"));
    }
    return type;
  }

  default List<T> verifySaveAll(final List<T> types) {
    types.forEach(this::refreshReference);
    return types;
  }

  default List<T> validateAll(final List<T> types) {
    final Set<String> violationMessages = newHashSet();
    types.forEach(type -> {
      buildDefaultValidatorFactory().getValidator().validate(type).forEach(cv -> violationMessages.add(cv.getPropertyPath() + ": " + cv.getMessage()));
    });
    if (!violationMessages.isEmpty()) {
      throw new TypeRuntimeException(join(violationMessages, "\n"));
    }
    return types;
  }

  default T verifyDelete(final T type) {
    refreshReference(type);
    return type;
  }

  default List<T> verifyDeleteAll(final List<T> types) {
    types.forEach(this::refreshReference);
    return types;
  }

  default Specification<T> specification(final TypeFilter<T> filter) {
    return where(specificationExample(of(filter.getType(), matchingAny().withIgnorePaths(getIgnorePats()).withIgnoreCase().withStringMatcher(CONTAINING))).and(creationDateBetween(filter.getFrom(), filter.getTo())));
  }

  default String[] getIgnorePats() {
    return TYPE_EXCLUDE_FIELDS;
  }

  default Specification<T> creationDateBetween(final Date from, final Date to) {
    return (root, query, cb) -> cb.and(cb.isNotNull(root.get("created")), cb.between(root.get("created"), from, to));
  }

  default Specification<T> specificationExample(final Example<T> example) {
    return (root, query, cb) -> getPredicate(root, cb, example);
  }

  default Map<String, Object> getHeaders() {
    final Map<String, Object> headers = newHashMap();
    headers.put(CONVERSION_HINT_HEADER, TypeView.Transfer.class);
    return headers;
  }

  ITypeRepository<T> getTypeRepository();

  String getPath();

  void delete(final T type);

  void deleteAll(final List<T> types);

  T save(final T type);

  List<T> saveAll(final List<T> types);

  T findOne(final T type);

  Page<T> findAll(final int page, final int size, final Direction direction, final List<String> properties, final T type);

  Page<T> findAll(final int page, final int size, final Direction direction, final List<String> properties, final TypeFilter<T> filter);
}
