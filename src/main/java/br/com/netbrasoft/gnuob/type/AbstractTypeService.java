package br.com.netbrasoft.gnuob.type;

import static br.com.netbrasoft.gnuob.ApplicationConstants.NO_RESULTS_FOUND;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_TOPIC;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.DELETE;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.DELETE_ALL;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.SAVE;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.SAVE_ALL;
import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.ObjectUtils.allNotNull;
import static org.apache.commons.lang3.StringUtils.join;
import static org.springframework.data.domain.Example.of;
import static org.springframework.data.domain.ExampleMatcher.matching;
import static org.springframework.data.domain.ExampleMatcher.matchingAny;
import static org.springframework.data.domain.ExampleMatcher.StringMatcher.CONTAINING;
import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.data.domain.Sort.by;
import static org.springframework.data.domain.Sort.Direction.ASC;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.messaging.simp.SimpMessagingTemplate;

public abstract class AbstractTypeService<T extends AbstractType> implements ITypeService<T> {
  @Autowired(required = true)
  private SimpMessagingTemplate simpMessagingTemplate;

  public void delete(final T type) {
    getTypeRepository().deleteAll(getTypeRepository().findAllById(getIds(newArrayList(verifyDelete(type)))));
    simpMessagingTemplate.convertAndSend(join(PATH_TOPIC, PATH, getPath()), new TypeTopic<T>(DELETE, type), getHeaders());
  }

  @Override
  public void deleteAll(final List<T> types) {
    getTypeRepository().deleteAll(getTypeRepository().findAllById(getIds(verifyDeleteAll(types))));
    simpMessagingTemplate.convertAndSend(join(PATH_TOPIC, PATH, getPath()), new TypeTopic<T>(DELETE_ALL, types), getHeaders());
  }

  private List<Long> getIds(final List<T> types) {
    return newArrayList(types.stream().map(AbstractType::getId).iterator());
  }

  @Override
  public T save(final T type) {
    final T attachedType = getTypeRepository().refresh(getTypeRepository().saveAndFlush(validate(verifySave(type))));
    simpMessagingTemplate.convertAndSend(join(PATH_TOPIC, PATH, getPath()), new TypeTopic<T>(SAVE, attachedType), getHeaders());
    return attachedType;
  }

  @Override
  public List<T> saveAll(final List<T> types) {
    final List<T> attachedTypes = getTypeRepository().refreshAll(getTypeRepository().saveAndFlushAll(validateAll(verifySaveAll(types))));
    simpMessagingTemplate.convertAndSend(join(PATH_TOPIC, PATH, getPath()), new TypeTopic<T>(SAVE_ALL, attachedTypes), getHeaders());
    return attachedTypes;
  }

  @Override
  public T findOne(final T type) {
    return findByIdOrOne(type);
  }

  private T findByIdOrOne(final T type) {
    final Optional<T> optional = getTypeRepository().findById(type.getId());
    return optional.isPresent() ? optional.get() : getTypeRepository().findOne(of(type, matching().withIgnorePaths(getIgnorePats()))).orElseThrow(() -> new TypeRuntimeException(NO_RESULTS_FOUND));
  }

  @Override
  public Page<T> findAll(final int page, final int size, final Direction direction, final List<String> properties, final T type) {
    return getTypeRepository().findAll(of(type, matchingAny().withIgnorePaths(getIgnorePats()).withIgnoreCase().withStringMatcher(CONTAINING)), of(page, size, by(allNotNull(direction) ? direction : ASC, properties.toArray(new String[properties.size()]))));
  }

  @Override
  public Page<T> findAll(final int page, final int size, final Direction direction, final List<String> properties, final TypeFilter<T> filter) {
    return getTypeRepository().findAll(specification(filter), of(page, size, by(allNotNull(direction) ? direction : ASC, properties.toArray(new String[properties.size()]))));
  }
}
