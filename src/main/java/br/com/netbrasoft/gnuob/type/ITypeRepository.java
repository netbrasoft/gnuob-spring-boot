package br.com.netbrasoft.gnuob.type;

import org.springframework.stereotype.Repository;

@Repository
public interface ITypeRepository<T extends AbstractType> extends IRepository<T, Long> {
}
