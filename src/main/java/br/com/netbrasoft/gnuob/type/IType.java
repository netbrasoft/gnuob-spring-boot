package br.com.netbrasoft.gnuob.type;

import java.io.Serializable;

import javax.persistence.Transient;

public interface IType extends Serializable {
  @Transient
  public default void prePersist() {}

  @Transient
  public default void preUpdate() {}

  long getId();
}
