package br.com.netbrasoft.gnuob.type;

import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.BooleanUtils.isNotFalse;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Setter;

@Setter
@MappedSuperclass
public abstract class AbstractActiveType extends AbstractType {
  protected static final long serialVersionUID = -9006579740581350346L;
  protected static final String[] TYPE_ACTIVE_EXCLUDE_FIELDS = addAll(TYPE_EXCLUDE_FIELDS, "checked");
  private Boolean checked;
  private Boolean active;
  private String code;
  private String name;
  private String description;

  @Override
  public int hashCode() {
    return reflectionHashCode(this, TYPE_ACTIVE_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, TYPE_ACTIVE_EXCLUDE_FIELDS);
  }

  @Override
  public void prePersist() {
    setActive(isNotFalse(getActive()));
    super.prePersist();
  }

  @Override
  public void preUpdate() {
    setActive(isNotFalse(getActive()));
    super.preUpdate();
  }

  @JsonIgnore
  @Transient
  public Boolean getChecked() {
    return checked;
  }

  @Column
  @JsonView(TypeView.Transfer.class)
  public Boolean getActive() {
    return active;
  }

  @Column(nullable = true)
  @Size(max = 255)
  @JsonView(TypeView.Transfer.class)
  public String getCode() {
    return code;
  }

  @Column
  @Size(max = 125)
  @JsonView(TypeView.Transfer.class)
  public String getName() {
    return name;
  }

  @Column
  @Size(max = 1024)
  @JsonView(TypeView.Transfer.class)
  public String getDescription() {
    return description;
  }
}
