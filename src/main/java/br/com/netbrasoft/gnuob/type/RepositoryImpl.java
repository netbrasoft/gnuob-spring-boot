package br.com.netbrasoft.gnuob.type;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import static com.google.common.collect.Lists.*;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;
import static org.springframework.util.Assert.*;

public class RepositoryImpl<T extends AbstractType, I extends Serializable> extends SimpleJpaRepository<T, I> implements IRepository<T, I> {
  private final EntityManager entityManager;

  public RepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
    super(entityInformation, entityManager);
    this.entityManager = entityManager;
  }

  @Override
  public <S extends T> List<S> saveAndFlushAll(Iterable<S> entities) {
    notNull(entities, "Entities must not be null!");
    List<S> result = newArrayList(saveAll(entities));
    flush();
    return result;
  }

  @Override
  @Transactional
  public T refresh(T type) {
    type = (T) entityManager.find(type.getClass(), type.getId());
    entityManager.refresh(type);
    return type;
  }

  @Override
  @Transactional
  public List<T> refreshAll(List<T> types) {
    types.forEach(t -> t = refresh(t));
    return types;
  }
}
