package br.com.netbrasoft.gnuob.type;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TypeRuntimeException extends RuntimeException {
  private static final long serialVersionUID = 3255468203736649364L;

  public TypeRuntimeException() {}

  public TypeRuntimeException(final String message) {
    super(message);
  }

  public TypeRuntimeException(final String message, final Exception e) {
    super(message, e);
  }

  @Override
  @JsonIgnore
  public StackTraceElement[] getStackTrace() {
    return super.getStackTrace();
  }

  @Override
  @JsonIgnore
  public synchronized Throwable getCause() {
    return super.getCause();
  }
}
