package br.com.netbrasoft.gnuob.category;

import br.com.netbrasoft.gnuob.type.TypeView;

public interface CategoryView extends TypeView {
  public interface Transfer extends TypeView.Transfer {
  }
}
