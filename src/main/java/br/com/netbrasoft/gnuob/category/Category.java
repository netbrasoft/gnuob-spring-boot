package br.com.netbrasoft.gnuob.category;

import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.ObjectUtils.allNotNull;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.document.DocumentView;
import br.com.netbrasoft.gnuob.type.AbstractActiveType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "code")})
public class Category extends AbstractActiveType {
  private static final long serialVersionUID = -8647587629878455322L;
  protected static final String[] CATEGORY_EXCLUDE_FIELDS = addAll(TYPE_ACTIVE_EXCLUDE_FIELDS, "parent");
  private Integer position;
  private Category parent;

  public static Category getInstance() {
    return new Category();
  }

  @Override
  public void prePersist() {
    setCode((allNotNull(getParent()) ? join(getParent().getCode(), " -> ", getName()) : getName()));
    super.prePersist();
  }

  @Override
  public void preUpdate() {
    setCode((allNotNull(getParent()) ? join(getParent().getCode(), " -> ", getName()) : getName()));
    super.preUpdate();
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, CATEGORY_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, CATEGORY_EXCLUDE_FIELDS);
  }

  @Column
  @JsonView({CategoryView.Transfer.class, DocumentView.Transfer.class})
  public Integer getPosition() {
    return position;
  }

  @Cache(usage = READ_WRITE)
  @ManyToOne(fetch = EAGER)
  @JsonView({CategoryView.Transfer.class, DocumentView.Transfer.class})
  public Category getParent() {
    return parent;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, CATEGORY_EXCLUDE_FIELDS);
  }
}
