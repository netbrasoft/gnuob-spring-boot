package br.com.netbrasoft.gnuob.category;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.netbrasoft.gnuob.type.IController;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@RestController
@CrossOrigin(maxAge = 3600)
public class CategoryController<C extends Category> implements IController<C> {
  @Autowired(required = true)
  private ITypeService<C> categoryService;

  @Override
  public ITypeService<C> getService() {
    return categoryService;
  }

  @Override
  @DeleteMapping(value = "/category")
  public void delete(@RequestBody final C type) {
    IController.super.delete(type);
  }

  @Override
  @DeleteMapping(value = "/category/all")
  public void deleteAll(@RequestBody final List<C> types) {
    IController.super.deleteAll(types);
  }

  @Override
  @PostMapping(value = "/category")
  @JsonView({CategoryView.Transfer.class})
  public C save(@RequestBody final C type) {
    return IController.super.save(type);
  }

  @Override
  @PostMapping(value = "/category/all")
  @JsonView({CategoryView.Transfer.class})
  public List<C> saveAll(@RequestBody final List<C> types) {
    return IController.super.saveAll(types);
  }

  @Override
  @PostMapping(value = "/category/of/one")
  @JsonView({CategoryView.Transfer.class})
  public C findOne(@RequestBody final C type) {
    return IController.super.findOne(type);
  }

  @Override
  @JsonView({CategoryView.Transfer.class})
  @PostMapping(value = "/category/of/all")
  public List<C> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final C type, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, type, response);
  }

  @Override
  @JsonView({CategoryView.Transfer.class})
  @PostMapping(value = "/category/of/all/filter")
  public List<C> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final TypeFilter<C> filter, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, filter, response);
  }
}
