package br.com.netbrasoft.gnuob.category;

import static org.springframework.messaging.core.AbstractMessageSendingTemplate.CONVERSION_HINT_HEADER;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.netbrasoft.gnuob.type.AbstractTypeService;
import br.com.netbrasoft.gnuob.type.ITypeRepository;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@Service
public class CategoryService<C extends Category> extends AbstractTypeService<C> {
  @Autowired(required = true)
  protected ICategoryRepository<C> categoryRepository;

  @Override
  public ITypeRepository<C> getTypeRepository() {
    return categoryRepository;
  }

  @Override
  public String getPath() {
    return Category.class.getSimpleName().toLowerCase();
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "category", condition = "#type.getId() > 0"),
      @CacheEvict(value = "categories", allEntries = true, condition = "#type.getId() > 0")
    }
  )
  //@formatter:on
  public void delete(C type) {
    super.delete(type);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "category", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "categories", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public void deleteAll(List<C> types) {
    super.deleteAll(types);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "categories", allEntries = true, condition = "#type.getId() > 0"),
    },
    put = {
      @CachePut(value = "category", condition = "#type.getId() > 0"),
    }
  )
  //@formatter:on
  public C save(C type) {
    return super.save(type);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "category", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "categories", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public List<C> saveAll(List<C> types) {
    return super.saveAll(types);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "category", sync = true, condition = "#type.getId() > 0")
  //@formatter:on
  public C findOne(C type) {
    return super.findOne(type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "categories", sync = true)
  //@formatter:on
  public Page<C> findAll(int page, int size, Direction direction, List<String> properties, C type) {
    return super.findAll(page, size, direction, properties, type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "categories", sync = true)
  //@formatter:on
  public Page<C> findAll(int page, int size, Direction direction, List<String> properties, TypeFilter<C> filter) {
    return super.findAll(page, size, direction, properties, filter);
  }

  @Override
  public Map<String, Object> getHeaders() {
    final Map<String, Object> headers = super.getHeaders();
    headers.replace(CONVERSION_HINT_HEADER, CategoryView.Transfer.class);
    return headers;
  }
}
