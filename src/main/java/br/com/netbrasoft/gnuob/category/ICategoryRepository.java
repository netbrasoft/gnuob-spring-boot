package br.com.netbrasoft.gnuob.category;

import br.com.netbrasoft.gnuob.type.ITypeRepository;

public interface ICategoryRepository<C extends Category> extends ITypeRepository<C> {
}
