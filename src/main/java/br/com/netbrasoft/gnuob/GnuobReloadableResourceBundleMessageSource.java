package br.com.netbrasoft.gnuob;

import static com.google.common.collect.Sets.newHashSet;
import static org.apache.commons.lang3.ObjectUtils.allNotNull;
import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

import java.util.Set;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

public class GnuobReloadableResourceBundleMessageSource extends ReloadableResourceBundleMessageSource {
  public Set<Object> getKeySet() {
    final Set<Object> keySet = newHashSet();
    getBasenameSet().forEach(bs -> calculateAllFilenames(bs, getLocale()).forEach(f -> {
      final PropertiesHolder propertiesHolder = getProperties(f);
      if (allNotNull(propertiesHolder.getProperties())) {
        keySet.addAll(propertiesHolder.getProperties().keySet());
      }
    }));
    return keySet;
  }
}
