package br.com.netbrasoft.gnuob.authentication;

import br.com.netbrasoft.gnuob.type.ITypeRepository;

public interface IPermissionRepository<P extends Permission> extends ITypeRepository<P> {
}
