package br.com.netbrasoft.gnuob.authentication;

import static com.google.common.collect.Maps.newHashMap;
import static java.util.Locale.getDefault;
import static org.apache.commons.lang3.ObjectUtils.allNotNull;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.HashMap;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import br.com.netbrasoft.gnuob.listeners.SendEmailListenerController.Email;
import br.com.netbrasoft.gnuob.setting.Setting;
import br.com.netbrasoft.gnuob.type.ITypeService;

@Aspect
@Component
public class UserServiceInterceptorController<U extends User> {
  private static final Logger LOGGER = getLogger(UserServiceInterceptorController.class);
  @Autowired(required = true)
  private JmsTemplate jmsTemplate;
  @Autowired(required = true)
  private TemplateEngine templateEngine;
  @Autowired(required = true)
  private ITypeService<Setting> settingService;

  @AfterReturning(pointcut = "execution(* br.com.netbrasoft.gnuob.authentication.UserService.register(..))", returning = "type")
  public void register(final Object type) {
    if (allNotNull(type)) {
      sendEmail((U) type, "ConfirmRegister", "Confirm Register");
    }
  }

  private void sendEmail(final U type, final String template, final String subject) {
    try {
      final HashMap<String, Object> context = newHashMap();
      context.put("user", type);
      context.put("setting", settingService.findOne(Setting.getInstance("DEFAULT_FRONTEND_URL", true)));
      jmsTemplate.convertAndSend("emailQueue", Email.getInstance(settingService.findOne(Setting.getInstance("DEFAULT_REPLY_EMAIL", true)).getValue(), type.getEmail(), subject, templateEngine.process(template, new Context(getDefault(), context))));
    } catch (final Exception e) {
      LOGGER.warn(e.getMessage(), e);
    }
  }

  @AfterReturning(pointcut = "execution(* br.com.netbrasoft.gnuob.authentication.UserService.forgotPassword(..))", returning = "type")
  public void forgotPassword(final Object type) {
    if (allNotNull(type)) {
      sendEmail((U) type, "ForgotPassword", "Forgot Password");
    }
  }
}
