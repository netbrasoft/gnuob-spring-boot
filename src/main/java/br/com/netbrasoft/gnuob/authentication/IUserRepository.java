package br.com.netbrasoft.gnuob.authentication;

import br.com.netbrasoft.gnuob.type.ITypeRepository;

public interface IUserRepository<U extends User> extends ITypeRepository<U> {
}
