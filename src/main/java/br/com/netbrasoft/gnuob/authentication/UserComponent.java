package br.com.netbrasoft.gnuob.authentication;

import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.BooleanUtils.isNotFalse;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.type.AbstractType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
public class UserComponent extends AbstractType {
  private static final long serialVersionUID = -8118795547339658032L;
  protected static final String[] USER_COMPONENT_EXCLUDE_FIELDS = addAll(TYPE_EXCLUDE_FIELDS, "canEdit", "isRequired");
  private Boolean canEdit;
  private Boolean isRequired;
  private Component component;

  public static UserComponent getInstance(final Component component) {
    final UserComponent userComponent = new UserComponent();
    userComponent.setComponent(component);
    return userComponent;
  }

  @Override
  public void prePersist() {
    setCanEdit(isNotFalse(getCanEdit()));
    setIsRequired(isNotFalse(getIsRequired()));
    super.prePersist();
  }

  @Override
  public void preUpdate() {
    setCanEdit(isNotFalse(getCanEdit()));
    setIsRequired(isNotFalse(getIsRequired()));
    super.preUpdate();
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, USER_COMPONENT_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, USER_COMPONENT_EXCLUDE_FIELDS);
  }

  @Column
  @JsonView({RoleView.Transfer.class})
  public Boolean getCanEdit() {
    return canEdit;
  }

  @Column
  @JsonView({RoleView.Transfer.class})
  public Boolean getIsRequired() {
    return isRequired;
  }

  @ManyToOne(fetch = EAGER)
  @JsonView({RoleView.Transfer.class})
  public Component getComponent() {
    return component;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, USER_COMPONENT_EXCLUDE_FIELDS);
  }
}
