package br.com.netbrasoft.gnuob.authentication;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.netbrasoft.gnuob.type.IController;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@RestController
@CrossOrigin(maxAge = 3600)
public class RoleController<R extends Role> implements IController<R> {
  @Autowired(required = true)
  private ITypeService<R> roleService;

  @Override
  public ITypeService<R> getService() {
    return roleService;
  }

  @Override
  @DeleteMapping(value = "/role")
  public void delete(@RequestBody final R type) {
    IController.super.delete(type);
  }

  @Override
  @DeleteMapping(value = "/role/all")
  public void deleteAll(@RequestBody final List<R> types) {
    IController.super.deleteAll(types);
  }

  @Override
  @PostMapping(value = "/role")
  @JsonView({RoleView.Transfer.class})
  public R save(@RequestBody final R type) {
    return IController.super.save(type);
  }

  @Override
  @PostMapping(value = "/role/all")
  @JsonView({RoleView.Transfer.class})
  public List<R> saveAll(@RequestBody final List<R> types) {
    return IController.super.saveAll(types);
  }

  @Override
  @PostMapping(value = "/role/of/one")
  @JsonView({RoleView.Transfer.class})
  public R findOne(@RequestBody final R type) {
    return IController.super.findOne(type);
  }

  @Override
  @JsonView({RoleView.Transfer.class})
  @PostMapping(value = "/role/of/all")
  public List<R> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final R type, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, type, response);
  }

  @Override
  @JsonView({RoleView.Transfer.class})
  @PostMapping(value = "/role/of/all/filter")
  public List<R> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final TypeFilter<R> filter, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, filter, response);
  }
}

