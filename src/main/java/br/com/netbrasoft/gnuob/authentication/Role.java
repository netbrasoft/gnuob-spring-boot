package br.com.netbrasoft.gnuob.authentication;

import static com.google.common.collect.Sets.newHashSet;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.type.AbstractActiveType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "code")})
public class Role extends AbstractActiveType {
  private static final long serialVersionUID = -1188567202325986652L;
  protected static final String[] ROLE_EXCLUDE_FIELDS = addAll(TYPE_ACTIVE_EXCLUDE_FIELDS, "rolePermissions", "roleUsers");
  private Set<RolePermission> rolePermissions = newHashSet();
  private Set<RoleUser> roleUsers = newHashSet();

  public static Role getInstance() {
    return new Role();
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, ROLE_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, ROLE_EXCLUDE_FIELDS);
  }

  @OneToMany(fetch = EAGER, cascade = {PERSIST, MERGE, REMOVE}, orphanRemoval = true)
  @JsonView({RoleView.Transfer.class, UserView.Transfer.class})
  public Set<RolePermission> getRolePermissions() {
    return rolePermissions;
  }

  @OneToMany(fetch = EAGER, cascade = {PERSIST, MERGE, REMOVE}, orphanRemoval = true)
  @JsonView(RoleView.Transfer.class)
  public Set<RoleUser> getRoleUsers() {
    return roleUsers;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, ROLE_EXCLUDE_FIELDS);
  }
}
