package br.com.netbrasoft.gnuob.authentication;

import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;
import org.springframework.security.core.GrantedAuthority;

import br.com.netbrasoft.gnuob.type.AbstractType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
public class UserRole extends AbstractType implements GrantedAuthority {
  private static final long serialVersionUID = 1551498979212490877L;
  protected static final String[] USER_ROLE_EXCLUDE_FIELDS = addAll(TYPE_EXCLUDE_FIELDS);
  private Role role;

  public static UserRole getInstance(final Role role) {
    final UserRole userRole = new UserRole();
    userRole.setRole(role);
    return userRole;
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, USER_ROLE_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, USER_ROLE_EXCLUDE_FIELDS);
  }

  @Override
  @Transient
  public String getAuthority() {
    return join("ROLE_", getRole().getCode()).toUpperCase();
  }

  @Cache(usage = READ_WRITE)
  @ManyToOne(fetch = EAGER)
  @JsonView({UserView.Transfer.class})
  public Role getRole() {
    return role;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, USER_ROLE_EXCLUDE_FIELDS);
  }
}
