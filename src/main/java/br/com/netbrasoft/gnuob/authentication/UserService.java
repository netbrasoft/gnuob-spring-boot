package br.com.netbrasoft.gnuob.authentication;

import static br.com.netbrasoft.gnuob.ApplicationConstants.INVALID_USERNAME_OR_PASSWORD;
import static br.com.netbrasoft.gnuob.Utils.createToken;
import static br.com.netbrasoft.gnuob.authentication.PermissionUser.PERMISSION_USER_EXCLUDE_FIELDS;
import static br.com.netbrasoft.gnuob.authentication.RoleUser.ROLE_USER_EXCLUDE_FIELDS;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.partitioningBy;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.beans.BeanUtils.copyProperties;
import static org.springframework.messaging.core.AbstractMessageSendingTemplate.CONVERSION_HINT_HEADER;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.netbrasoft.gnuob.type.AbstractTypeService;
import br.com.netbrasoft.gnuob.type.ITypeRepository;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeFilter;
import br.com.netbrasoft.gnuob.type.TypeRuntimeException;

@Service
public class UserService<U extends User> extends AbstractTypeService<U> implements IUserService<U> {
  private static final Logger LOGGER = getLogger(UserService.class);
  @Autowired(required = true)
  protected IUserRepository<U> userRepository;
  @Autowired(required = true)
  private ITypeService<Permission> permissionService;
  @Autowired(required = true)
  private ITypeService<Role> roleService;

  @Override
  public String getPath() {
    return User.class.getSimpleName().toLowerCase();
  }

  @Override
  public ITypeRepository<U> getTypeRepository() {
    return userRepository;
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "user", condition = "#type.getId() > 0"),
      @CacheEvict(value = "users", allEntries = true, condition = "#type.getId() > 0")
    }
  )
  //@formatter:on
  public void delete(final U type) {
    super.delete(type);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "user", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "users", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public void deleteAll(final List<U> types) {
    super.deleteAll(types);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "users", allEntries = true, condition = "#type.getId() > 0"),
    },
    put = {
      @CachePut(value = "user", condition = "#type.getId() > 0"),
    }
  )
  //@formatter:on
  public U save(final U type) {
    final List<Role> roles = newArrayList();
    final List<Permission> permissions = newArrayList();
    try {
      roles.addAll(updateRoles(newArrayList(type)));
      permissions.addAll(updatePermissions(newArrayList(type)));
      final U attachedType = super.save(restoreUserPassword(type));
      roles.forEach(r -> r.getRoleUsers().stream().filter(ru -> ru.getUser().equals(attachedType)).forEach(ru -> ru.setUser(attachedType)));
      permissions.forEach(p -> p.getPermissionUsers().stream().filter(pu -> pu.getUser().equals(attachedType)).forEach(pu -> pu.setUser(attachedType)));
      return attachedType;
    } finally {
      roleService.saveAll(roles);
      permissionService.saveAll(permissions);
    }
  }

  private List<Role> updateRoles(final List<U> types) {
    final List<Role> roles = newArrayList();
    types.stream().filter(U::flagDetached).forEach(u -> {
      final U original = findOne(u);
      roles.addAll(saveRoleUser(original, u));
      roles.addAll(deleteRoleUser(original, u));
    });
    types.stream().filter(U::flagNotDetached).forEach(p -> roles.addAll(this.saveRoleUser(p)));
    return roles;
  }

  private List<Role> saveRoleUser(final U original, final U update) {
    final List<Role> roles = newArrayList();
    update.getUserRoles().stream().collect(partitioningBy(original.getUserRoles()::contains)).get(false).forEach(save -> roles.add(addRoleUser(update, save)));
    return roles;
  }

  private Role addRoleUser(User user, UserRole userRole) {
    final RoleUser roleUser = RoleUser.getInstance(user);
    final Optional<RoleUser> optional = userRole.getRole().getRoleUsers().stream().filter(ru -> ru.getUser().equals(user)).findFirst();
    if (!optional.isPresent()) {
      copyProperties(userRole, roleUser, addAll(ROLE_USER_EXCLUDE_FIELDS));
      userRole.getRole().getRoleUsers().add(roleUser);
    } else {
      copyProperties(userRole, optional.get());
    }
    return userRole.getRole();
  }

  private List<Role> deleteRoleUser(final U original, final U update) {
    final List<Role> roles = newArrayList();
    original.getUserRoles().stream().collect(partitioningBy(update.getUserRoles()::contains)).get(false).forEach(remove -> roles.add(removeRoleUser(original, remove)));
    return roles;
  }

  private Role removeRoleUser(User user, UserRole userRole) {
    final Optional<RoleUser> optional = userRole.getRole().getRoleUsers().stream().filter(ru -> ru.getUser().equals(user)).findFirst();
    if (optional.isPresent()) {
      userRole.getRole().getRoleUsers().remove(optional.get());
    }
    return userRole.getRole();
  }

  private List<Role> saveRoleUser(final U update) {
    final List<Role> roles = newArrayList();
    update.getUserRoles().stream().forEach(ur -> roles.add(addRoleUser(update, ur)));
    return roles;
  }

  private List<Permission> updatePermissions(final List<U> types) {
    final List<Permission> permissions = newArrayList();
    types.stream().filter(U::flagDetached).forEach(r -> {
      final U original = findOne(r);
      permissions.addAll(savePermissionUser(original, r));
      permissions.addAll(deletePermissionUser(original, r));
    });
    types.stream().filter(U::flagNotDetached).forEach(p -> permissions.addAll(this.savePermissionUser(p)));
    return permissions;
  }

  private List<Permission> savePermissionUser(final U original, final U update) {
    final List<Permission> permissions = newArrayList();
    update.getUserPermissions().stream().collect(partitioningBy(original.getUserPermissions()::contains)).get(false).forEach(save -> permissions.add(addPermissionUser(update, save)));
    return permissions;
  }

  private Permission addPermissionUser(User user, UserPermission userPermission) {
    final PermissionUser permissionUser = PermissionUser.getInstance(user);
    final Optional<PermissionUser> optional = userPermission.getPermission().getPermissionUsers().stream().filter(pu -> pu.getUser().equals(user)).findFirst();
    if (!optional.isPresent()) {
      copyProperties(userPermission, permissionUser, addAll(PERMISSION_USER_EXCLUDE_FIELDS));
      userPermission.getPermission().getPermissionUsers().add(permissionUser);
    } else {
      copyProperties(userPermission, optional.get());
    }
    return userPermission.getPermission();
  }

  private List<Permission> deletePermissionUser(final U original, final U update) {
    final List<Permission> permissions = newArrayList();
    original.getUserPermissions().stream().collect(partitioningBy(update.getUserPermissions()::contains)).get(false).forEach(remove -> permissions.add(removePermissionUser(original, remove)));
    return permissions;
  }

  private Permission removePermissionUser(User user, UserPermission userPermission) {
    final Optional<PermissionUser> optional = userPermission.getPermission().getPermissionUsers().stream().filter(pu -> pu.getUser().equals(user)).findFirst();
    if (optional.isPresent()) {
      userPermission.getPermission().getPermissionUsers().remove(optional.get());
    }
    return userPermission.getPermission();
  }

  private List<Permission> savePermissionUser(final U update) {
    final List<Permission> permissions = newArrayList();
    update.getUserPermissions().stream().forEach(pu -> permissions.add(addPermissionUser(update, pu)));
    return permissions;
  }

  private U restoreUserPassword(final U type) {
    final Optional<U> optional = userRepository.findById(type.getId());
    if (optional.isPresent()) {
      if (isNotBlank(type.getPassword()) && type.getPassword().matches("^(?=.*\\d).{4,8}$")) {
        type.changePassword(type.getPassword());
        return type;
      }
      type.setPassword(optional.get().getPassword());
      return type;
    }
    type.changePassword(isNotBlank(type.getPassword()) ? type.getPassword() : type.getNewPassword());
    return type;
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "user", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "users", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public List<U> saveAll(final List<U> types) {
    return super.saveAll(types);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "user", sync = true, condition = "#type.getId() > 0")
  //@formatter:on
  public U findOne(final U type) {
    return super.findOne(type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "users", sync = true)
  //@formatter:on
  public Page<U> findAll(final int page, final int size, final Direction direction, final List<String> properties, final U type) {
    return super.findAll(page, size, direction, properties, type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "users", sync = true)
  //@formatter:on
  public Page<U> findAll(final int page, final int size, final Direction direction, final List<String> properties, final TypeFilter<U> filter) {
    return super.findAll(page, size, direction, properties, filter);
  }

  @Override
  public Map<String, Object> getHeaders() {
    final Map<String, Object> headers = super.getHeaders();
    headers.replace(CONVERSION_HINT_HEADER, UserView.Transfer.class);
    return headers;
  }

  @Override
  public UserDetails loadUserByUsername(final String username) {
    try {
      return findOne((U) User.getInstance(username));
    } catch (Exception e) {
      throw new UsernameNotFoundException(INVALID_USERNAME_OR_PASSWORD, e);
    }
  }

  @Override
  public U register(final U type) {
    try {
      loadUserByUsername(type.getUsername());
      throw new TypeRuntimeException("Registration Failed");
    } catch (UsernameNotFoundException e) {
      type.setActive(false);
      type.setRoot(false);
      type.setToken(createToken(type.getUsername()));
      return save(type);
    }
  }

  @Override
  public U forgotPassword(final U type) {
    try {
      final U attachedType = (U) loadUserByUsername(type.getUsername());
      attachedType.setToken(createToken(attachedType.getUsername()));
      return save(attachedType);
    } catch (UsernameNotFoundException e) {
      LOGGER.warn(e.getMessage(), e);
      throw new TypeRuntimeException("Forgot Password Failed", e);
    }
  }

  @Override
  public U confirmForgotPassword(final U type) {
    try {
      final U attachedType = (U) loadUserByUsername(type.getUsername());
      attachedType.setPassword(type.getPassword());
      return save(attachedType);
    } catch (UsernameNotFoundException e) {
      LOGGER.warn(e.getMessage(), e);
      throw new TypeRuntimeException("Confirm Forgot Password Failed", e);
    }
  }

  @Override
  public U confirmRegister(final U type) {
    try {
      final U attachedType = (U) loadUserByUsername(type.getUsername());
      attachedType.setActive(true);
      return save(attachedType);
    } catch (UsernameNotFoundException e) {
      LOGGER.warn(e.getMessage(), e);
      throw new TypeRuntimeException("Confirm Registration Failed", e);
    }
  }
}
