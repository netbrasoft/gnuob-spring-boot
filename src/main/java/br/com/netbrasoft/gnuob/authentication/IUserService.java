package br.com.netbrasoft.gnuob.authentication;

import org.springframework.security.core.userdetails.UserDetailsService;

import br.com.netbrasoft.gnuob.type.ITypeService;

public interface IUserService<U extends User> extends ITypeService<U>, UserDetailsService {
  public U register(final U type);

  public U forgotPassword(final U type);

  public U confirmForgotPassword(final U type);

  public U confirmRegister(final U type);
}
