package br.com.netbrasoft.gnuob.authentication;

import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.BooleanUtils.isNotFalse;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.type.AbstractType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
public class PermissionUser extends AbstractType {
  private static final long serialVersionUID = 5732410222252000930L;
  protected static final String[] PERMISSION_USER_EXCLUDE_FIELDS = addAll(TYPE_EXCLUDE_FIELDS, "canCreate", "canRead", "canUpdate", "canDelete");
  private Boolean canCreate;
  private Boolean canRead;
  private Boolean canUpdate;
  private Boolean canDelete;
  private User user;

  public static PermissionUser getInstance(final User user) {
    final PermissionUser permissionUser = new PermissionUser();
    permissionUser.setUser(user);
    return permissionUser;
  }

  @Override
  public void prePersist() {
    setCanCreate(isNotFalse(getCanCreate()));
    setCanRead(isNotFalse(getCanRead()));
    setCanUpdate(isNotFalse(getCanUpdate()));
    setCanDelete(isNotFalse(getCanDelete()));
    super.prePersist();
  }

  @Override
  public void preUpdate() {
    setCanCreate(isNotFalse(getCanCreate()));
    setCanRead(isNotFalse(getCanRead()));
    setCanUpdate(isNotFalse(getCanUpdate()));
    setCanDelete(isNotFalse(getCanDelete()));
    super.preUpdate();
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, PERMISSION_USER_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, PERMISSION_USER_EXCLUDE_FIELDS);
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanCreate() {
    return canCreate;
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanRead() {
    return canRead;
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanUpdate() {
    return canUpdate;
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanDelete() {
    return canDelete;
  }

  @Cache(usage = READ_WRITE)
  @ManyToOne(fetch = EAGER)
  @JsonView({PermissionView.Transfer.class})
  public User getUser() {
    return user;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, PERMISSION_USER_EXCLUDE_FIELDS);
  }
}
