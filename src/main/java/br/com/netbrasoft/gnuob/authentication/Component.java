package br.com.netbrasoft.gnuob.authentication;

import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.type.AbstractActiveType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "code")})
public class Component extends AbstractActiveType {
  private static final long serialVersionUID = -8828121915237925038L;
  protected static final String[] COMPONENT_EXCLUDE_FIELDS = addAll(TYPE_EXCLUDE_FIELDS);

  public static Component getInstance() {
    return new Component();
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, COMPONENT_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, COMPONENT_EXCLUDE_FIELDS);
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, COMPONENT_EXCLUDE_FIELDS);
  }
}
