package br.com.netbrasoft.gnuob.authentication;

import br.com.netbrasoft.gnuob.type.ITypeRepository;

public interface IRoleRepository<R extends Role> extends ITypeRepository<R> {
}
