package br.com.netbrasoft.gnuob.authentication;

import static br.com.netbrasoft.gnuob.Utils.extractUsername;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.netbrasoft.gnuob.type.IController;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@RestController
@CrossOrigin(maxAge = 3600)
public class UserController<U extends User> implements IController<U> {
  @Autowired(required = true)
  private IUserService<U> userService;

  @Override
  public ITypeService<U> getService() {
    return userService;
  }

  @Override
  @DeleteMapping(value = "/user")
  public void delete(@RequestBody final U type) {
    IController.super.delete(type);
  }

  @Override
  @DeleteMapping(value = "/user/all")
  public void deleteAll(@RequestBody final List<U> types) {
    IController.super.deleteAll(types);
  }

  @Override
  @PostMapping(value = "/user")
  @JsonView({UserView.Transfer.class})
  public U save(@RequestBody final U type) {
    return IController.super.save(type);
  }

  @PostMapping(value = "/user/register")
  public void register(@RequestBody final U type) {
    userService.register(type);
  }

  @PostMapping(value = "/user/register/{token}")
  public void confirmRegister(@PathVariable final String token) {
    userService.confirmRegister((U) User.getInstance(extractUsername(token)));
  }

  @PostMapping(value = "/user/forgot/password")
  public void forgotPassword(@RequestBody final U type) {
    userService.forgotPassword(type);
  }

  @PostMapping(value = "/user/forgot/password/{token}")
  public void confirmForgotPassword(@RequestBody final U type, @PathVariable final String token) {
    type.setUsername(extractUsername(token));
    userService.confirmForgotPassword(type);
  }

  @Override
  @PostMapping(value = "/user/all")
  @JsonView({UserView.Transfer.class})
  public List<U> saveAll(@RequestBody final List<U> types) {
    return IController.super.saveAll(types);
  }

  @Override
  @PostMapping(value = "/user/of/one")
  @JsonView({UserView.Transfer.class})
  public U findOne(@RequestBody final U type) {
    return IController.super.findOne(type);
  }

  @Override
  @JsonView({UserView.Transfer.class})
  @PostMapping(value = "/user/of/all")
  public List<U> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final U type, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, type, response);
  }

  @Override
  @JsonView({UserView.Transfer.class})
  @PostMapping(value = "/user/of/all/filter")
  public List<U> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final TypeFilter<U> filter, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, filter, response);
  }
}
