package br.com.netbrasoft.gnuob.authentication;

import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.BooleanUtils.isNotFalse;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.type.AbstractType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
public class PermissionComponent extends AbstractType {
  private static final long serialVersionUID = -279560456382717645L;
  protected static final String[] PERMISSION_COMPONENT_EXCLUDE_FIELDS = addAll(TYPE_EXCLUDE_FIELDS);
  private Boolean canEdit;
  private Boolean isRequired;
  private Component component;

  public static PermissionComponent getInstance(final Component component) {
    final PermissionComponent permissionComponent = new PermissionComponent();
    permissionComponent.setComponent(component);
    return permissionComponent;
  }

  @Override
  public void prePersist() {
    setCanEdit(isNotFalse(getCanEdit()));
    setIsRequired(isNotFalse(getIsRequired()));
    super.prePersist();
  }

  @Override
  public void preUpdate() {
    setCanEdit(isNotFalse(getCanEdit()));
    setIsRequired(isNotFalse(getIsRequired()));
    super.preUpdate();
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, PERMISSION_COMPONENT_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, PERMISSION_COMPONENT_EXCLUDE_FIELDS);
  }

  @Column
  @JsonView({PermissionView.Transfer.class})
  public Boolean getCanEdit() {
    return canEdit;
  }

  @Column
  @JsonView({PermissionView.Transfer.class})
  public Boolean getIsRequired() {
    return isRequired;
  }

  @Cache(usage = READ_WRITE)
  @ManyToOne(fetch = EAGER)
  @JsonView({PermissionView.Transfer.class})
  public Component getComponent() {
    return component;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, PERMISSION_COMPONENT_EXCLUDE_FIELDS);
  }
}
