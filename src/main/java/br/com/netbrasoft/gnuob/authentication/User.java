package br.com.netbrasoft.gnuob.authentication;

import static com.google.common.collect.Sets.newHashSet;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.BooleanUtils.isNotFalse;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.apache.commons.lang3.RandomStringUtils.random;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.StringUtils.trim;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import java.util.Collection;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.netbrasoft.gnuob.type.AbstractActiveType;
import de.rtner.security.auth.spi.SimplePBKDF2;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "code"), @UniqueConstraint(columnNames = "username")})
public class User extends AbstractActiveType implements UserDetails, Authentication {
  private static final long serialVersionUID = -1157293859376990976L;
  protected static final String[] USER_EXCLUDE_FIELDS = addAll(TYPE_ACTIVE_EXCLUDE_FIELDS, "authenticated", "online", "token", "newPassword", "userRoles", "userPermissions");
  private Boolean changePassword;
  private boolean authenticated;
  private Boolean mobile;
  private Boolean online;
  private Boolean root;
  private String email;
  private String firstName;
  private String lastName;
  private String username;
  private String mobileIpAddress;
  private String mobileManufacturer;
  private String mobileMaxDownload;
  private String mobileModel;
  private String mobileNetworkType;
  private String mobilePackageName;
  private String mobilePlatform;
  private String mobileSerial;
  private String mobileSignal;
  private String mobileUuid;
  private String mobileVersion;
  private String mobileVersionCode;
  private String mobileVersionNumber;
  private String password;
  private String token;
  private String cookie;
  private final String newPassword = random(8, true, true);
  private Set<UserRole> userRoles = newHashSet();
  private Set<UserPermission> userPermissions = newHashSet();

  public static User getInstance(final String username) {
    final User user = new User();
    user.setUsername(username);
    return user;
  }

  @Override
  public void prePersist() {
    setName(join(getFirstName(), " ", getLastName()));
    if (isBlank(getEmail())) {
      setEmail(getUsername());
    }
    setChangePassword(isNotFalse(getChangePassword()));
    setMobile(isTrue(getMobile()));
    setOnline(isTrue(getOnline()));
    setRoot(isTrue(getRoot()));
    if (isTrue(getChangePassword()) && isBlank(getPassword())) {
      changePassword(getNewPassword());
    }
    super.prePersist();
  }

  protected void changePassword(final String password) {
    setPassword(new SimplePBKDF2().deriveKeyFormatted(trim(password)));
  }

  @Override
  public void preUpdate() {
    setName(join(getFirstName(), " ", getLastName()));
    if (isBlank(getEmail())) {
      setEmail(getUsername());
    }
    setChangePassword(isNotFalse(getChangePassword()));
    setMobile(isTrue(getMobile()));
    setOnline(isTrue(getOnline()));
    setRoot(isTrue(getRoot()));
    if (isTrue(getChangePassword()) && isBlank(getPassword())) {
      changePassword(getNewPassword());
    }
    super.preUpdate();
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, USER_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, addAll(USER_EXCLUDE_FIELDS, "password"));
  }

  @Override
  @Transient
  @JsonIgnore
  public Object getCredentials() {
    return getPassword();
  }

  @Override
  @Transient
  @JsonIgnore
  public Object getDetails() {
    return this;
  }

  @Override
  @Transient
  @JsonIgnore
  public Object getPrincipal() {
    return this;
  }

  @Override
  @Transient
  @JsonIgnore
  public boolean isAuthenticated() {
    return this.authenticated;
  }

  @Override
  public void setAuthenticated(final boolean isAuthenticated) {
    this.authenticated = isAuthenticated;
  }

  @Override
  @Transient
  @JsonIgnore
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return getUserRoles();
  }

  @Override
  @Column
  @Size(max = 127)
  public String getPassword() {
    return this.password;
  }

  @Override
  @Column
  @Size(max = 127)
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getUsername() {
    return this.username;
  }

  @Override
  @Transient
  @JsonIgnore
  public boolean isAccountNonExpired() {
    return getActive();
  }

  @Override
  @Transient
  @JsonIgnore
  public boolean isAccountNonLocked() {
    return getActive();
  }

  @Override
  @Transient
  @JsonIgnore
  public boolean isCredentialsNonExpired() {
    return getActive();
  }

  @Override
  @Transient
  @JsonIgnore
  public boolean isEnabled() {
    return getActive();
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public Boolean getChangePassword() {
    return changePassword;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public Boolean getMobile() {
    return mobile;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public Boolean getOnline() {
    return online;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public Boolean getRoot() {
    return root;
  }

  @Column
  @Size(max = 127)
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getEmail() {
    return email;
  }

  @Column
  @Size(max = 25)
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getFirstName() {
    return firstName;
  }

  @Column
  @Size(max = 25)
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getLastName() {
    return lastName;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobileIpAddress() {
    return mobileIpAddress;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobileManufacturer() {
    return mobileManufacturer;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobileMaxDownload() {
    return mobileMaxDownload;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobileModel() {
    return mobileModel;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobileNetworkType() {
    return mobileNetworkType;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobilePackageName() {
    return mobilePackageName;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobilePlatform() {
    return mobilePlatform;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobileSerial() {
    return mobileSerial;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobileSignal() {
    return mobileSignal;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobileUuid() {
    return mobileUuid;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobileVersion() {
    return mobileVersion;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobileVersionCode() {
    return mobileVersionCode;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getMobileVersionNumber() {
    return mobileVersionNumber;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getToken() {
    return token;
  }

  @Column
  @JsonView({UserView.Transfer.class, RoleView.Transfer.class, PermissionView.Transfer.class})
  public String getCookie() {
    return cookie;
  }

  @JsonIgnore
  @Transient
  public String getNewPassword() {
    return newPassword;
  }

  @OneToMany(fetch = EAGER, cascade = {PERSIST, MERGE, REMOVE}, orphanRemoval = true)
  @JsonView({UserView.Transfer.class})
  public Set<UserRole> getUserRoles() {
    return userRoles;
  }

  @OneToMany(fetch = EAGER, cascade = {PERSIST, MERGE, REMOVE}, orphanRemoval = true)
  @JsonView({UserView.Transfer.class})
  public Set<UserPermission> getUserPermissions() {
    return userPermissions;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, USER_EXCLUDE_FIELDS);
  }
}
