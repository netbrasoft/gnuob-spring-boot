package br.com.netbrasoft.gnuob.authentication;

import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.BooleanUtils.isNotFalse;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.type.AbstractType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
public class PermissionRole extends AbstractType {
  private static final long serialVersionUID = -4534643020672646740L;
  protected static final String[] PERMISSION_ROLE_EXCLUDE_FIELDS = addAll(TYPE_EXCLUDE_FIELDS);
  private Boolean canCreate;
  private Boolean canRead;
  private Boolean canUpdate;
  private Boolean canDelete;
  private Role role;

  public static PermissionRole getInstance(final Role role) {
    final PermissionRole permissionRole = new PermissionRole();
    permissionRole.setRole(role);
    return permissionRole;
  }

  @Override
  public void prePersist() {
    setCanCreate(isNotFalse(getCanCreate()));
    setCanRead(isNotFalse(getCanRead()));
    setCanUpdate(isNotFalse(getCanUpdate()));
    setCanDelete(isNotFalse(getCanDelete()));
    super.prePersist();
  }

  @Override
  public void preUpdate() {
    setCanCreate(isNotFalse(getCanCreate()));
    setCanRead(isNotFalse(getCanRead()));
    setCanUpdate(isNotFalse(getCanUpdate()));
    setCanDelete(isNotFalse(getCanDelete()));
    super.preUpdate();
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, addAll(TYPE_EXCLUDE_FIELDS, "canCreate", "canRead", "canUpdate", "canDelete"));
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, addAll(TYPE_EXCLUDE_FIELDS, "canCreate", "canRead", "canUpdate", "canDelete"));
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanCreate() {
    return canCreate;
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanRead() {
    return canRead;
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanUpdate() {
    return canUpdate;
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanDelete() {
    return canDelete;
  }

  @Cache(usage = READ_WRITE)
  @ManyToOne(fetch = EAGER)
  @JsonView({PermissionView.Transfer.class})
  public Role getRole() {
    return role;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, addAll(TYPE_EXCLUDE_FIELDS, PERMISSION_ROLE_EXCLUDE_FIELDS));
  }
}
