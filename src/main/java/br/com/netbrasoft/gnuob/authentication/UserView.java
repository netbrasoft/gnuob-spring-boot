package br.com.netbrasoft.gnuob.authentication;

import br.com.netbrasoft.gnuob.type.TypeView;

public interface UserView extends TypeView {
  public interface Transfer extends TypeView.Transfer {
  }
}
