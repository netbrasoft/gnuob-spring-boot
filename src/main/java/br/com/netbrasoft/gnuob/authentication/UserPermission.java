package br.com.netbrasoft.gnuob.authentication;

import static com.google.common.collect.Sets.newHashSet;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.BooleanUtils.isNotFalse;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.type.AbstractType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
public class UserPermission extends AbstractType {
  private static final long serialVersionUID = -7433122711749067784L;
  protected static final String[] USER_PERMISSION_EXCLUDE_FIELDS = addAll(TYPE_EXCLUDE_FIELDS);
  private Boolean canCreate;
  private Boolean canRead;
  private Boolean canUpdate;
  private Boolean canDelete;
  private Permission permission;
  private Set<UserComponent> userComponents = newHashSet();

  public static UserPermission getInstance(final Permission permission) {
    final UserPermission userPermission = new UserPermission();
    userPermission.setPermission(permission);
    return userPermission;
  }

  @Override
  public void prePersist() {
    setCanCreate(isNotFalse(getCanCreate()));
    setCanRead(isNotFalse(getCanRead()));
    setCanUpdate(isNotFalse(getCanUpdate()));
    setCanDelete(isNotFalse(getCanDelete()));
    super.prePersist();
  }

  @Override
  public void preUpdate() {
    setCanCreate(isNotFalse(getCanCreate()));
    setCanRead(isNotFalse(getCanRead()));
    setCanUpdate(isNotFalse(getCanUpdate()));
    setCanDelete(isNotFalse(getCanDelete()));
    super.preUpdate();
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, addAll(TYPE_EXCLUDE_FIELDS, "canCreate", "canRead", "canUpdate", "canDelete", "userComponents"));
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, addAll(TYPE_EXCLUDE_FIELDS, "canCreate", "canRead", "canUpdate", "canDelete", "userComponents"));
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanCreate() {
    return canCreate;
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanRead() {
    return canRead;
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanUpdate() {
    return canUpdate;
  }

  @Column
  @JsonView({UserView.Transfer.class})
  public Boolean getCanDelete() {
    return canDelete;
  }

  @ManyToOne(fetch = EAGER)
  @JsonView({UserView.Transfer.class})
  public Permission getPermission() {
    return permission;
  }

  @OneToMany(fetch = EAGER, cascade = {PERSIST, MERGE, REMOVE})
  @JsonView({UserView.Transfer.class})
  public Set<UserComponent> getUserComponents() {
    return userComponents;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, USER_PERMISSION_EXCLUDE_FIELDS);
  }
}
