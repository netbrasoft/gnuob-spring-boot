package br.com.netbrasoft.gnuob.authentication;

import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.type.AbstractType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
public class RoleUser extends AbstractType {
  private static final long serialVersionUID = -2402756711920261802L;
  protected static final String[] ROLE_USER_EXCLUDE_FIELDS = addAll(TYPE_EXCLUDE_FIELDS);
  private User user;

  public static RoleUser getInstance(final User user) {
    final RoleUser userRole = new RoleUser();
    userRole.setUser(user);
    return userRole;
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, ROLE_USER_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, ROLE_USER_EXCLUDE_FIELDS);
  }

  @Cache(usage = READ_WRITE)
  @ManyToOne(fetch = EAGER)
  @JsonView({RoleView.Transfer.class})
  public User getUser() {
    return user;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, ROLE_USER_EXCLUDE_FIELDS);
  }
}
