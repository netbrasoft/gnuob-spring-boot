package br.com.netbrasoft.gnuob.authentication;

import static br.com.netbrasoft.gnuob.authentication.PermissionRole.PERMISSION_ROLE_EXCLUDE_FIELDS;
import static br.com.netbrasoft.gnuob.authentication.UserRole.USER_ROLE_EXCLUDE_FIELDS;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.partitioningBy;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.springframework.beans.BeanUtils.copyProperties;
import static org.springframework.messaging.core.AbstractMessageSendingTemplate.CONVERSION_HINT_HEADER;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.netbrasoft.gnuob.type.AbstractTypeService;
import br.com.netbrasoft.gnuob.type.ITypeRepository;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@Service
public class RoleService<R extends Role> extends AbstractTypeService<R> {
  @Autowired(required = true)
  protected IRoleRepository<R> roleRepository;
  @Autowired(required = true)
  private ITypeService<Permission> permissionService;
  @Autowired(required = true)
  private ITypeService<User> userService;

  @Override
  public String getPath() {
    return Role.class.getSimpleName().toLowerCase();
  }

  @Override
  public ITypeRepository<R> getTypeRepository() {
    return roleRepository;
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "role", condition = "#type.getId() > 0"),
      @CacheEvict(value = "roles", allEntries = true, condition = "#type.getId() > 0")
    }
  )
  //@formatter:on
  public void delete(final R type) {
    super.delete(type);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "role", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "roles", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public void deleteAll(final List<R> types) {
    super.deleteAll(types);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "roles", allEntries = true, condition = "#type.getId() > 0"),
    },
    put = {
      @CachePut(value = "role", condition = "#type.getId() > 0"),
    }
  )
  //@formatter:on
  public R save(final R type) {
    final List<User> users = newArrayList();
    final List<Permission> permissions = newArrayList();
    try {
      users.addAll(updateUsers(newArrayList(type)));
      permissions.addAll(updatePermissions(newArrayList(type)));
      final R attachedType = super.save(type);
      users.forEach(u -> u.getUserRoles().stream().filter(ur -> ur.getRole().equals(attachedType)).forEach(ur -> ur.setRole(attachedType)));
      permissions.forEach(p -> p.getPermissionRoles().stream().filter(pr -> pr.getRole().equals(attachedType)).forEach(pr -> pr.setRole(attachedType)));
      return attachedType;
    } finally {
      userService.saveAll(users);
      permissionService.saveAll(permissions);
    }
  }

  private List<Permission> updatePermissions(final List<R> types) {
    final List<Permission> permissions = newArrayList();
    types.stream().filter(R::flagDetached).forEach(r -> {
      final R original = findOne(r);
      permissions.addAll(savePermissionRole(original, r));
      permissions.addAll(deletePermissionRole(original, r));
    });
    types.stream().filter(R::flagNotDetached).forEach(p -> permissions.addAll(this.savePermissionRole(p)));
    return permissions;
  }

  private List<Permission> savePermissionRole(R original, final R update) {
    final List<Permission> permissions = newArrayList();
    update.getRolePermissions().stream().collect(partitioningBy(original.getRolePermissions()::contains)).get(false).forEach(save -> permissions.add(addPermissionRole(update, save)));
    return permissions;
  }

  private Permission addPermissionRole(final Role role, final RolePermission rolePermission) {
    final PermissionRole permissionRole = PermissionRole.getInstance(role);
    final Optional<PermissionRole> optional = rolePermission.getPermission().getPermissionRoles().stream().filter(pr -> pr.getRole().equals(role)).findFirst();
    if (!optional.isPresent()) {
      copyProperties(permissionRole, permissionRole, addAll(PERMISSION_ROLE_EXCLUDE_FIELDS));
      rolePermission.getPermission().getPermissionRoles().add(permissionRole);
    } else {
      copyProperties(permissionRole, optional.get());
    }
    return rolePermission.getPermission();
  }

  private List<Permission> deletePermissionRole(R original, final R update) {
    final List<Permission> permissions = newArrayList();
    original.getRolePermissions().stream().collect(partitioningBy(update.getRolePermissions()::contains)).get(false).forEach(remove -> permissions.add(removePermissionRole(original, remove)));
    return permissions;
  }

  private Permission removePermissionRole(final Role role, final RolePermission rolePermission) {
    final Optional<PermissionRole> optional = rolePermission.getPermission().getPermissionRoles().stream().filter(pr -> pr.getRole().equals(role)).findFirst();
    rolePermission.getPermission().getPermissionRoles().remove(optional.get());
    return rolePermission.getPermission();
  }

  private List<Permission> savePermissionRole(R update) {
    final List<Permission> permissions = newArrayList();
    update.getRolePermissions().stream().forEach(pu -> permissions.add(addPermissionRole(update, pu)));
    return permissions;
  }

  private List<User> updateUsers(final List<R> types) {
    final List<User> users = newArrayList();
    types.stream().filter(R::flagDetached).forEach(r -> {
      final R original = findOne(r);
      users.addAll(saveUserRole(original, r));
      users.addAll(deleteUserRole(original, r));
    });
    types.stream().filter(R::flagNotDetached).forEach(p -> users.addAll(this.saveUserRole(p)));
    return users;
  }

  private List<User> saveUserRole(R original, final R update) {
    final List<User> users = newArrayList();
    update.getRoleUsers().stream().collect(partitioningBy(original.getRoleUsers()::contains)).get(false).forEach(save -> users.add(addUserRole(update, save)));
    return users;
  }

  private User addUserRole(final Role role, RoleUser roleUser) {
    final UserRole userRole = UserRole.getInstance(role);
    final Optional<UserRole> optional = roleUser.getUser().getUserRoles().stream().filter(ur -> ur.getRole().equals(role)).findFirst();
    if (!optional.isPresent()) {
      copyProperties(userRole, userRole, addAll(USER_ROLE_EXCLUDE_FIELDS));
      roleUser.getUser().getUserRoles().add(userRole);
    } else {
      copyProperties(userRole, optional.get());
    }
    return roleUser.getUser();
  }

  private User removeUserRole(final Role role, RoleUser roleUser) {
    final Optional<UserRole> optional = roleUser.getUser().getUserRoles().stream().filter(ur -> ur.getRole().equals(role)).findFirst();
    roleUser.getUser().getUserRoles().remove(optional.get());
    return roleUser.getUser();
  }

  private List<User> deleteUserRole(R original, final R update) {
    final List<User> users = newArrayList();
    original.getRoleUsers().stream().collect(partitioningBy(update.getRoleUsers()::contains)).get(false).forEach(remove -> users.add(removeUserRole(original, remove)));
    return users;
  }

  private List<User> saveUserRole(R update) {
    final List<User> users = newArrayList();
    update.getRoleUsers().stream().forEach(pu -> users.add(addUserRole(update, pu)));
    return users;
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "role", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "roles", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public List<R> saveAll(final List<R> types) {
    return super.saveAll(types);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "role", sync = true, condition = "#type.getId() > 0")
  //@formatter:on
  public R findOne(final R type) {
    return super.findOne(type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "roles", sync = true)
  //@formatter:on
  public Page<R> findAll(final int page, final int size, final Direction direction, final List<String> properties, final R type) {
    return super.findAll(page, size, direction, properties, type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "roles", sync = true)
  //@formatter:on
  public Page<R> findAll(final int page, final int size, final Direction direction, final List<String> properties, final TypeFilter<R> filter) {
    return super.findAll(page, size, direction, properties, filter);
  }

  @Override
  public Map<String, Object> getHeaders() {
    final Map<String, Object> headers = super.getHeaders();
    headers.replace(CONVERSION_HINT_HEADER, RoleView.Transfer.class);
    return headers;
  }
}
