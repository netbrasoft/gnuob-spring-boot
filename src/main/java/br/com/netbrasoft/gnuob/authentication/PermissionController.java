package br.com.netbrasoft.gnuob.authentication;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.netbrasoft.gnuob.type.IController;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@RestController
@CrossOrigin(maxAge = 3600)
public class PermissionController<P extends Permission> implements IController<P> {
  @Autowired(required = true)
  private ITypeService<P> permissionService;

  @Override
  public ITypeService<P> getService() {
    return permissionService;
  }

  @Override
  @DeleteMapping(value = "/permission")
  public void delete(@RequestBody final P type) {
    IController.super.delete(type);
  }

  @Override
  @DeleteMapping(value = "/permission/all")
  public void deleteAll(@RequestBody final List<P> types) {
    IController.super.deleteAll(types);
  }

  @Override
  @PostMapping(value = "/permission")
  @JsonView({PermissionView.Transfer.class})
  public P save(@RequestBody final P type) {
    return IController.super.save(type);
  }

  @Override
  @PostMapping(value = "/permission/all")
  @JsonView({PermissionView.Transfer.class})
  public List<P> saveAll(@RequestBody final List<P> types) {
    return IController.super.saveAll(types);
  }

  @Override
  @PostMapping(value = "/permission/of/one")
  @JsonView({PermissionView.Transfer.class})
  public P findOne(@RequestBody final P type) {
    return IController.super.findOne(type);
  }

  @Override
  @JsonView({PermissionView.Transfer.class})
  @PostMapping(value = "/permission/of/all")
  public List<P> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final P type, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, type, response);
  }

  @Override
  @JsonView({PermissionView.Transfer.class})
  @PostMapping(value = "/permission/of/all/filter")
  public List<P> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final TypeFilter<P> filter, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, filter, response);
  }
}

