package br.com.netbrasoft.gnuob.authentication;

import static br.com.netbrasoft.gnuob.authentication.RolePermission.ROLE_PERMISSION_EXCLUDE_FIELDS;
import static br.com.netbrasoft.gnuob.authentication.UserPermission.USER_PERMISSION_EXCLUDE_FIELDS;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.partitioningBy;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.springframework.beans.BeanUtils.copyProperties;
import static org.springframework.messaging.core.AbstractMessageSendingTemplate.CONVERSION_HINT_HEADER;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.netbrasoft.gnuob.type.AbstractTypeService;
import br.com.netbrasoft.gnuob.type.ITypeRepository;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@Service
public class PermissionService<P extends Permission> extends AbstractTypeService<P> {
  @Autowired(required = true)
  protected IPermissionRepository<P> permissionRepository;
  @Autowired(required = true)
  private ITypeService<User> userService;
  @Autowired(required = true)
  private ITypeService<Role> roleService;

  @Override
  public String getPath() {
    return Permission.class.getSimpleName().toLowerCase();
  }

  @Override
  public ITypeRepository<P> getTypeRepository() {
    return permissionRepository;
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "permission", condition = "#type.getId() > 0"),
      @CacheEvict(value = "permissions", allEntries = true, condition = "#type.getId() > 0")
    }
  )
  //@formatter:on
  public void delete(final P type) {
    super.delete(type);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "permission", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "permissions", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public void deleteAll(final List<P> types) {
    super.deleteAll(types);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "permissions", allEntries = true, condition = "#type.getId() > 0"),
    },
    put = {
      @CachePut(value = "permission", condition = "#type.getId() > 0"),
    }
  )
  //@formatter:on
  public P save(final P type) {
    final List<User> users = newArrayList();
    final List<Role> roles = newArrayList();
    try {
      users.addAll(updateUsers(newArrayList(type)));
      roles.addAll(updateRoles(newArrayList(type)));
      final P attachedType = super.save(type);
      users.forEach(u -> u.getUserPermissions().stream().filter(up -> up.getPermission().equals(attachedType)).forEach(up -> up.setPermission(attachedType)));
      roles.forEach(r -> r.getRolePermissions().stream().filter(rp -> rp.getPermission().equals(attachedType)).forEach(rp -> rp.setPermission(attachedType)));
      return attachedType;
    } finally {
      userService.saveAll(users);
      roleService.saveAll(roles);
    }
  }

  private List<User> updateUsers(final List<P> types) {
    final List<User> users = newArrayList();
    types.stream().filter(P::flagDetached).forEach(p -> {
      final P original = findOne(p);
      users.addAll(saveUserPermission(original, p));
      users.addAll(deleteUserPermission(original, p));
    });
    types.stream().filter(P::flagNotDetached).forEach(p -> users.addAll(this.saveUserPermission(p)));
    return users;
  }

  private List<User> saveUserPermission(P original, final P update) {
    final List<User> users = newArrayList();
    update.getPermissionUsers().stream().collect(partitioningBy(original.getPermissionUsers()::contains)).get(false).forEach(save -> users.add(addUserPermission(update, save)));
    return users;
  }

  private User addUserPermission(final Permission permission, final PermissionUser permissionUser) {
    final UserPermission userPermission = UserPermission.getInstance(permission);
    final Optional<UserPermission> optional = permissionUser.getUser().getUserPermissions().stream().filter(up -> up.getPermission().equals(permission)).findFirst();
    if (!optional.isPresent()) {
      copyProperties(permissionUser, userPermission, addAll(USER_PERMISSION_EXCLUDE_FIELDS));
      permissionUser.getUser().getUserPermissions().add(userPermission);
    } else {
      copyProperties(permissionUser, optional.get());
    }
    return permissionUser.getUser();
  }

  private List<User> deleteUserPermission(P original, final P update) {
    final List<User> users = newArrayList();
    original.getPermissionUsers().stream().collect(partitioningBy(update.getPermissionUsers()::contains)).get(false).forEach(remove -> users.add(removeUserPermission(original, remove)));
    return users;
  }

  private User removeUserPermission(final Permission permission, final PermissionUser permissionUser) {
    final Optional<UserPermission> optional = permissionUser.getUser().getUserPermissions().stream().filter(up -> up.getPermission().equals(permission)).findFirst();
    if (optional.isPresent()) {
      permissionUser.getUser().getUserPermissions().remove(optional.get());
    }
    return permissionUser.getUser();
  }

  private List<User> saveUserPermission(P update) {
    final List<User> users = newArrayList();
    update.getPermissionUsers().stream().forEach(pu -> users.add(addUserPermission(update, pu)));
    return users;
  }

  private List<Role> updateRoles(final List<P> types) {
    final List<Role> roles = newArrayList();
    types.stream().filter(P::flagDetached).forEach(p -> {
      final P original = findOne(p);
      roles.addAll(saveRolePermission(original, p));
      roles.addAll(deleteRolePermission(original, p));
    });
    types.stream().filter(P::flagNotDetached).forEach(p -> roles.addAll(this.saveRolePermission(p)));
    return roles;
  }

  private List<Role> saveRolePermission(P original, final P update) {
    final List<Role> roles = newArrayList();
    update.getPermissionRoles().stream().collect(partitioningBy(original.getPermissionRoles()::contains)).get(false).forEach(save -> roles.add(addRolePermission(update, save)));
    return roles;
  }

  private Role addRolePermission(final Permission permission, final PermissionRole permissionRole) {
    final RolePermission rolePermission = RolePermission.getInstance(permission);
    final Optional<RolePermission> optional = permissionRole.getRole().getRolePermissions().stream().filter(rp -> rp.getPermission().equals(permission)).findFirst();
    if (!optional.isPresent()) {
      copyProperties(permissionRole, rolePermission, addAll(ROLE_PERMISSION_EXCLUDE_FIELDS));
      permissionRole.getRole().getRolePermissions().add(rolePermission);
    } else {
      copyProperties(permissionRole, optional.get());
    }
    return permissionRole.getRole();
  }

  private List<Role> deleteRolePermission(P original, final P update) {
    final List<Role> roles = newArrayList();
    original.getPermissionRoles().stream().collect(partitioningBy(update.getPermissionRoles()::contains)).get(false).forEach(remove -> roles.add(removeRolePermission(original, remove)));
    return roles;
  }

  private Role removeRolePermission(final Permission permission, final PermissionRole permissionRole) {
    final Optional<RolePermission> optional = permissionRole.getRole().getRolePermissions().stream().filter(up -> up.getPermission().equals(permission)).findFirst();
    if (optional.isPresent()) {
      permissionRole.getRole().getRolePermissions().remove(optional.get());
    }
    return permissionRole.getRole();
  }

  private List<Role> saveRolePermission(P update) {
    final List<Role> roles = newArrayList();
    update.getPermissionRoles().stream().forEach(pr -> roles.add(addRolePermission(update, pr)));
    return roles;
  }


  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "permission", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "permissions", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public List<P> saveAll(final List<P> types) {
    return super.saveAll(types);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "permission", sync = true, condition = "#type.getId() > 0")
  //@formatter:on
  public P findOne(final P type) {
    return super.findOne(type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "permissions", sync = true)
  //@formatter:on
  public Page<P> findAll(final int page, final int size, final Direction direction, final List<String> properties, final P type) {
    return super.findAll(page, size, direction, properties, type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "permissions", sync = true)
  //@formatter:on
  public Page<P> findAll(final int page, final int size, final Direction direction, final List<String> properties, final TypeFilter<P> filter) {
    return super.findAll(page, size, direction, properties, filter);
  }

  @Override
  public Map<String, Object> getHeaders() {
    final Map<String, Object> headers = super.getHeaders();
    headers.replace(CONVERSION_HINT_HEADER, PermissionView.Transfer.class);
    return headers;
  }
}
