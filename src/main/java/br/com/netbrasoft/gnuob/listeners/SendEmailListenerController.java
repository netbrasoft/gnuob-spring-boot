package br.com.netbrasoft.gnuob.listeners;

import static org.slf4j.LoggerFactory.getLogger;

import java.io.Serializable;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

@Component
public class SendEmailListenerController {
  private static final Logger LOGGER = getLogger(SendEmailListenerController.class);
  @Autowired(required = true)
  private JavaMailSender mailSender;

  @JmsListener(destination = "emailQueue")
  public void receiveMessage(final Email email) {
    try {
      final MimeMessagePreparator messagePreparator = mimeMessage -> {
        final MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        messageHelper.setFrom(email.from);
        messageHelper.setTo(email.to);
        messageHelper.setSubject(email.subject);
        messageHelper.setText(email.text, true);
      };
      mailSender.send(messagePreparator);
    } catch (final Exception e) {
      LOGGER.warn(e.getMessage(), e);
    }
  }

  public static final class Email implements Serializable {
    private static final long serialVersionUID = -472039383711023610L;
    private String from;
    private String to;
    private String subject;
    private String text;

    public static Email getInstance(final String from, final String to, final String subject, final String text) {
      final Email email = new Email();
      email.from = from;
      email.to = to;
      email.subject = subject;
      email.text = text;
      return email;
    }
  }
}
