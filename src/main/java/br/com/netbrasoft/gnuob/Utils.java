package br.com.netbrasoft.gnuob;

import static br.com.netbrasoft.gnuob.ApplicationConstants.BEARER;
import static br.com.netbrasoft.gnuob.ApplicationConstants.SECRET_KEY;
import static io.jsonwebtoken.Jwts.builder;
import static io.jsonwebtoken.Jwts.parser;
import static io.jsonwebtoken.SignatureAlgorithm.HS512;
import static java.lang.System.currentTimeMillis;
import static org.apache.commons.lang3.ObjectUtils.allNotNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.join;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

import java.util.Date;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

public final class Utils {
  public static String getUsername() {
    return hasUserDetails() ? getUserNameFromUserDetails() : EMPTY;
  }

  private static boolean hasUserDetails() {
    return hasContext() && hasAuthentication() && hasDetails() && getDetails() instanceof UserDetails;
  }

  private static boolean hasContext() {
    return allNotNull(getContext());
  }

  private static boolean hasAuthentication() {
    return allNotNull(getAuthentication());
  }

  private static Authentication getAuthentication() {
    return getContext().getAuthentication();
  }

  private static boolean hasDetails() {
    return allNotNull(getDetails());
  }

  private static Object getDetails() {
    return getAuthentication().getDetails();
  }

  private static String getUserNameFromUserDetails() {
    return ((UserDetails) getDetails()).getUsername();
  }

  public static String createToken(final String username) {
    return join(BEARER, builder().setSubject(username).setExpiration(new Date(currentTimeMillis() + 86_400_000L)).signWith(HS512, SECRET_KEY.getBytes()).compact());
  }

  public static String extractUsername(final String token) {
    return parser().setSigningKey(SECRET_KEY.getBytes()).parseClaimsJws(token.replace(BEARER, EMPTY)).getBody().getSubject();
  }

  private Utils() {}
}
