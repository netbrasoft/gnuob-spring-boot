package br.com.netbrasoft.gnuob.document;

import br.com.netbrasoft.gnuob.type.TypeView;

public interface DocumentView extends TypeView {
  public interface Transfer extends TypeView.Transfer {
  }
}
