package br.com.netbrasoft.gnuob.document;

import br.com.netbrasoft.gnuob.type.ITypeRepository;

public interface IDocumentRepository<D extends Document> extends ITypeRepository<D> {
}
