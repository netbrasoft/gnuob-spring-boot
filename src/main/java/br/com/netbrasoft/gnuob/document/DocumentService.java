package br.com.netbrasoft.gnuob.document;

import static br.com.netbrasoft.gnuob.document.Document.DOCUMENT_EXCLUDE_FIELDS;
import static org.springframework.messaging.core.AbstractMessageSendingTemplate.CONVERSION_HINT_HEADER;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.netbrasoft.gnuob.type.AbstractTypeService;
import br.com.netbrasoft.gnuob.type.ITypeRepository;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@Service
public class DocumentService<D extends Document> extends AbstractTypeService<D> {
  @Autowired(required = true)
  protected IDocumentRepository<D> documentRepository;

  @Override
  public ITypeRepository<D> getTypeRepository() {
    return documentRepository;
  }

  @Override
  public String getPath() {
    return Document.class.getSimpleName().toLowerCase();
  }

  @Override
  public String[] getIgnorePats() {
    return DOCUMENT_EXCLUDE_FIELDS;
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "document", condition = "#type.getId() > 0"),
      @CacheEvict(value = "documents", allEntries = true, condition = "#type.getId() > 0")
    }
  )
  //@formatter:on
  public void delete(D type) {
    super.delete(type);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "document", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "documents", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public void deleteAll(List<D> types) {
    super.deleteAll(types);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "documents", allEntries = true, condition = "#type.getId() > 0"),
    },
    put = {
      @CachePut(value = "document", condition = "#type.getId() > 0"),
    }
  )
  //@formatter:on
  public D save(D type) {
    return super.save(type);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "document", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "documents", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public List<D> saveAll(List<D> types) {
    return super.saveAll(types);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "document", sync = true, condition = "#type.getId() > 0")
  //@formatter:on
  public D findOne(D type) {
    return super.findOne(type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "documents", sync = true)
  //@formatter:on
  public Page<D> findAll(int page, int size, Direction direction, List<String> properties, D type) {
    return super.findAll(page, size, direction, properties, type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "documents", sync = true)
  //@formatter:on
  public Page<D> findAll(int page, int size, Direction direction, List<String> properties, TypeFilter<D> filter) {
    return super.findAll(page, size, direction, properties, filter);
  }

  @Override
  public Map<String, Object> getHeaders() {
    final Map<String, Object> headers = super.getHeaders();
    headers.replace(CONVERSION_HINT_HEADER, DocumentView.Transfer.class);
    return headers;
  }
}
