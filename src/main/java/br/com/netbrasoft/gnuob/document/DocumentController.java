package br.com.netbrasoft.gnuob.document;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.netbrasoft.gnuob.type.IController;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@RestController
@CrossOrigin(maxAge = 3600)
public class DocumentController<D extends Document> implements IController<D> {
  @Autowired(required = true)
  private ITypeService<D> documentService;

  @Override
  public ITypeService<D> getService() {
    return documentService;
  }

  @Override
  @DeleteMapping(value = "/document")
  public void delete(@RequestBody final D type) {
    IController.super.delete(type);
  }

  @Override
  @DeleteMapping(value = "/document/all")
  public void deleteAll(@RequestBody final List<D> types) {
    IController.super.deleteAll(types);
  }

  @Override
  @PostMapping(value = "/document")
  @JsonView({DocumentView.Transfer.class})
  public D save(@RequestBody final D type) {
    return IController.super.save(type);
  }

  @Override
  @PostMapping(value = "/document/all")
  @JsonView({DocumentView.Transfer.class})
  public List<D> saveAll(@RequestBody final List<D> types) {
    return IController.super.saveAll(types);
  }

  @Override
  @PostMapping(value = "/document/of/one")
  @JsonView({DocumentView.Transfer.class})
  public D findOne(@RequestBody final D type) {
    return IController.super.findOne(type);
  }

  @Override
  @JsonView({DocumentView.Transfer.class})
  @PostMapping(value = "/document/of/all")
  public List<D> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final D type, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, type, response);
  }

  @Override
  @JsonView({DocumentView.Transfer.class})
  @PostMapping(value = "/document/of/all/filter")
  public List<D> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final TypeFilter<D> filter, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, filter, response);
  }
}
