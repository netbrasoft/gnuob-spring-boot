package br.com.netbrasoft.gnuob.document;

import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.type.AbstractActiveType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "code")})
public class Document extends AbstractActiveType {
  private static final long serialVersionUID = 1462570772724221506L;
  protected static final String[] DOCUMENT_EXCLUDE_FIELDS = addAll(TYPE_ACTIVE_EXCLUDE_FIELDS);
  private String content;

  public static Document getInstance() {
    return new Document();
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, DOCUMENT_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, DOCUMENT_EXCLUDE_FIELDS);
  }

  @Lob
  @Column
  @JsonView({DocumentView.Transfer.class})
  public String getContent() {
    return content;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, DOCUMENT_EXCLUDE_FIELDS);
  }
}
