package br.com.netbrasoft.gnuob;

import static br.com.netbrasoft.gnuob.ApplicationConstants.ALLOWED_ORIGINS;
import static br.com.netbrasoft.gnuob.ApplicationConstants.APPLICATION_JSON;
import static br.com.netbrasoft.gnuob.ApplicationConstants.AUTHORIZATION;
import static br.com.netbrasoft.gnuob.ApplicationConstants.BR_COM_NETBRASOFT_GNUOB;
import static br.com.netbrasoft.gnuob.ApplicationConstants.CONTENT_TYPE;
import static br.com.netbrasoft.gnuob.ApplicationConstants.ENCODING_UTF_8;
import static br.com.netbrasoft.gnuob.ApplicationConstants.INVALID_USERNAME_OR_PASSWORD;
import static br.com.netbrasoft.gnuob.ApplicationConstants.METHOD_ALLOWED_HEADER;
import static br.com.netbrasoft.gnuob.ApplicationConstants.METHOD_AUTHORIZATION;
import static br.com.netbrasoft.gnuob.ApplicationConstants.METHOD_DELETE;
import static br.com.netbrasoft.gnuob.ApplicationConstants.METHOD_GET;
import static br.com.netbrasoft.gnuob.ApplicationConstants.METHOD_HEAD;
import static br.com.netbrasoft.gnuob.ApplicationConstants.METHOD_LOCATION;
import static br.com.netbrasoft.gnuob.ApplicationConstants.METHOD_OPTIONS;
import static br.com.netbrasoft.gnuob.ApplicationConstants.METHOD_PATCH;
import static br.com.netbrasoft.gnuob.ApplicationConstants.METHOD_POST;
import static br.com.netbrasoft.gnuob.ApplicationConstants.METHOD_PUT;
import static br.com.netbrasoft.gnuob.ApplicationConstants.METHOD_X_TOTAL_COUNT;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_APP;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_ASSETS;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_CSS;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_EOT;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_FAVICON_ICO;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_GS_GUIDE_WEBSOCKET;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_H2_CONSOLE;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_HTML;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_INDEX_HTML;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_JS;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_LOGIN;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_LOGOUT;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_MAP;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_ORIGIN;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_SVG;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_TOPIC;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_TTF;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_USER_FORGOT_PASSWORD;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_USER_REGISTER;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_WEBMANIFEST;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_WOFF;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_WOFF2;
import static br.com.netbrasoft.gnuob.ApplicationConstants.POST;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PROTOCOL_AJP_1_3;
import static br.com.netbrasoft.gnuob.ApplicationConstants.USERNAME;
import static br.com.netbrasoft.gnuob.Utils.createToken;
import static br.com.netbrasoft.gnuob.Utils.extractUsername;
import static br.com.netbrasoft.gnuob.Utils.getUsername;
import static com.fasterxml.jackson.databind.MapperFeature.DEFAULT_VIEW_INCLUSION;
import static com.google.common.collect.Lists.newArrayList;
import static java.lang.System.getProperty;
import static java.util.concurrent.TimeUnit.SECONDS;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;
import static org.apache.commons.lang3.BooleanUtils.isTrue;
import static org.apache.commons.lang3.ObjectUtils.allNotNull;
import static org.apache.commons.lang3.StringUtils.trim;
import static org.slf4j.MDC.clear;
import static org.slf4j.MDC.put;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.CacheControl.maxAge;
import static org.springframework.http.CacheControl.noStore;
import static org.springframework.messaging.simp.stomp.StompCommand.CONNECT;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;
import static org.springframework.security.core.context.SecurityContextHolder.clearContext;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.MappedInterceptor;
import org.springframework.web.servlet.mvc.WebContentInterceptor;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import br.com.netbrasoft.gnuob.authentication.User;
import br.com.netbrasoft.gnuob.authentication.UserView;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.RepositoryImpl;
import br.com.netbrasoft.gnuob.type.TypeRuntimeException;
import de.rtner.security.auth.spi.SimplePBKDF2;
import liquibase.integration.spring.SpringLiquibase;

@EnableAutoConfiguration
@EnableCaching
@EnableJms
@EnableScheduling
@EnableWebSecurity
@EnableWebSocketMessageBroker()
@ComponentScan(BR_COM_NETBRASOFT_GNUOB)
@EnableJpaRepositories(repositoryBaseClass = RepositoryImpl.class)
@SpringBootApplication
public class Application {
  //@formatter:off
	private static final String[] JWT_MATCHERS = {
		PATH_ASSETS,
		PATH_CSS,
		PATH_FAVICON_ICO,
		PATH_GS_GUIDE_WEBSOCKET,
		PATH_H2_CONSOLE,
		PATH_JS,
		PATH_EOT,
		PATH_HTML,
		PATH_MAP,
		PATH_SVG,
		PATH_TTF,
		PATH_WEBMANIFEST,
		PATH_WOFF,
		PATH_WOFF2,
		PATH_USER_FORGOT_PASSWORD,
		PATH_USER_REGISTER,
		PATH, 
	};
	//@formatter:on
  @Autowired(required = true)
  private ITypeService<User> userService;
  @Value("${stomp.authentication:true}")
  private Boolean stompAuthentication;
  @Autowired(required = true)
  private DataSource dataSource;

  public static void main(final String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Configuration
  public class MappedDiagnosticContextConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
      registry.addInterceptor(createHandlerInterceptor());
      WebMvcConfigurer.super.addInterceptors(registry);
    }

    private HandlerInterceptor createHandlerInterceptor() {
      return new HandlerInterceptor() {
        @Override
        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
          put(USERNAME, getUsername());
          return HandlerInterceptor.super.preHandle(request, response, handler);
        }

        @Override
        public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
          clear();
          HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
        }
      };
    }
  }

  @Configuration
  @EnableGlobalMethodSecurity(prePostEnabled = true)
  public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {
    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
      final DefaultMethodSecurityExpressionHandler expressionHandler = new DefaultMethodSecurityExpressionHandler();
      expressionHandler.setPermissionEvaluator(createPermissionEvaluator());
      return expressionHandler;
    }

    private PermissionEvaluator createPermissionEvaluator() {
      return new PermissionEvaluator() {

        @Override
        public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
          return switch ((String) permission) {
            default -> isRoot(authentication);
          };
        }

        private Boolean isRoot(final Authentication authentication) {
          return isTrue(((User) authentication.getPrincipal()).getRoot());
        }

        @Override
        public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
          return false;
        }
      };
    }
  }

  @Configuration
  public class JWSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      auth.userDetailsService((UserDetailsService) userService).passwordEncoder(createPasswordEncoder());
    }

    private PasswordEncoder createPasswordEncoder() {
      return new PasswordEncoder() {
        @Override
        public String encode(final CharSequence rawPassword) {
          return trim(rawPassword.toString());
        }

        @Override
        public boolean matches(final CharSequence rawPassword, final String encodedPassword) {
          return new SimplePBKDF2().verifyKeyFormatted(encodedPassword, encode(rawPassword));
        }
      };
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      //@formatter:off
			http
				.headers()
				.frameOptions()
				.sameOrigin()
				.and()
				.cors()
				.and()
				.csrf()
				.disable()
				.authorizeRequests()
				.antMatchers(JWT_MATCHERS)
				.permitAll()
				.anyRequest()
				.authenticated()
				.and()
				.logout()
				.logoutRequestMatcher(createLogoutRequestMatcher())
				.addLogoutHandler(createLogoutHandler())
				.logoutSuccessHandler(createLogoutSuccessHandler())
				.and()
				.addFilterBefore(createInternalServerErrorOncePerRequestFilter(), UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(createTokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(createUsernamePasswordAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
				.sessionManagement()
				.sessionCreationPolicy(STATELESS);
			//@formatter:on
    }

    private AntPathRequestMatcher createLogoutRequestMatcher() {
      return new AntPathRequestMatcher(PATH_LOGOUT);
    }

    private LogoutHandler createLogoutHandler() {
      return (request, response, authentication) -> {
        final User user = (User) getAuthentication(extractUsername(extractToken(request)));
        user.setOnline(false);
        userService.save(user);
      };
    }

    private String extractToken(final HttpServletRequest request) {
      return request.getHeader(AUTHORIZATION);
    }

    private LogoutSuccessHandler createLogoutSuccessHandler() {
      return (request, response, authentication) -> clearContext();
    }

    private OncePerRequestFilter createInternalServerErrorOncePerRequestFilter() {
      return new OncePerRequestFilter() {
        @Override
        protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
          try {
            filterChain.doFilter(request, response);
          } catch (Exception e) {
            response.setStatus(SC_INTERNAL_SERVER_ERROR);
            response.addHeader(CONTENT_TYPE, APPLICATION_JSON);
            response.getWriter().write(new ObjectMapper().writeValueAsString(e.getCause()));
          }
        }
      };
    }

    private BasicAuthenticationFilter createTokenAuthenticationFilter() {
      try {
        return new BasicAuthenticationFilter(JWSecurityConfig.this.authenticationManager()) {
          @Override
          protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain) throws IOException, ServletException {
            try {
              if (allNotNull(request.getHeader(AUTHORIZATION))) {
                onSuccessfulAuthentication(request, response, getAuthentication(extractUsername(extractToken(request))));
              }
              chain.doFilter(request, response);
            } catch (AuthenticationException e) {
              onUnsuccessfulAuthentication(request, response, e);
            }
          }

          @Override
          protected void onSuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) throws IOException {
            authResult.setAuthenticated(((User) authResult).getActive());
            getContext().setAuthentication(authResult);
          }

          @Override
          protected void onUnsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException {
            response.setStatus(SC_UNAUTHORIZED);
            response.addHeader(CONTENT_TYPE, APPLICATION_JSON);
            response.getWriter().write(new ObjectMapper().writeValueAsString(new TypeRuntimeException(failed.getMessage())));
          }
        };
      } catch (Exception e) {
        throw new TypeRuntimeException(e.getMessage(), e);
      }
    }

    private AbstractAuthenticationProcessingFilter createUsernamePasswordAuthenticationFilter() {
      return new AbstractAuthenticationProcessingFilter(new AntPathRequestMatcher(PATH_LOGIN, POST)) {
        @Override
        public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
          try {
            return JWSecurityConfig.this.authenticationManager().authenticate(createUsernamePasswordAuthenticationToken(getUserFromRequest(request)));
          } catch (final Exception e) {
            throw new BadCredentialsException(INVALID_USERNAME_OR_PASSWORD, e);
          }
        }

        private UsernamePasswordAuthenticationToken createUsernamePasswordAuthenticationToken(final User user) {
          return new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), newArrayList());
        }

        private User getUserFromRequest(HttpServletRequest request) throws IOException {
          return new ObjectMapper().readValue(request.getInputStream(), User.class);
        }

        @Override
        protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
          final User user = (User) getAuthentication(((User) authResult.getPrincipal()).getUsername());
          user.setOnline(true);
          user.setToken(createToken(user.getUsername()));
          response.addHeader(AUTHORIZATION, user.getToken());
          response.addHeader(CONTENT_TYPE, APPLICATION_JSON);
          response.getWriter().write(createUserObjectWriter().writeValueAsString(userService.save(user)));
        }

        private ObjectWriter createUserObjectWriter() {
          return new ObjectMapper().disable(DEFAULT_VIEW_INCLUSION).writerWithView(UserView.Transfer.class);
        }

        @Override
        protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
          response.setStatus(SC_UNAUTHORIZED);
          response.addHeader(CONTENT_TYPE, APPLICATION_JSON);
          response.getWriter().write(new ObjectMapper().writeValueAsString(new TypeRuntimeException(failed.getMessage())));
        }
      };
    }
  }

  private Authentication getAuthentication(final String username) {
    return (Authentication) ((UserDetailsService) userService).loadUserByUsername(username);
  }

  @Configuration
  @Order(HIGHEST_PRECEDENCE + 99)
  public class WebSocketAuthorizationSecurityConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {

    @Override
    protected void configureInbound(final MessageSecurityMetadataSourceRegistry messages) {
      if (isTrue(Application.this.stompAuthentication)) {
        //@formatter:off
				messages
					.simpDestMatchers(PATH_GS_GUIDE_WEBSOCKET)
					.authenticated()
					.anyMessage()
					.authenticated(); 
				//@formatter:on
      } else {
        //@formatter:off
				messages
					.simpDestMatchers(PATH_GS_GUIDE_WEBSOCKET)
					.permitAll()
					.anyMessage()
					.permitAll(); 
				//@formatter:on
      }
    }

    @Override
    protected boolean sameOriginDisabled() {
      return true;
    }
  }

  @Configuration
  public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    @Override
    public void configureMessageBroker(final MessageBrokerRegistry config) {
      config.enableSimpleBroker(PATH_TOPIC);
      config.setApplicationDestinationPrefixes(PATH_APP);
    }

    @Override
    public void registerStompEndpoints(final StompEndpointRegistry registry) {
      registry.addEndpoint(PATH_GS_GUIDE_WEBSOCKET).setAllowedOrigins(ALLOWED_ORIGINS);
    }

    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
      registration.interceptors(createChannelInterceptor());
    }

    @Bean
    public ChannelInterceptor createChannelInterceptor() {
      return new ChannelInterceptor() {
        @Override
        public Message<?> preSend(Message<?> message, MessageChannel channel) {
          final StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
          if (accessor != null && CONNECT.equals(accessor.getCommand()) && allNotNull(accessor.getLogin(), accessor.getPasscode())) {
            final Authentication authResult = getAuthentication(accessor.getLogin());
            if (((User) authResult).getUsername().equals(extractUsername(accessor.getPasscode()))) {
              authResult.setAuthenticated(((User) authResult).getActive());
              accessor.setUser(authResult);
            }
          }
          return ChannelInterceptor.super.preSend(message, channel);
        }
      };
    }
  }

  @Bean
  CorsConfigurationSource corsConfigurationSource() {
    final CorsConfiguration config = new CorsConfiguration();
    config.setAllowCredentials(true);
    config.addAllowedOrigin(METHOD_ALLOWED_HEADER);
    config.addAllowedHeader(METHOD_ALLOWED_HEADER);
    config.addAllowedMethod(METHOD_OPTIONS);
    config.addAllowedMethod(METHOD_HEAD);
    config.addAllowedMethod(METHOD_GET);
    config.addAllowedMethod(METHOD_PUT);
    config.addAllowedMethod(METHOD_POST);
    config.addAllowedMethod(METHOD_DELETE);
    config.addAllowedMethod(METHOD_PATCH);
    config.addExposedHeader(METHOD_LOCATION);
    config.addExposedHeader(METHOD_AUTHORIZATION);
    config.addExposedHeader(METHOD_X_TOTAL_COUNT);
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration(PATH_ORIGIN, config);
    return source;
  }

  @Bean
  public MappedInterceptor mappedInterceptor() {
    WebContentInterceptor webContentInterceptor = new WebContentInterceptor();
    webContentInterceptor.setCacheControl(maxAge(0, SECONDS).cachePublic());
    webContentInterceptor.addCacheMapping(noStore().mustRevalidate(), PATH_INDEX_HTML);
    return new MappedInterceptor(null, webContentInterceptor);
  }

  @Bean
  public TomcatServletWebServerFactory tomcatServletWebServerFactory() {
    final TomcatServletWebServerFactory tomcatServletWebServerFactory = new TomcatServletWebServerFactory();
    final Connector ajpConnector = new Connector(PROTOCOL_AJP_1_3);
    ajpConnector.setPort(8009);
    ajpConnector.setProperty("address", getProperty("address", "127.0.0.1"));
    ajpConnector.setSecure(true);
    ajpConnector.setAllowTrace(false);
    ajpConnector.setScheme("https");
    return tomcatServletWebServerFactory;
  }

  @Bean
  public LiquibaseProperties liquibaseProperties() {
    final LiquibaseProperties liquibaseProperties = new LiquibaseProperties();
    liquibaseProperties.setChangeLog("classpath:/db/changelog/db.changelog-master.xml");
    return liquibaseProperties;
  }

  @Bean
  @DependsOn(value = "entityManagerFactory")
  public SpringLiquibase liquibase() {
    SpringLiquibase liquibase = new SpringLiquibase();
    liquibase.setChangeLog(liquibaseProperties().getChangeLog());
    liquibase.setContexts(liquibaseProperties().getContexts());
    liquibase.setLabels(liquibaseProperties().getLabels());
    liquibase.setDataSource(dataSource);
    liquibase.setDefaultSchema(liquibaseProperties().getDefaultSchema());
    liquibase.setDropFirst(liquibaseProperties().isDropFirst());
    liquibase.setShouldRun(true);
    liquibase.setLabels(liquibaseProperties().getLabels());
    liquibase.setChangeLogParameters(liquibaseProperties().getParameters());
    return liquibase;
  }

  @Bean
  public MessageSource messageSource() {
    final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setBasename("classpath:messages");
    messageSource.setDefaultEncoding(ENCODING_UTF_8);
    return messageSource;
  }
}
