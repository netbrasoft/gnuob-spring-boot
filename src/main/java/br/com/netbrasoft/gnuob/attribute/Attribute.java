package br.com.netbrasoft.gnuob.attribute;

import static javax.persistence.FetchType.EAGER;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.ObjectUtils.allNotNull;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ReflectionToStringBuilder.toStringExclude;
import static org.apache.commons.lang3.builder.ToStringBuilder.setDefaultStyle;
import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;

import org.hibernate.annotations.Cache;

import br.com.netbrasoft.gnuob.type.AbstractActiveType;
import lombok.Setter;

@Setter
@Cache(usage = READ_WRITE)
@Entity
public class Attribute extends AbstractActiveType {
  private static final long serialVersionUID = -5996141203181597450L;
  protected static final String[] ATTRIBUTE_EXCLUDE_FIELDS = addAll(TYPE_EXCLUDE_FIELDS, "parent");
  private Integer position;
  private Attribute parent;

  public static Attribute getInstance() {
    return new Attribute();
  }

  @Override
  public void prePersist() {
    setCode((allNotNull(getParent()) ? join(getParent().getCode(), " -> ", getName()) : getName()));
    super.prePersist();
  }

  @Override
  public void preUpdate() {
    setCode((allNotNull(getParent()) ? join(getParent().getCode(), " -> ", getName()) : getName()));
    super.preUpdate();
  }

  @Override
  public int hashCode() {
    return reflectionHashCode(this, ATTRIBUTE_EXCLUDE_FIELDS);
  }

  @Override
  public boolean equals(final Object obj) {
    return reflectionEquals(this, obj, ATTRIBUTE_EXCLUDE_FIELDS);
  }

  @Column
  @JsonView({AttributeView.Transfer.class})
  public Integer getPosition() {
    return position;
  }

  @Cache(usage = READ_WRITE)
  @ManyToOne(fetch = EAGER)
  @JsonView({AttributeView.Transfer.class})
  public Attribute getParent() {
    return parent;
  }

  @Override
  public String toString() {
    setDefaultStyle(JSON_STYLE);
    return toStringExclude(this, ATTRIBUTE_EXCLUDE_FIELDS);
  }
}
