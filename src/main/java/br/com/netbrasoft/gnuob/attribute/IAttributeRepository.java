package br.com.netbrasoft.gnuob.attribute;

import br.com.netbrasoft.gnuob.type.ITypeRepository;

public interface IAttributeRepository<A extends Attribute> extends ITypeRepository<A> {
}
