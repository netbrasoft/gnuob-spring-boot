package br.com.netbrasoft.gnuob.attribute;

import br.com.netbrasoft.gnuob.type.TypeView;

public interface AttributeView extends TypeView {
  public interface Transfer extends TypeView.Transfer {
  }
}
