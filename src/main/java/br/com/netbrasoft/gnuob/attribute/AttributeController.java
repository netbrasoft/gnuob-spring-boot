package br.com.netbrasoft.gnuob.attribute;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.netbrasoft.gnuob.type.IController;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@RestController
@CrossOrigin(maxAge = 3600)
public class AttributeController<A extends Attribute> implements IController<A> {
  @Autowired(required = true)
  private ITypeService<A> attributeService;

  @Override
  public ITypeService<A> getService() {
    return attributeService;
  }

  @Override
  @DeleteMapping(value = "/attribute")
  public void delete(@RequestBody final A type) {
    IController.super.delete(type);
  }

  @Override
  @DeleteMapping(value = "/attribute/all")
  public void deleteAll(@RequestBody final List<A> types) {
    IController.super.deleteAll(types);
  }

  @Override
  @PostMapping(value = "/attribute")
  @JsonView({AttributeView.Transfer.class})
  public A save(@RequestBody final A type) {
    return IController.super.save(type);
  }

  @Override
  @PostMapping(value = "/attribute/all")
  @JsonView({AttributeView.Transfer.class})
  public List<A> saveAll(@RequestBody final List<A> types) {
    return IController.super.saveAll(types);
  }

  @Override
  @PostMapping(value = "/attribute/of/one")
  @JsonView({AttributeView.Transfer.class})
  public A findOne(@RequestBody final A type) {
    return IController.super.findOne(type);
  }

  @Override
  @JsonView({AttributeView.Transfer.class})
  @PostMapping(value = "/attribute/of/all")
  public List<A> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final A type, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, type, response);
  }

  @Override
  @JsonView({AttributeView.Transfer.class})
  @PostMapping(value = "/attribute/of/all/filter")
  public List<A> findAll(@RequestParam final int page, @RequestParam final int size, @RequestParam final Direction direction, @RequestParam final List<String> properties, @RequestBody final TypeFilter<A> filter, final HttpServletResponse response) {
    return IController.super.findAll(page, size, direction, properties, filter, response);
  }
}
