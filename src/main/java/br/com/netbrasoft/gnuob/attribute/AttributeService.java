package br.com.netbrasoft.gnuob.attribute;

import static org.springframework.messaging.core.AbstractMessageSendingTemplate.CONVERSION_HINT_HEADER;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.netbrasoft.gnuob.type.AbstractTypeService;
import br.com.netbrasoft.gnuob.type.ITypeRepository;
import br.com.netbrasoft.gnuob.type.TypeFilter;

@Service
public class AttributeService<A extends Attribute> extends AbstractTypeService<A> {
  @Autowired(required = true)
  protected IAttributeRepository<A> attributeRepository;

  @Override
  public ITypeRepository<A> getTypeRepository() {
    return attributeRepository;
  }

  @Override
  public String getPath() {
    return Attribute.class.getSimpleName().toLowerCase();
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "attribute", condition = "#type.getId() > 0"),
      @CacheEvict(value = "attributes", allEntries = true, condition = "#type.getId() > 0")
    }
  )
  //@formatter:on
  public void delete(A type) {
    super.delete(type);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "attribute", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "attributes", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public void deleteAll(List<A> types) {
    super.deleteAll(types);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "attributes", allEntries = true, condition = "#type.getId() > 0"),
    },
    put = {
      @CachePut(value = "attribute", condition = "#type.getId() > 0"),
    }
  )
  //@formatter:on
  public A save(A type) {
    return super.save(type);
  }

  @Override
  //@formatter:off
  @Caching(
    evict = {
      @CacheEvict(value = "attribute", allEntries = true, condition = "#types.size() > 0"),
      @CacheEvict(value = "attributes", allEntries = true, condition = "#types.size() > 0")
    }
  )
  //@formatter:on
  public List<A> saveAll(List<A> types) {
    return super.saveAll(types);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "attribute", sync = true, condition = "#type.getId() > 0")
  //@formatter:on
  public A findOne(A type) {
    return super.findOne(type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "attributes", sync = true)
  //@formatter:on
  public Page<A> findAll(int page, int size, Direction direction, List<String> properties, A type) {
    return super.findAll(page, size, direction, properties, type);
  }

  @Override
  //@formatter:off
  @Cacheable(value = "attributes", sync = true)
  //@formatter:on
  public Page<A> findAll(int page, int size, Direction direction, List<String> properties, TypeFilter<A> filter) {
    return super.findAll(page, size, direction, properties, filter);
  }

  @Override
  public Map<String, Object> getHeaders() {
    final Map<String, Object> headers = super.getHeaders();
    headers.replace(CONVERSION_HINT_HEADER, AttributeView.Transfer.class);
    return headers;
  }
}
