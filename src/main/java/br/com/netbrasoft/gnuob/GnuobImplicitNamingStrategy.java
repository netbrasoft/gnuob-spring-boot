package br.com.netbrasoft.gnuob;

import static org.apache.commons.lang3.StringUtils.join;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitForeignKeyNameSource;
import org.hibernate.boot.model.naming.ImplicitIndexNameSource;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyLegacyJpaImpl;
import org.hibernate.boot.model.naming.ImplicitUniqueKeyNameSource;

public class GnuobImplicitNamingStrategy extends ImplicitNamingStrategyLegacyJpaImpl {
  private static final long serialVersionUID = 2647725043606611844L;

  @Override
  public Identifier determineForeignKeyName(ImplicitForeignKeyNameSource source) {
    return toIdentifier(join("FK_", source.getTableName().getText(), "_", String.join("_", source.getColumnNames().stream().map(Identifier::getText).toList())).toUpperCase(), source.getBuildingContext());
  }

  @Override
  public Identifier determineUniqueKeyName(ImplicitUniqueKeyNameSource source) {
    return toIdentifier(join("UK_", source.getTableName().getText(), "_", String.join("_", source.getColumnNames().stream().map(Identifier::getText).toList())).toUpperCase(), source.getBuildingContext());
  }

  @Override
  public Identifier determineIndexName(ImplicitIndexNameSource source) {
    return toIdentifier(join("IDX_", source.getTableName().getText(), "_", String.join("_", source.getColumnNames().stream().map(Identifier::getText).toList())).toUpperCase(), source.getBuildingContext());
  }
}
