INSERT INTO "PUBLIC"."GNUOB_ATTRIBUTE"("ID",
                            "CODE",
                            "CREATED",
                            "DESCRIPTION",
                            "DTYPE",
                            "MODIFIED",
                            "NAME",
                            "USER_CREATED",
                            "USER_MODIFIED",
                            "VERSION",
                            "POSITION",
                            "PARENT_ID",
                            "ACTIVE")
VALUES(1000, 'code', CURRENT_TIMESTAMP, 'description', 'attribute', CURRENT_TIMESTAMP, 'Bar Foo', '', '', 0, null, null, true);