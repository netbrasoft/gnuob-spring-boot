INSERT INTO "PUBLIC"."GNUOB_SETTING"("ID",
                            "CODE",
                            "CREATED",
                            "DESCRIPTION",
                            "DTYPE",
                            "MODIFIED",
                            "NAME",
                            "USER_CREATED",
                            "USER_MODIFIED",
                            "VERSION",
                            "VALUE",
                            "TYPE",
                            "ACTIVE")
VALUES(1000, 'code', CURRENT_TIMESTAMP, 'description', 'setting', CURRENT_TIMESTAMP, 'Bar Foo', '', '', 0, 'value', 'STRING', true);