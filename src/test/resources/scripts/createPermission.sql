INSERT INTO "PUBLIC"."GNUOB_PERMISSION"("ID",
                            "CODE",
                            "CREATED",
                            "DESCRIPTION",
                            "DTYPE",
                            "MODIFIED",
                            "NAME",
                            "USER_CREATED",
                            "USER_MODIFIED",
                            "VERSION",
                            "ACTIVE")
VALUES(1000, 'code', CURRENT_TIMESTAMP, 'description', 'permission', CURRENT_TIMESTAMP, 'Bar Foo', '', '', 0, true);