INSERT INTO "PUBLIC"."GNUOB_ROLE"("ID",
                            "CODE",
                            "CREATED",
                            "DESCRIPTION",
                            "DTYPE",
                            "MODIFIED",
                            "NAME",
                            "USER_CREATED",
                            "USER_MODIFIED",
                            "VERSION",
                            "ACTIVE")
VALUES(1000, 'code', CURRENT_TIMESTAMP, 'description', 'role', CURRENT_TIMESTAMP, 'Bar Foo', '', '', 0, true);