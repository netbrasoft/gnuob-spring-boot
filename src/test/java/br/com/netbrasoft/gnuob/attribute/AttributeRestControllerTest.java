package br.com.netbrasoft.gnuob.attribute;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.com.netbrasoft.gnuob.type.AbstractTypeRestControllerTest;
import br.com.netbrasoft.gnuob.type.ITypeService;

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "spring.mail.test-connection=false"})
class AttributeRestControllerTest extends AbstractTypeRestControllerTest<Attribute> {
  private final static Attribute NEW_ATTRIBUTE = Attribute.getInstance();
  @MockBean
  private AttributeService<Attribute> mockAttributeService;

  @Override
  @BeforeEach
  public void setup() {
    super.setup();
    NEW_ATTRIBUTE.setActive(true);
    NEW_ATTRIBUTE.setCode("code");
    NEW_ATTRIBUTE.setDescription("description");
    NEW_ATTRIBUTE.setId(0);
    NEW_ATTRIBUTE.setName("Bar Foo");
    NEW_ATTRIBUTE.setParent(null);
    NEW_ATTRIBUTE.setVersion(0);
  }

  @Override
  @AfterEach
  public void setDown() {}

  @Override
  public Attribute getType() {
    return NEW_ATTRIBUTE;
  }

  @Override
  public ITypeService<Attribute> getTypeMockService() {
    return (ITypeService<Attribute>) mockAttributeService;
  }

  @Override
  public Class<Attribute> getClassType() {
    return Attribute.class;
  }

  @Override
  public String getPath() {
    return "/attribute";
  }

  @Override
  public Class<Attribute[]> getClassTypes() {
    return Attribute[].class;
  }

  @Test
  void saveShouldReturnAttribute() {
    super.saveShouldReturnType();
  }

  @Test
  public void wrongSaveShouldReturnServiceError() {
    super.wrongSaveShouldReturnServiceError();
  }

  @Test
  void saveAllShouldReturnAttributes() {
    super.saveAllShouldReturnTypes();
  }

  @Test
  public void wrongSaveAllShouldReturnServiceError() {
    super.wrongSaveAllShouldReturnServiceError();
  }

  @Test
  void deleteShouldNotReturnAttribute() {
    super.deleteShouldNotReturnType();
  }

  @Test
  public void wrongDeleteShouldReturnServiceError() {
    super.wrongDeleteShouldReturnServiceError();
  }

  @Test
  void deleteAllShouldNotReturnAttributes() {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  public void wrongDeleteAllShouldReturnServiceError() {
    super.wrongDeleteAllShouldReturnServiceError();
  }

  @Test
  void findOneShouldReturnAttribute() {
    super.findOneShouldReturnType();
  }

  @Test
  public void wrongFindOneShouldReturnServiceError() {
    super.wrongFindOneShouldReturnServiceError();
  }

  @Test
  void findAllShouldReturnAttributes() {
    super.findAllShouldReturnTypes();
  }

  @Test
  public void wrongFindAllShouldReturnServiceError() {
    super.wrongFindAllShouldReturnServiceError();
  }

  @Test
  void findAllFilterShouldReturnAttributes() {
    super.findAllFilterShouldReturnTypes();
  }

  @Test
  public void wrongFindAllFilterShouldReturnServiceError() {
    super.wrongFindAllFilterShouldReturnServiceError();
  }
}
