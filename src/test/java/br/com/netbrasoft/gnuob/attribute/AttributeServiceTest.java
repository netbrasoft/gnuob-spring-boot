package br.com.netbrasoft.gnuob.attribute;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.lang.reflect.Type;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.transaction.Transactional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import br.com.netbrasoft.gnuob.type.AbstractTypeServiceTest;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeTopic;

@Transactional
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "stomp.authentication=false", "spring.mail.test-connection=false", "spring.liquibase.contexts:test"})
class AttributeServiceTest extends AbstractTypeServiceTest<Attribute> {
  static class AttributeTypeTopic extends TypeTopic<Attribute> {
  }

  @Autowired(required = true)
  private ITypeService<Attribute> attributeService;
  private final Attribute NEW_ATTRIBUTE = Attribute.getInstance();

  @Override
  public Attribute getType() {
    return NEW_ATTRIBUTE;
  }

  @Override
  public ITypeService<Attribute> getTypService() {
    return attributeService;
  }

  @Override
  public Type getTypePayloadType() {
    return AttributeTypeTopic.class;
  }

  @Override
  public String getTypeSimpleName() {
    return Attribute.class.getSimpleName();
  }

  @BeforeEach
  public void setup() throws InterruptedException, ExecutionException, TimeoutException {
    super.setup();
    NEW_ATTRIBUTE.setActive(true);
    NEW_ATTRIBUTE.setCode("code");
    NEW_ATTRIBUTE.setDescription("description");
    NEW_ATTRIBUTE.setId(0);
    NEW_ATTRIBUTE.setName("Bar Foo");
    NEW_ATTRIBUTE.setVersion(0);
    NEW_ATTRIBUTE.setParent(null);
  }

  @AfterEach
  public void setDown() throws InterruptedException, ExecutionException {
    super.setDown();
  }

  @Test
  void saveCreateShouldReturnAttribute() throws InterruptedException, ExecutionException, TimeoutException {
    getType().setCode("Bar Foo");
    super.saveCreateShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createAttribute.sql"})
  void saveUpdateShouldReturnAttribute() throws InterruptedException, ExecutionException, TimeoutException {
    getType().setCode("Bar Foo");
    super.saveUpdateShouldReturnType();
  }

  @Test
  void saveAllShouldReturnAttributes() throws InterruptedException, ExecutionException, TimeoutException {
    getType().setCode("Bar Foo");
    super.saveAllShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createAttribute.sql"})
  void deleteShouldNotReturnAttribute() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteShouldNotReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createAttribute.sql"})
  void deleteAllShouldNotReturnAttributes() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createAttribute.sql"})
  void findOneByIdShouldReturnAttribute() {
    super.findOneByIdShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createAttribute.sql"})
  void findOneByExampleShouldReturnAttribute() {
    super.findOneByExampleShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createAttribute.sql"})
  public void findOneShouldReturnNoResultError() {
    super.findOneShouldReturnNoResultError();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createAttribute.sql"})
  void findAllShouldReturnAttributes() {
    super.findAllShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createAttribute.sql"})
  void findAllFilterShouldReturnAttributes() {
    super.findAllFilterShouldReturnTypes();
  }
}
