package br.com.netbrasoft.gnuob.document;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.com.netbrasoft.gnuob.type.AbstractTypeRestControllerTest;
import br.com.netbrasoft.gnuob.type.ITypeService;

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "spring.mail.test-connection=false"})
class DocumentRestControllerTest extends AbstractTypeRestControllerTest<Document> {
  private final static Document NEW_DOCUMENT = Document.getInstance();
  @MockBean
  private DocumentService<Document> mockDocumentService;

  @Override
  @BeforeEach
  public void setup() {
    super.setup();
    NEW_DOCUMENT.setActive(true);
    NEW_DOCUMENT.setCode("code");
    NEW_DOCUMENT.setDescription("description");
    NEW_DOCUMENT.setId(0);
    NEW_DOCUMENT.setName("Bar Foo");
    NEW_DOCUMENT.setVersion(0);
  }

  @Override
  @AfterEach
  public void setDown() {}

  @Override
  public Document getType() {
    return NEW_DOCUMENT;
  }

  @Override
  public ITypeService<Document> getTypeMockService() {
    return (ITypeService<Document>) mockDocumentService;
  }

  @Override
  public Class<Document> getClassType() {
    return Document.class;
  }

  @Override
  public String getPath() {
    return "/document";
  }

  @Override
  public Class<Document[]> getClassTypes() {
    return Document[].class;
  }

  @Test
  void saveShouldReturnDocument() {
    super.saveShouldReturnType();
  }

  @Test
  public void wrongSaveShouldReturnServiceError() {
    super.wrongSaveShouldReturnServiceError();
  }

  @Test
  void saveAllShouldReturnDocuments() {
    super.saveAllShouldReturnTypes();
  }

  @Test
  public void wrongSaveAllShouldReturnServiceError() {
    super.wrongSaveAllShouldReturnServiceError();
  }

  @Test
  void deleteShouldNotReturnDocument() {
    super.deleteShouldNotReturnType();
  }

  @Test
  public void wrongDeleteShouldReturnServiceError() {
    super.wrongDeleteShouldReturnServiceError();
  }

  @Test
  void deleteAllShouldNotReturnDocuments() {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  public void wrongDeleteAllShouldReturnServiceError() {
    super.wrongDeleteAllShouldReturnServiceError();
  }

  @Test
  void findOneShouldReturnDocument() {
    super.findOneShouldReturnType();
  }

  @Test
  public void wrongFindOneShouldReturnServiceError() {
    super.wrongFindOneShouldReturnServiceError();
  }

  @Test
  void findAllShouldReturnDocuments() {
    super.findAllShouldReturnTypes();
  }

  @Test
  public void wrongFindAllShouldReturnServiceError() {
    super.wrongFindAllShouldReturnServiceError();
  }

  @Test
  void findAllFilterShouldReturnDocuments() {
    super.findAllFilterShouldReturnTypes();
  }

  @Test
  public void wrongFindAllFilterShouldReturnServiceError() {
    super.wrongFindAllFilterShouldReturnServiceError();
  }
}
