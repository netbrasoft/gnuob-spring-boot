package br.com.netbrasoft.gnuob.document;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.lang.reflect.Type;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.transaction.Transactional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import br.com.netbrasoft.gnuob.type.AbstractTypeServiceTest;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeTopic;

@Transactional
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "stomp.authentication=false", "spring.mail.test-connection=false", "spring.liquibase.contexts:test"})
class DocumentServiceTest extends AbstractTypeServiceTest<Document> {
  static class DocumentTypeTopic extends TypeTopic<Document> {
  }

  @Autowired(required = true)
  private ITypeService<Document> documentService;
  private final Document NEW_DOCUMENT = Document.getInstance();


  @Override
  public Document getType() {
    return NEW_DOCUMENT;
  }

  @Override
  public ITypeService<Document> getTypService() {
    return documentService;
  }

  @Override
  public Type getTypePayloadType() {
    return DocumentTypeTopic.class;
  }

  @Override
  public String getTypeSimpleName() {
    return Document.class.getSimpleName();
  }

  @BeforeEach
  public void setup() throws InterruptedException, ExecutionException, TimeoutException {
    super.setup();
    NEW_DOCUMENT.setActive(true);
    NEW_DOCUMENT.setCode("code");
    NEW_DOCUMENT.setDescription("description");
    NEW_DOCUMENT.setId(0);
    NEW_DOCUMENT.setName("Bar Foo");
    NEW_DOCUMENT.setVersion(0);
    NEW_DOCUMENT.setContent("content");
  }

  @AfterEach
  public void setDown() throws InterruptedException, ExecutionException {
    super.setDown();
  }

  @Test
  void saveCreateShouldReturnDocument() throws InterruptedException, ExecutionException, TimeoutException {
    getType().setCode("Bar Foo");
    super.saveCreateShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createDocument.sql"})
  void saveUpdateShouldReturnDocument() throws InterruptedException, ExecutionException, TimeoutException {
    getType().setCode("Bar Foo");
    super.saveUpdateShouldReturnType();
  }

  @Test
  void saveAllShouldReturnDocuments() throws InterruptedException, ExecutionException, TimeoutException {
    getType().setCode("Bar Foo");
    super.saveAllShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createDocument.sql"})
  void deleteShouldNotReturnDocument() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteShouldNotReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createDocument.sql"})
  void deleteAllShouldNotReturnDocuments() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createDocument.sql"})
  void findOneByIdShouldReturnDocument() {
    super.findOneByIdShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createDocument.sql"})
  void findOneByExampleShouldReturnDocument() {
    super.findOneByExampleShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createDocument.sql"})
  public void findOneShouldReturnNoResultError() {
    super.findOneShouldReturnNoResultError();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createDocument.sql"})
  void findAllShouldReturnDocuments() {
    super.findAllShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createDocument.sql"})
  void findAllFilterShouldReturnDocuments() {
    super.findAllFilterShouldReturnTypes();
  }
}
