package br.com.netbrasoft.gnuob.authentication;

import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.SAVE;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.lang.reflect.Type;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeRuntimeException;
import br.com.netbrasoft.gnuob.type.TypeTopic;

@Transactional
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "stomp.authentication=false", "spring.mail.test-connection=false"})
class AuthenticationServiceTest extends AbstractAuthenticationServiceTest<User> {
  static class UserTypeTopic extends TypeTopic<User> {
  }

  private final User NEW_USER = User.getInstance("bar@foo.com");

  @Autowired(required = true)
  private IUserService<User> userService;

  @Override
  public User getType() {
    return NEW_USER;
  }

  @Override
  public ITypeService<User> getTypService() {
    return userService;
  }

  @Override
  public Type getTypePayloadType() {
    return UserTypeTopic.class;
  }

  @Override
  public String getTypeSimpleName() {
    return User.class.getSimpleName();
  }

  @BeforeEach
  public void setup() throws InterruptedException, ExecutionException, TimeoutException {
    super.setup();
    NEW_USER.setActive(true);
    NEW_USER.setChangePassword(true);
    NEW_USER.setCode("code");
    NEW_USER.setCookie("cookie");
    NEW_USER.setDescription("description");
    NEW_USER.setEmail("bar@foo.com");
    NEW_USER.setFirstName("Bar");
    NEW_USER.setLastName("Foo");
    NEW_USER.setUsername("bar@foo.com");
    NEW_USER.setId(0);
    NEW_USER.setMobile(true);
    NEW_USER.setMobileIpAddress("mobileIpAddress");
    NEW_USER.setMobileManufacturer("mobileManufacturer");
    NEW_USER.setMobileMaxDownload("mobileMaxDownload");
    NEW_USER.setMobileModel("mobileModel");
    NEW_USER.setMobileNetworkType("mobileNetworkType");
    NEW_USER.setMobilePackageName("mobilePackageName");
    NEW_USER.setMobilePlatform("mobilePlatform");
    NEW_USER.setMobileSerial("mobileSerial");
    NEW_USER.setMobileSignal("mobileSignal");
    NEW_USER.setMobileUuid("mobileUuid");
    NEW_USER.setMobileVersion("mobileVersion");
    NEW_USER.setMobileVersionCode("mobileVersionCode");
    NEW_USER.setMobileVersionNumber("mobileVersionNumber");
    NEW_USER.setName("Bar Foo");
    NEW_USER.setPassword("secret");
    NEW_USER.setRoot(true);
    NEW_USER.setVersion(0);
  }

  @AfterEach
  public void setDown() throws InterruptedException, ExecutionException {
    super.setDown();
  }

  @Test
  void registerShouldReturnUser() throws InterruptedException, ExecutionException, TimeoutException, MessagingException {
    final User attachedUser = userService.register(NEW_USER);
    final TypeTopic<User> userTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(getGreenMail().waitForIncomingEmail(10000L, 1)).isTrue();
    assertThat(attachedUser).isEqualTo(NEW_USER);
    assertThat(attachedUser.getId()).isPositive();
    assertThat(attachedUser.getVersion()).isZero();
    assertThat(attachedUser.getDtype()).isEqualTo(User.class.getSimpleName().toLowerCase());
    assertThat(attachedUser.getCreated()).isNotNull();
    assertThat(attachedUser.getModified()).isNotNull();
    assertThat(attachedUser.getUserCreated()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(attachedUser.getActive()).isFalse();
    assertThat(attachedUser.getRoot()).isFalse();
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(userTypeTopic.getType()).isEqualTo(NEW_USER);
    assertThat(userTypeTopic.getType().getActive()).isFalse();
    assertThat(userTypeTopic.getType().getRoot()).isFalse();
    assertThat(getGreenMail().getReceivedMessages()).isNotEmpty();
    assertThat(getGreenMail().getReceivedMessages()[0].getSubject()).isEqualTo("Confirm Register");
    assertThat(getGreenMail().getReceivedMessages()[0].getAllRecipients()).isNotEmpty();
    assertThat(((InternetAddress) getGreenMail().getReceivedMessages()[0].getAllRecipients()[0]).getAddress()).isEqualTo(NEW_USER.getEmail());
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  void registerShouldNotReturnUser() {
    final Throwable thrown = assertThrows(TypeRuntimeException.class, () -> userService.register(NEW_USER));
    assertThat(thrown.getMessage()).isEqualTo("Registration Failed");
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  void confirmRegisterShouldReturnUser() throws InterruptedException, ExecutionException, TimeoutException {
    final User attachedUser = userService.confirmRegister(NEW_USER);
    final TypeTopic<User> userTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(attachedUser).isEqualTo(NEW_USER);
    assertThat(attachedUser.getId()).isPositive();
    assertThat(attachedUser.getVersion()).isZero();
    assertThat(attachedUser.getDtype()).isEqualTo(User.class.getSimpleName().toLowerCase());
    assertThat(attachedUser.getCreated()).isNotNull();
    assertThat(attachedUser.getModified()).isNotNull();
    assertThat(attachedUser.getUserCreated()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(attachedUser.getActive()).isTrue();
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(userTypeTopic.getType()).isEqualTo(NEW_USER);
    assertThat(userTypeTopic.getType().getActive()).isTrue();
  }

  @Test
  void confirmRegisterShouldNotReturnUser() {
    final Throwable thrown = assertThrows(TypeRuntimeException.class, () -> userService.confirmRegister(NEW_USER));
    assertThat(thrown.getMessage()).isEqualTo("Confirm Registration Failed");
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  void forgotPasswordShouldReturnUser() throws InterruptedException, ExecutionException, TimeoutException, MessagingException {
    final User attachedUser = userService.forgotPassword(NEW_USER);
    final TypeTopic<User> userTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(getGreenMail().waitForIncomingEmail(10000L, 1)).isTrue();
    assertThat(attachedUser).isEqualTo(NEW_USER);
    assertThat(attachedUser.getId()).isPositive();
    assertThat(attachedUser.getVersion()).isEqualTo(1);
    assertThat(attachedUser.getDtype()).isEqualTo(User.class.getSimpleName().toLowerCase());
    assertThat(attachedUser.getCreated()).isNotNull();
    assertThat(attachedUser.getModified()).isNotNull();
    assertThat(attachedUser.getUserCreated()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(userTypeTopic.getType()).isEqualTo(NEW_USER);
    assertThat(getGreenMail().getReceivedMessages()).isNotEmpty();
    assertThat(getGreenMail().getReceivedMessages()[0].getSubject()).isEqualTo("Forgot Password");
    assertThat(getGreenMail().getReceivedMessages()[0].getAllRecipients()).isNotEmpty();
    assertThat(((InternetAddress) getGreenMail().getReceivedMessages()[0].getAllRecipients()[0]).getAddress()).isEqualTo(NEW_USER.getEmail());
  }

  @Test
  void forgotPasswordShouldNotReturnUser() {
    final Throwable thrown = assertThrows(TypeRuntimeException.class, () -> userService.forgotPassword(NEW_USER));
    assertThat(thrown.getMessage()).isEqualTo("Forgot Password Failed");
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  void confirmForgotPasswordShouldReturnUser() throws InterruptedException, ExecutionException, TimeoutException {
    final User attachedUser = userService.confirmForgotPassword(NEW_USER);
    final TypeTopic<User> userTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(attachedUser).isEqualTo(NEW_USER);
    assertThat(attachedUser.getId()).isPositive();
    assertThat(attachedUser.getVersion()).isEqualTo(1);
    assertThat(attachedUser.getDtype()).isEqualTo(User.class.getSimpleName().toLowerCase());
    assertThat(attachedUser.getCreated()).isNotNull();
    assertThat(attachedUser.getModified()).isNotNull();
    assertThat(attachedUser.getUserCreated()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(userTypeTopic.getType()).isEqualTo(NEW_USER);
  }

  @Test
  void confirmForgotPasswordShouldNotReturnUser() {
    final Throwable thrown = assertThrows(TypeRuntimeException.class, () -> userService.confirmForgotPassword(NEW_USER));
    assertThat(thrown.getMessage()).isEqualTo("Confirm Forgot Password Failed");
  }
}
