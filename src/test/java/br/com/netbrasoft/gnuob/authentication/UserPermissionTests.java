package br.com.netbrasoft.gnuob.authentication;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UserPermissionTests {
  private final static UserPermission NEW_USER_PERMISSION = UserPermission.getInstance(null);

  @BeforeEach
  public void setup() {
    NEW_USER_PERMISSION.setId(0);
    NEW_USER_PERMISSION.setVersion(0);
  }

  @Test
  void testPrePersist() {
    NEW_USER_PERMISSION.setCanCreate(null);
    NEW_USER_PERMISSION.setCanRead(null);
    NEW_USER_PERMISSION.setCanUpdate(null);
    NEW_USER_PERMISSION.setCanDelete(null);
    NEW_USER_PERMISSION.prePersist();
    assertThat(NEW_USER_PERMISSION.getCanCreate()).isTrue();
    assertThat(NEW_USER_PERMISSION.getCanRead()).isTrue();
    assertThat(NEW_USER_PERMISSION.getCanUpdate()).isTrue();
    assertThat(NEW_USER_PERMISSION.getCanDelete()).isTrue();
  }

  @Test
  void testPreUpdate() {
    NEW_USER_PERMISSION.setCanCreate(null);
    NEW_USER_PERMISSION.setCanRead(null);
    NEW_USER_PERMISSION.setCanUpdate(null);
    NEW_USER_PERMISSION.setCanDelete(null);
    NEW_USER_PERMISSION.preUpdate();
    assertThat(NEW_USER_PERMISSION.getCanCreate()).isTrue();
    assertThat(NEW_USER_PERMISSION.getCanRead()).isTrue();
    assertThat(NEW_USER_PERMISSION.getCanUpdate()).isTrue();
    assertThat(NEW_USER_PERMISSION.getCanDelete()).isTrue();
  }
}
