package br.com.netbrasoft.gnuob.authentication;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RoleComponentTests {
  private final static RoleComponent NEW_ROLE_COMPONENT = RoleComponent.getInstance(null);

  @BeforeEach
  public void setup() {
    NEW_ROLE_COMPONENT.setId(0);
    NEW_ROLE_COMPONENT.setVersion(0);
  }

  @Test
  void testPrePersist() {
    NEW_ROLE_COMPONENT.setCanEdit(null);
    NEW_ROLE_COMPONENT.setIsRequired(null);
    NEW_ROLE_COMPONENT.prePersist();
    assertThat(NEW_ROLE_COMPONENT.getCanEdit()).isTrue();
    assertThat(NEW_ROLE_COMPONENT.getIsRequired()).isTrue();
  }

  @Test
  void testPreUpdate() {
    NEW_ROLE_COMPONENT.setCanEdit(null);
    NEW_ROLE_COMPONENT.setIsRequired(null);
    NEW_ROLE_COMPONENT.preUpdate();
    assertThat(NEW_ROLE_COMPONENT.getCanEdit()).isTrue();
    assertThat(NEW_ROLE_COMPONENT.getIsRequired()).isTrue();
  }
}
