package br.com.netbrasoft.gnuob.authentication;

import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.SAVE;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.SAVE_ALL;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeTopic;

@Transactional
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "stomp.authentication=false", "spring.mail.test-connection=false", "spring.liquibase.contexts:test"})
class PermissionServiceTests extends AbstractAuthenticationServiceTest<Permission> {
  static class PermissionTypeTopic extends TypeTopic<Permission> {
  }

  private final User NEW_USER = User.getInstance("bar@foo.com");
  private final Permission NEW_PERMISSION = Permission.getInstance();
  private final Component NEW_COMPONENT = Component.getInstance();
  private final Role NEW_ROLE = Role.getInstance();


  @Autowired(required = true)
  private ITypeService<Permission> permissionService;

  @Override
  public Permission getType() {
    return NEW_PERMISSION;
  }

  @Override
  public ITypeService<Permission> getTypService() {
    return permissionService;
  }

  @Override
  public Type getTypePayloadType() {
    return PermissionTypeTopic.class;
  }

  @Override
  public String getTypeSimpleName() {
    return Permission.class.getSimpleName();
  }

  @BeforeEach
  public void setup() throws InterruptedException, ExecutionException, TimeoutException {
    super.setup();
    NEW_PERMISSION.setActive(true);
    NEW_PERMISSION.setCode("code");
    NEW_PERMISSION.setDescription("description");
    NEW_PERMISSION.setId(0);
    NEW_PERMISSION.setName("Bar Foo");
    NEW_PERMISSION.setVersion(0);
    NEW_COMPONENT.setActive(true);
    NEW_COMPONENT.setCode("code");
    NEW_COMPONENT.setDescription("description");
    NEW_COMPONENT.setId(1000L);
    NEW_COMPONENT.setName("Bar Foo");
    NEW_COMPONENT.setVersion(0);
    NEW_ROLE.setActive(true);
    NEW_ROLE.setCode("code");
    NEW_ROLE.setDescription("description");
    NEW_ROLE.setId(1000L);
    NEW_ROLE.setName("Bar Foo");
    NEW_ROLE.setVersion(0);
    NEW_USER.setActive(true);
    NEW_USER.setChangePassword(true);
    NEW_USER.setCode("code");
    NEW_USER.setCookie("cookie");
    NEW_USER.setDescription("description");
    NEW_USER.setEmail("bar@foo.com");
    NEW_USER.setFirstName("Bar");
    NEW_USER.setLastName("Foo");
    NEW_USER.setUsername("bar@foo.com");
    NEW_USER.setId(1000L);
    NEW_USER.setMobile(true);
    NEW_USER.setMobileIpAddress("mobileIpAddress");
    NEW_USER.setMobileManufacturer("mobileManufacturer");
    NEW_USER.setMobileMaxDownload("mobileMaxDownload");
    NEW_USER.setMobileModel("mobileModel");
    NEW_USER.setMobileNetworkType("mobileNetworkType");
    NEW_USER.setMobilePackageName("mobilePackageName");
    NEW_USER.setMobilePlatform("mobilePlatform");
    NEW_USER.setMobileSerial("mobileSerial");
    NEW_USER.setMobileSignal("mobileSignal");
    NEW_USER.setMobileUuid("mobileUuid");
    NEW_USER.setMobileVersion("mobileVersion");
    NEW_USER.setMobileVersionCode("mobileVersionCode");
    NEW_USER.setMobileVersionNumber("mobileVersionNumber");
    NEW_USER.setName("Bar Foo");
    NEW_USER.setPassword("9083D070BA251593:1000:1A82A5073A126C2755904561B501F4A631C8681F");
    NEW_USER.setRoot(true);
    NEW_USER.setVersion(0);
    NEW_PERMISSION.getPermissionComponents().add(PermissionComponent.getInstance(NEW_COMPONENT));
    NEW_PERMISSION.getPermissionRoles().add(PermissionRole.getInstance(NEW_ROLE));
    NEW_PERMISSION.getPermissionUsers().add(PermissionUser.getInstance(NEW_USER));
  }

  @AfterEach
  public void setDown() throws InterruptedException, ExecutionException {
    super.setDown();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql", "classpath:scripts/createUser.sql", "classpath:scripts/createComponent.sql"})
  void saveCreateShouldReturnPermission() throws InterruptedException, ExecutionException, TimeoutException {
    final Permission attachedPermission = permissionService.save(NEW_PERMISSION);
    final TypeTopic<Permission> permissionTypeTopic = getTopicPermissionCompletableFuture().get(10, SECONDS);
    final TypeTopic<User> userTypeTopic = getTopicUserCompletableFuture().get(10, SECONDS);
    final TypeTopic<Role> roleTypeTopic = getTopicRoleCompletableFuture().get(10, SECONDS);
    assertThat(attachedPermission).isEqualTo(NEW_PERMISSION);
    assertThat(attachedPermission.getPermissionRoles().iterator().next().getRole()).isEqualTo(NEW_ROLE);
    assertThat(attachedPermission.getPermissionUsers().iterator().next().getUser()).isEqualTo(NEW_USER);
    assertThat(attachedPermission.getPermissionComponents().iterator().next().getComponent()).isEqualTo(NEW_COMPONENT);
    assertThat(attachedPermission.getId()).isPositive();
    assertThat(attachedPermission.getVersion()).isEqualTo(1);
    assertThat(attachedPermission.getDtype()).isEqualTo(Permission.class.getSimpleName().toLowerCase());
    assertThat(attachedPermission.getCreated()).isNotNull();
    assertThat(attachedPermission.getModified()).isNotNull();
    assertThat(attachedPermission.getUserCreated()).isNotNull();
    assertThat(attachedPermission.getUserModified()).isNotNull();
    assertThat(permissionTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(permissionTypeTopic.getType()).isEqualTo(NEW_PERMISSION);
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(userTypeTopic.getTypes().get(0)).isEqualTo(NEW_USER);
    assertThat(userTypeTopic.getTypes().get(0).getUserPermissions().iterator().next().getPermission()).isEqualTo(NEW_PERMISSION);
    assertThat(roleTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(roleTypeTopic.getTypes().get(0)).isEqualTo(NEW_ROLE);
    assertThat(roleTypeTopic.getTypes().get(0).getRolePermissions().iterator().next().getPermission()).isEqualTo(NEW_PERMISSION);
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createPermission.sql", "classpath:scripts/createRole.sql", "classpath:scripts/createUser.sql", "classpath:scripts/createComponent.sql"})
  void saveUpdateShouldReturnPermission() throws InterruptedException, ExecutionException, TimeoutException {
    setupPermission();
    NEW_PERMISSION.setId(1000L);
    NEW_PERMISSION.setVersion(1);
    NEW_PERMISSION.getPermissionRoles().clear();
    NEW_PERMISSION.getPermissionUsers().clear();
    final Permission attachedPermission = permissionService.save(NEW_PERMISSION);
    final TypeTopic<Permission> permissionTypeTopic = getTopicPermissionCompletableFuture().get(10, SECONDS);
    final TypeTopic<User> userTypeTopic = getTopicUserCompletableFuture().get(10, SECONDS);
    final TypeTopic<Role> roleTypeTopic = getTopicRoleCompletableFuture().get(10, SECONDS);
    assertThat(attachedPermission).isEqualTo(NEW_PERMISSION);
    assertThat(attachedPermission.getPermissionRoles().isEmpty()).isTrue();
    assertThat(attachedPermission.getPermissionUsers().isEmpty()).isTrue();
    assertThat(attachedPermission.getId()).isPositive();
    assertThat(attachedPermission.getVersion()).isEqualTo(2);
    assertThat(attachedPermission.getDtype()).isEqualTo(Permission.class.getSimpleName().toLowerCase());
    assertThat(attachedPermission.getCreated()).isNotNull();
    assertThat(attachedPermission.getModified()).isNotNull();
    assertThat(attachedPermission.getUserCreated()).isNotNull();
    assertThat(attachedPermission.getUserModified()).isNotNull();
    assertThat(permissionTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(permissionTypeTopic.getType()).isEqualTo(NEW_PERMISSION);
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(userTypeTopic.getTypes().get(0)).isEqualTo(NEW_USER);
    assertThat(userTypeTopic.getTypes().get(0).getUserPermissions().isEmpty()).isTrue();
    assertThat(roleTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(roleTypeTopic.getTypes().get(0)).isEqualTo(NEW_ROLE);
    assertThat(roleTypeTopic.getTypes().get(0).getRolePermissions().isEmpty()).isTrue();
  }

  private void setupPermission() throws InterruptedException, ExecutionException, TimeoutException {
    setDown();
    NEW_PERMISSION.setId(1000L);
    permissionService.save(NEW_PERMISSION);
    setup();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql", "classpath:scripts/createUser.sql", "classpath:scripts/createComponent.sql"})
  void saveAllShouldReturnPermissions() throws InterruptedException, ExecutionException, TimeoutException {
    final List<Permission> attachedTypes = permissionService.saveAll(newArrayList(NEW_PERMISSION));
    final TypeTopic<Permission> typeTopic = getTopicPermissionCompletableFuture().get(10, SECONDS);
    assertThat(attachedTypes.get(0)).isEqualTo(NEW_PERMISSION);
    assertThat(attachedTypes.get(0).getPermissionRoles().iterator().next().getRole()).isEqualTo(NEW_ROLE);
    assertThat(attachedTypes.get(0).getPermissionUsers().iterator().next().getUser()).isEqualTo(NEW_USER);
    assertThat(attachedTypes.get(0).getPermissionComponents().iterator().next().getComponent()).isEqualTo(NEW_COMPONENT);
    assertThat(attachedTypes.get(0).getId()).isGreaterThan(0L);
    assertThat(attachedTypes.get(0).getVersion()).isEqualTo(1);
    assertThat(attachedTypes.get(0).getDtype()).isEqualTo(Permission.class.getSimpleName().toLowerCase());
    assertThat(attachedTypes.get(0).getCreated()).isNotNull();
    assertThat(attachedTypes.get(0).getModified()).isNotNull();
    assertThat(attachedTypes.get(0).getUserCreated()).isNotNull();
    assertThat(attachedTypes.get(0).getUserModified()).isNotNull();
    assertThat(typeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(typeTopic.getTypes().get(0)).isEqualTo(NEW_PERMISSION);
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createPermission.sql"})
  void deleteShouldNotReturnPermission() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteShouldNotReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createPermission.sql"})
  void deleteAllShouldNotReturnPermissions() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createPermission.sql"})
  void findOneByIdShouldReturnPermission() {
    super.findOneByIdShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createPermission.sql"})
  void findOneByExampleShouldReturnPermission() {
    super.findOneByExampleShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createPermission.sql"})
  public void findOneShouldReturnNoResultError() {
    super.findOneShouldReturnNoResultError();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createPermission.sql"})
  void findAllShouldReturnPermissions() {
    super.findAllShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createPermission.sql"})
  void findAllFilterShouldReturnPermissions() {
    super.findAllFilterShouldReturnTypes();
  }
}
