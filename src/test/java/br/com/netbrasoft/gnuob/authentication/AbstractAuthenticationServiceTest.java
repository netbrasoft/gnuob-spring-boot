package br.com.netbrasoft.gnuob.authentication;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.lang3.StringUtils.join;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.util.concurrent.ListenableFuture;

import br.com.netbrasoft.gnuob.type.AbstractActiveType;
import br.com.netbrasoft.gnuob.type.AbstractTypeServiceTest;
import br.com.netbrasoft.gnuob.type.TypeTopic;

public abstract class AbstractAuthenticationServiceTest<T extends AbstractActiveType> extends AbstractTypeServiceTest<T> {
  static class UserTypeTopic extends TypeTopic<User> {
  }

  static class PermissionTypeTopic extends TypeTopic<Permission> {
  }

  static class RoleTypeTopic extends TypeTopic<Role> {
  }

  private CompletableFuture<TypeTopic<User>> topicUserCompletableFuture;
  private CompletableFuture<TypeTopic<Permission>> topicPermissionCompletableFuture;
  private CompletableFuture<TypeTopic<Role>> topicRoleCompletableFuture;
  private CompletableFuture<Boolean> sessionUserCompletableFuture;
  private CompletableFuture<Boolean> sessionPermissionCompletableFuture;
  private CompletableFuture<Boolean> sessionRoleCompletableFuture;
  private ListenableFuture<StompSession> userTopicStompSession;
  private ListenableFuture<StompSession> permissionTopicStompSession;
  private ListenableFuture<StompSession> roleTopicStompSession;

  public void setup() throws InterruptedException, ExecutionException, TimeoutException {
    super.setup();
    topicUserCompletableFuture = new CompletableFuture<TypeTopic<User>>();
    topicPermissionCompletableFuture = new CompletableFuture<TypeTopic<Permission>>();
    topicRoleCompletableFuture = new CompletableFuture<TypeTopic<Role>>();
    sessionUserCompletableFuture = new CompletableFuture<Boolean>();
    sessionPermissionCompletableFuture = new CompletableFuture<Boolean>();
    sessionRoleCompletableFuture = new CompletableFuture<Boolean>();
    userTopicStompSession = createStompSession(createUserStompSessionHandler());
    permissionTopicStompSession = createStompSession(createPermissionStompSessionHandler());
    roleTopicStompSession = createStompSession(createRoleStompSessionHandler());
    if (!sessionUserCompletableFuture.get(10, SECONDS) || !sessionRoleCompletableFuture.get(10, SECONDS) || !sessionRoleCompletableFuture.get(10, SECONDS)) {
      throw new TimeoutException();
    }
  }

  private StompSessionHandler createUserStompSessionHandler() {
    return new StompSessionHandlerAdapter() {

      @Override
      public Type getPayloadType(StompHeaders headers) {
        return UserTypeTopic.class;
      }

      @Override
      public void handleFrame(StompHeaders headers, Object payload) {
        topicUserCompletableFuture.complete((UserTypeTopic) payload);
      }

      @Override
      public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        session.subscribe(join("/topic/", User.class.getSimpleName()).toLowerCase(), this);
        sessionUserCompletableFuture.complete(session.isConnected());
      }
    };
  }

  private StompSessionHandler createPermissionStompSessionHandler() {
    return new StompSessionHandlerAdapter() {

      @Override
      public Type getPayloadType(StompHeaders headers) {
        return PermissionTypeTopic.class;
      }

      @Override
      public void handleFrame(StompHeaders headers, Object payload) {
        topicPermissionCompletableFuture.complete((PermissionTypeTopic) payload);
      }

      @Override
      public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        session.subscribe(join("/topic/", Permission.class.getSimpleName()).toLowerCase(), this);
        sessionPermissionCompletableFuture.complete(session.isConnected());
      }
    };
  }

  private StompSessionHandler createRoleStompSessionHandler() {
    return new StompSessionHandlerAdapter() {

      @Override
      public Type getPayloadType(StompHeaders headers) {
        return RoleTypeTopic.class;
      }

      @Override
      public void handleFrame(StompHeaders headers, Object payload) {
        topicRoleCompletableFuture.complete((RoleTypeTopic) payload);
      }

      @Override
      public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        session.subscribe(join("/topic/", Role.class.getSimpleName()).toLowerCase(), this);
        sessionRoleCompletableFuture.complete(session.isConnected());
      }
    };
  }

  public void setDown() throws InterruptedException, ExecutionException {
    super.setDown();
    userTopicStompSession.get().disconnect();
    permissionTopicStompSession.get().disconnect();
    roleTopicStompSession.get().disconnect();
  }

  public CompletableFuture<TypeTopic<User>> getTopicUserCompletableFuture() {
    return topicUserCompletableFuture;
  }

  public CompletableFuture<TypeTopic<Permission>> getTopicPermissionCompletableFuture() {
    return topicPermissionCompletableFuture;
  }

  public CompletableFuture<TypeTopic<Role>> getTopicRoleCompletableFuture() {
    return topicRoleCompletableFuture;
  }
}
