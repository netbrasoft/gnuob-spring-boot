package br.com.netbrasoft.gnuob.authentication;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PermissionComponentTests {
  private final static PermissionComponent NEW_PERMISSION_COMPONENT = PermissionComponent.getInstance(null);

  @BeforeEach
  public void setup() {
    NEW_PERMISSION_COMPONENT.setId(0);
    NEW_PERMISSION_COMPONENT.setVersion(0);
  }

  @Test
  void testPrePersist() {
    NEW_PERMISSION_COMPONENT.setCanEdit(null);
    NEW_PERMISSION_COMPONENT.setIsRequired(null);
    NEW_PERMISSION_COMPONENT.prePersist();
    assertThat(NEW_PERMISSION_COMPONENT.getCanEdit()).isTrue();
    assertThat(NEW_PERMISSION_COMPONENT.getIsRequired()).isTrue();
  }

  @Test
  void testPreUpdate() {
    NEW_PERMISSION_COMPONENT.setCanEdit(null);
    NEW_PERMISSION_COMPONENT.setIsRequired(null);
    NEW_PERMISSION_COMPONENT.preUpdate();
    assertThat(NEW_PERMISSION_COMPONENT.getCanEdit()).isTrue();
    assertThat(NEW_PERMISSION_COMPONENT.getIsRequired()).isTrue();
  }
}
