package br.com.netbrasoft.gnuob.authentication;

import static br.com.netbrasoft.gnuob.ApplicationConstants.AUTHORIZATION;
import static br.com.netbrasoft.gnuob.ApplicationConstants.INVALID_USERNAME_OR_PASSWORD;
import static br.com.netbrasoft.gnuob.Utils.createToken;
import static org.apache.commons.lang3.StringUtils.join;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import br.com.netbrasoft.gnuob.type.AbstractTypeRestControllerTest;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeRuntimeException;

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "spring.mail.test-connection=false"})
class AuthenticationRestControllerTests extends AbstractTypeRestControllerTest<User> {
  @BeforeEach
  public void setup() {
    super.setup();
  }

  @Override
  @AfterEach
  public void setDown() {}

  @Override
  public User getType() {
    return AUTHENTICATED_USER;
  }

  @Override
  public ITypeService<User> getTypeMockService() {
    return (ITypeService<User>) super.getMockUserService();
  }

  @Override
  public Class<User> getClassType() {
    return User.class;
  }

  @Override
  public String getPath() {
    return "/user";
  }

  @Override
  public Class<User[]> getClassTypes() {
    return User[].class;
  }

  @Test
  void loginShouldReturnUser() {
    class Login {
      public String username = "foo@bar.com";
      public String password = "secret";
    }
    final User type = getType();
    when(((UserDetailsService) getTypeMockService()).loadUserByUsername("foo@bar.com")).thenReturn(type);
    when(getTypeMockService().save(type)).thenReturn(type);
    final ResponseEntity<User> responseEntity = getRestTemplate().postForEntity(createPathLogin(), new Login(), getClassType());
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isEqualTo(type);
    assertThat(responseEntity.getHeaders().get(AUTHORIZATION).get(0)).isEqualTo(responseEntity.getBody().getToken());
    verify((UserDetailsService) getTypeMockService(), times(2)).loadUserByUsername("foo@bar.com");
    verify(getTypeMockService(), times(1)).save(type);
  }

  private String createPathLogin() {
    return join("http://localhost:", getPort(), getPath(), "/of/login");
  }

  @Test
  void logoutShouldNotReturnUser() {
    final User type = getType();
    when(((UserDetailsService) getTypeMockService()).loadUserByUsername("foo@bar.com")).thenReturn(type);
    when(getTypeMockService().save(type)).thenReturn(type);
    final ResponseEntity<Void> responseEntity = getRestTemplate().postForEntity(createPathLogout(), createHttpEntity(createToken("foo@bar.com")), Void.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isNull();
    assertThat(responseEntity.getHeaders().get(AUTHORIZATION)).isNull();
    verify((UserDetailsService) getTypeMockService(), times(1)).loadUserByUsername("foo@bar.com");
    verify(getTypeMockService(), times(1)).save(type);
  }

  private String createPathLogout() {
    return join("http://localhost:", getPort(), getPath(), "/of/logout");
  }

  private HttpEntity<Void> createHttpEntity(final String token) {
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    httpHeaders.add(AUTHORIZATION, token);
    final HttpEntity<Void> httpEntity = new HttpEntity<Void>(null, httpHeaders);
    return httpEntity;
  }

  @Test
  void wrongUsernameShouldReturnUnauthorized() {
    class Login {
      public String username = "wrong-foo@bar.com";
      public String password = "secret";
    }
    final User type = getType();
    when(((UserDetailsService) getTypeMockService()).loadUserByUsername("wrong-foo@bar.com")).thenThrow(new UsernameNotFoundException(INVALID_USERNAME_OR_PASSWORD));
    final ResponseEntity<TypeRuntimeException> responseEntity = getRestTemplate().postForEntity(createPathLogin(), new Login(), TypeRuntimeException.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(401);
    assertThat(responseEntity.getBody().getMessage()).isEqualTo(INVALID_USERNAME_OR_PASSWORD);
    assertThat(responseEntity.getHeaders().get(AUTHORIZATION)).isNull();
    verify((UserDetailsService) getTypeMockService(), times(1)).loadUserByUsername("wrong-foo@bar.com");
    verify(getTypeMockService(), times(0)).save(type);
  }

  @Test
  void wrongPasswordShouldReturnUnauthorized() {
    class Login {
      public String username = "foo@bar.com";
      public String password = "wrong-secret";
    }
    final User type = getType();
    when(((UserDetailsService) getTypeMockService()).loadUserByUsername("foo@bar.com")).thenReturn(type);
    final ResponseEntity<TypeRuntimeException> responseEntity = getRestTemplate().postForEntity(createPathLogin(), new Login(), TypeRuntimeException.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(401);
    assertThat(responseEntity.getBody().getMessage()).isEqualTo(INVALID_USERNAME_OR_PASSWORD);
    assertThat(responseEntity.getHeaders().get(AUTHORIZATION)).isNull();
    verify((UserDetailsService) getTypeMockService(), times(1)).loadUserByUsername("foo@bar.com");
    verify(getTypeMockService(), times(0)).save(type);
  }

  @Test
  void wrongUsernameInTokenShouldReturnUnauthorized() {
    final User type = getType();
    when(((UserDetailsService) getTypeMockService()).loadUserByUsername("wrong-foo@bar.com")).thenThrow(new UsernameNotFoundException(INVALID_USERNAME_OR_PASSWORD));
    final ResponseEntity<TypeRuntimeException> responseEntity = getRestTemplate().postForEntity(createPathLogin(), createHttpEntity(createToken("wrong-foo@bar.com")), TypeRuntimeException.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(401);
    assertThat(responseEntity.getBody().getMessage()).isEqualTo(INVALID_USERNAME_OR_PASSWORD);
    assertThat(responseEntity.getHeaders().get(AUTHORIZATION)).isNull();
    verify((UserDetailsService) getTypeMockService(), times(1)).loadUserByUsername("wrong-foo@bar.com");
    verify(getTypeMockService(), times(0)).save(type);
  }

  @Test
  void registerShouldCreateUser() {
    final User type = getType();
    when(((IUserService) getTypeMockService()).register(type)).thenReturn(null);
    final ResponseEntity<Void> responseEntity = getRestTemplate().postForEntity(createPathRegister(EMPTY), type, Void.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isNull();
    assertThat(responseEntity.getHeaders().get(AUTHORIZATION)).isNull();
    verify((IUserService) getTypeMockService(), times(1)).register(type);
  }

  private String createPathRegister(final String tokenPath) {
    return join("http://localhost:", getPort(), getPath(), "/register", tokenPath);
  }

  @Test
  void confirmRegisterShouldUpdateUser() {
    when(((IUserService) getTypeMockService()).confirmRegister(User.getInstance("foo@bar.com"))).thenReturn(null);
    final ResponseEntity<Void> responseEntity = getRestTemplate().postForEntity(createPathRegister(join("/", createToken("foo@bar.com"))), null, Void.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isNull();
    assertThat(responseEntity.getHeaders().get(AUTHORIZATION)).isNull();
    verify((IUserService) getTypeMockService(), times(1)).confirmRegister(User.getInstance("foo@bar.com"));
  }

  @Test
  void forgotPasswordShouldUpdateUser() {
    final User type = getType();
    when(((IUserService) getTypeMockService()).forgotPassword(type)).thenReturn(null);
    final ResponseEntity<Void> responseEntity = getRestTemplate().postForEntity(createPathForgotPassword(EMPTY), type, Void.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isNull();
    assertThat(responseEntity.getHeaders().get(AUTHORIZATION)).isNull();
    verify((IUserService) getTypeMockService(), times(1)).forgotPassword(type);
  }

  private String createPathForgotPassword(final String tokenPath) {
    return join("http://localhost:", getPort(), getPath(), "/forgot/password", tokenPath);
  }

  @Test
  void confirmForgotPasswordShouldUpdateUser() {
    final User type = getType();
    when(((IUserService) getTypeMockService()).confirmForgotPassword(type)).thenReturn(null);
    final ResponseEntity<Void> responseEntity = getRestTemplate().postForEntity(createPathForgotPassword(join("/", createToken("foo@bar.com"))), type, Void.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isNull();
    assertThat(responseEntity.getHeaders().get(AUTHORIZATION)).isNull();
    verify((IUserService) getTypeMockService(), times(1)).confirmForgotPassword(type);
  }
}
