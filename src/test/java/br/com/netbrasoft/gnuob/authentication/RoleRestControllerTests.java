package br.com.netbrasoft.gnuob.authentication;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.com.netbrasoft.gnuob.type.AbstractTypeRestControllerTest;
import br.com.netbrasoft.gnuob.type.ITypeService;

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "spring.mail.test-connection=false"})
class RoleRestControllerTests extends AbstractTypeRestControllerTest<Role> {
  private final static Role NEW_ROLE = Role.getInstance();
  @MockBean
  private ITypeService<Role> mockRoleService;

  @BeforeEach
  public void setup() {
    super.setup();
    NEW_ROLE.setActive(true);
    NEW_ROLE.setCode("code");
    NEW_ROLE.setDescription("description");
    NEW_ROLE.setId(0);
    NEW_ROLE.setName("Bar Foo");
    NEW_ROLE.setVersion(0);
  }

  @Override
  @AfterEach
  public void setDown() {}

  @Override
  public Role getType() {
    return NEW_ROLE;
  }

  @Override
  public ITypeService<Role> getTypeMockService() {
    return (ITypeService<Role>) mockRoleService;
  }

  @Override
  public Class<Role> getClassType() {
    return Role.class;
  }

  @Override
  public String getPath() {
    return "/role";
  }

  @Override
  public Class<Role[]> getClassTypes() {
    return Role[].class;
  }

  @Test
  void saveShouldReturnRole() {
    super.saveShouldReturnType();
  }

  @Test
  public void wrongSaveShouldReturnServiceError() {
    super.wrongSaveShouldReturnServiceError();
  }

  @Test
  void saveAllShouldReturnRoles() {
    super.saveAllShouldReturnTypes();
  }

  @Test
  public void wrongSaveAllShouldReturnServiceError() {
    super.wrongSaveAllShouldReturnServiceError();
  }

  @Test
  void deleteShouldNotReturnRole() {
    super.deleteShouldNotReturnType();
  }

  @Test
  public void wrongDeleteShouldReturnServiceError() {
    super.wrongDeleteShouldReturnServiceError();
  }

  @Test
  void deleteAllShouldNotReturnRoles() {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  public void wrongDeleteAllShouldReturnServiceError() {
    super.wrongDeleteAllShouldReturnServiceError();
  }

  @Test
  void findOneShouldReturnRole() {
    super.findOneShouldReturnType();
  }

  @Test
  public void wrongFindOneShouldReturnServiceError() {
    super.wrongFindOneShouldReturnServiceError();
  }

  @Test
  void findAllShouldReturnRoles() {
    super.findAllShouldReturnTypes();
  }

  @Test
  public void wrongFindAllShouldReturnServiceError() {
    super.wrongFindAllShouldReturnServiceError();
  }

  @Test
  void findAllFilterShouldReturnRoles() {
    super.findAllFilterShouldReturnTypes();
  }

  @Test
  public void wrongFindAllFilterShouldReturnServiceError() {
    super.wrongFindAllFilterShouldReturnServiceError();
  }
}
