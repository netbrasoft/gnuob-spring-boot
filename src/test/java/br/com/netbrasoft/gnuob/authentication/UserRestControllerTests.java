package br.com.netbrasoft.gnuob.authentication;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.netbrasoft.gnuob.type.AbstractTypeRestControllerTest;
import br.com.netbrasoft.gnuob.type.ITypeService;
import de.rtner.security.auth.spi.SimplePBKDF2;

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "spring.mail.test-connection=false"})
class UserRestControllerTests extends AbstractTypeRestControllerTest<User> {
  private final static User NEW_USER = User.getInstance("bar@foo.com");

  @BeforeEach
  public void setup() {
    super.setup();
    NEW_USER.setActive(true);
    NEW_USER.setChangePassword(true);
    NEW_USER.setCode("code");
    NEW_USER.setCookie("cookie");
    NEW_USER.setDescription("description");
    NEW_USER.setEmail("bar@foo.com");
    NEW_USER.setId(0);
    NEW_USER.setMobile(true);
    NEW_USER.setMobileIpAddress("mobileIpAddress");
    NEW_USER.setMobileManufacturer("mobileManufacturer");
    NEW_USER.setMobileMaxDownload("mobileMaxDownload");
    NEW_USER.setMobileModel("mobileModel");
    NEW_USER.setMobileNetworkType("mobileNetworkType");
    NEW_USER.setMobilePackageName("mobilePackageName");
    NEW_USER.setMobilePlatform("mobilePlatform");
    NEW_USER.setMobileSerial("mobileSerial");
    NEW_USER.setMobileSignal("mobileSignal");
    NEW_USER.setMobileUuid("mobileUuid");
    NEW_USER.setMobileVersion("mobileVersion");
    NEW_USER.setMobileVersionCode("mobileVersionCode");
    NEW_USER.setMobileVersionNumber("mobileVersionNumber");
    NEW_USER.setName("Bar Foo");
    NEW_USER.setPassword(new SimplePBKDF2().deriveKeyFormatted("secret"));
    NEW_USER.setRoot(true);
    NEW_USER.setVersion(0);
  }

  @Override
  @AfterEach
  public void setDown() {}

  @Override
  public User getType() {
    return NEW_USER;
  }

  @Override
  public ITypeService<User> getTypeMockService() {
    return (ITypeService<User>) super.getMockUserService();
  }

  @Override
  public Class<User> getClassType() {
    return User.class;
  }

  @Override
  public String getPath() {
    return "/user";
  }

  @Override
  public Class<User[]> getClassTypes() {
    return User[].class;
  }

  @Test
  void saveShouldReturnUser() {
    super.saveShouldReturnType();
  }

  @Test
  public void wrongSaveShouldReturnServiceError() {
    super.wrongSaveShouldReturnServiceError();
  }

  @Test
  void saveAllShouldReturnUsers() {
    super.saveAllShouldReturnTypes();
  }

  @Test
  public void wrongSaveAllShouldReturnServiceError() {
    super.wrongSaveAllShouldReturnServiceError();
  }

  @Test
  void deleteShouldNotReturnUser() {
    super.deleteShouldNotReturnType();
  }

  @Test
  public void wrongDeleteShouldReturnServiceError() {
    super.wrongDeleteShouldReturnServiceError();
  }

  @Test
  void deleteAllShouldNotReturnUsers() {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  public void wrongDeleteAllShouldReturnServiceError() {
    super.wrongDeleteAllShouldReturnServiceError();
  }

  @Test
  void findOneShouldReturnUser() {
    super.findOneShouldReturnType();
  }

  @Test
  public void wrongFindOneShouldReturnServiceError() {
    super.wrongFindOneShouldReturnServiceError();
  }

  @Test
  void findAllShouldReturnUsers() {
    super.findAllShouldReturnTypes();
  }

  @Test
  public void wrongFindAllShouldReturnServiceError() {
    super.wrongFindAllShouldReturnServiceError();
  }

  @Test
  void findAllFilterShouldReturnUsers() {
    super.findAllFilterShouldReturnTypes();
  }

  @Test
  public void wrongFindAllFilterShouldReturnServiceError() {
    super.wrongFindAllFilterShouldReturnServiceError();
  }
}
