package br.com.netbrasoft.gnuob.authentication;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.com.netbrasoft.gnuob.type.AbstractTypeRestControllerTest;
import br.com.netbrasoft.gnuob.type.ITypeService;

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "spring.mail.test-connection=false"})
class PermissionRestControllerTests extends AbstractTypeRestControllerTest<Permission> {
  private final static Permission NEW_PERMISSION = Permission.getInstance();
  @MockBean
  private ITypeService<Permission> mockPermissionService;

  @BeforeEach
  public void setup() {
    super.setup();
    NEW_PERMISSION.setActive(true);
    NEW_PERMISSION.prePersist();
    NEW_PERMISSION.setCode("code");
    NEW_PERMISSION.setDescription("description");
    NEW_PERMISSION.setId(0);
    NEW_PERMISSION.setName("Bar Foo");
    NEW_PERMISSION.setVersion(0);
  }

  @Override
  @AfterEach
  public void setDown() {}

  @Override
  public Permission getType() {
    return NEW_PERMISSION;
  }

  @Override
  public ITypeService<Permission> getTypeMockService() {
    return (ITypeService<Permission>) mockPermissionService;
  }

  @Override
  public Class<Permission> getClassType() {
    return Permission.class;
  }

  @Override
  public String getPath() {
    return "/permission";
  }

  @Override
  public Class<Permission[]> getClassTypes() {
    return Permission[].class;
  }

  @Test
  void saveShouldReturnPermission() {
    super.saveShouldReturnType();
  }

  @Test
  public void wrongSaveShouldReturnServiceError() {
    super.wrongSaveShouldReturnServiceError();
  }

  @Test
  void saveAllShouldReturnPermissions() {
    super.saveAllShouldReturnTypes();
  }

  @Test
  public void wrongSaveAllShouldReturnServiceError() {
    super.wrongSaveAllShouldReturnServiceError();
  }

  @Test
  void deleteShouldNotReturnPermission() {
    super.deleteShouldNotReturnType();
  }

  @Test
  public void wrongDeleteShouldReturnServiceError() {
    super.wrongDeleteShouldReturnServiceError();
  }

  @Test
  void deleteAllShouldNotReturnPermissions() {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  public void wrongDeleteAllShouldReturnServiceError() {
    super.wrongDeleteAllShouldReturnServiceError();
  }

  @Test
  void findOneShouldReturnPermission() {
    super.findOneShouldReturnType();
  }

  @Test
  public void wrongFindOneShouldReturnServiceError() {
    super.wrongFindOneShouldReturnServiceError();
  }

  @Test
  void findAllShouldReturnPermissions() {
    super.findAllShouldReturnTypes();
  }

  @Test
  public void wrongFindAllShouldReturnServiceError() {
    super.wrongFindAllShouldReturnServiceError();
  }

  @Test
  void findAllFilterShouldReturnPermissions() {
    super.findAllFilterShouldReturnTypes();
  }

  @Test
  public void wrongFindAllFilterShouldReturnServiceError() {
    super.wrongFindAllFilterShouldReturnServiceError();
  }
}
