package br.com.netbrasoft.gnuob.authentication;

import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.SAVE;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.SAVE_ALL;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeTopic;

@Transactional
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "stomp.authentication=false", "spring.mail.test-connection=false", "spring.liquibase.contexts:test"})
class RoleServiceTests extends AbstractAuthenticationServiceTest<Role> {
  static class RoleTypeTopic extends TypeTopic<Role> {
  }

  private final User NEW_USER = User.getInstance("bar@foo.com");
  private final Permission NEW_PERMISSION = Permission.getInstance();
  private final Component NEW_COMPONENT = Component.getInstance();
  private final Role NEW_ROLE = Role.getInstance();

  @Autowired(required = true)
  private ITypeService<Role> roleService;

  @Override
  public Role getType() {
    return NEW_ROLE;
  }

  @Override
  public ITypeService<Role> getTypService() {
    return roleService;
  }

  @Override
  public Type getTypePayloadType() {
    return RoleTypeTopic.class;
  }

  @Override
  public String getTypeSimpleName() {
    return Role.class.getSimpleName();
  }

  @BeforeEach
  public void setup() throws InterruptedException, ExecutionException, TimeoutException {
    super.setup();
    NEW_ROLE.setActive(true);
    NEW_ROLE.setCode("code");
    NEW_ROLE.setDescription("description");
    NEW_ROLE.setId(0);
    NEW_ROLE.setName("Bar Foo");
    NEW_ROLE.setVersion(0);
    NEW_COMPONENT.setActive(true);
    NEW_COMPONENT.setCode("code");
    NEW_COMPONENT.setDescription("description");
    NEW_COMPONENT.setId(1000L);
    NEW_COMPONENT.setName("Bar Foo");
    NEW_COMPONENT.setVersion(0);
    NEW_USER.setActive(true);
    NEW_USER.setChangePassword(true);
    NEW_USER.setCode("code");
    NEW_USER.setCookie("cookie");
    NEW_USER.setDescription("description");
    NEW_USER.setEmail("bar@foo.com");
    NEW_USER.setFirstName("Bar");
    NEW_USER.setLastName("Foo");
    NEW_USER.setUsername("bar@foo.com");
    NEW_USER.setId(1000L);
    NEW_USER.setMobile(true);
    NEW_USER.setMobileIpAddress("mobileIpAddress");
    NEW_USER.setMobileManufacturer("mobileManufacturer");
    NEW_USER.setMobileMaxDownload("mobileMaxDownload");
    NEW_USER.setMobileModel("mobileModel");
    NEW_USER.setMobileNetworkType("mobileNetworkType");
    NEW_USER.setMobilePackageName("mobilePackageName");
    NEW_USER.setMobilePlatform("mobilePlatform");
    NEW_USER.setMobileSerial("mobileSerial");
    NEW_USER.setMobileSignal("mobileSignal");
    NEW_USER.setMobileUuid("mobileUuid");
    NEW_USER.setMobileVersion("mobileVersion");
    NEW_USER.setMobileVersionCode("mobileVersionCode");
    NEW_USER.setMobileVersionNumber("mobileVersionNumber");
    NEW_USER.setName("Bar Foo");
    NEW_USER.setPassword("9083D070BA251593:1000:1A82A5073A126C2755904561B501F4A631C8681F");
    NEW_USER.setRoot(true);
    NEW_USER.setVersion(0);
    NEW_PERMISSION.setActive(true);
    NEW_PERMISSION.setCode("code");
    NEW_PERMISSION.setDescription("description");
    NEW_PERMISSION.setId(1000L);
    NEW_PERMISSION.setName("Bar Foo");
    NEW_PERMISSION.setVersion(0);
    NEW_ROLE.getRoleUsers().add(RoleUser.getInstance(NEW_USER));
    NEW_ROLE.getRolePermissions().add(RolePermission.getInstance(NEW_PERMISSION));
    NEW_ROLE.getRolePermissions().iterator().next().getRoleComponents().add(RoleComponent.getInstance(NEW_COMPONENT));
  }

  @AfterEach
  public void setDown() throws InterruptedException, ExecutionException {
    super.setDown();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql", "classpath:scripts/createPermission.sql", "classpath:scripts/createComponent.sql"})
  void saveCreateShouldReturnRole() throws InterruptedException, ExecutionException, TimeoutException {
    final Role attachedRole = roleService.save(NEW_ROLE);
    final TypeTopic<Permission> permissionTypeTopic = getTopicPermissionCompletableFuture().get(10, SECONDS);
    final TypeTopic<User> userTypeTopic = getTopicUserCompletableFuture().get(10, SECONDS);
    final TypeTopic<Role> roleTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(attachedRole).isEqualTo(NEW_ROLE);
    assertThat(attachedRole.getRoleUsers().iterator().next().getUser()).isEqualTo(NEW_USER);
    assertThat(attachedRole.getRolePermissions().iterator().next().getPermission()).isEqualTo(NEW_PERMISSION);
    assertThat(attachedRole.getRolePermissions().iterator().next().getRoleComponents().iterator().next().getComponent()).isEqualTo(NEW_COMPONENT);
    assertThat(attachedRole.getId()).isPositive();
    assertThat(attachedRole.getVersion()).isEqualTo(1);
    assertThat(attachedRole.getDtype()).isEqualTo(Role.class.getSimpleName().toLowerCase());
    assertThat(attachedRole.getCreated()).isNotNull();
    assertThat(attachedRole.getModified()).isNotNull();
    assertThat(attachedRole.getUserCreated()).isNotNull();
    assertThat(attachedRole.getUserModified()).isNotNull();
    assertThat(roleTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(roleTypeTopic.getType()).isEqualTo(NEW_ROLE);
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(userTypeTopic.getTypes().get(0)).isEqualTo(NEW_USER);
    assertThat(userTypeTopic.getTypes().get(0).getUserRoles().iterator().next().getRole()).isEqualTo(NEW_ROLE);
    assertThat(permissionTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(permissionTypeTopic.getTypes().get(0)).isEqualTo(NEW_PERMISSION);
    assertThat(permissionTypeTopic.getTypes().get(0).getPermissionRoles().iterator().next().getRole()).isEqualTo(NEW_ROLE);
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql", "classpath:scripts/createUser.sql", "classpath:scripts/createPermission.sql", "classpath:scripts/createComponent.sql"})
  void saveUpdateShouldReturnRole() throws InterruptedException, ExecutionException, TimeoutException {
    setupRole();
    NEW_ROLE.setId(1000L);
    NEW_ROLE.setVersion(1);
    NEW_ROLE.getRoleUsers().clear();
    NEW_ROLE.getRolePermissions().clear();
    final Role attachedRole = roleService.save(NEW_ROLE);
    final TypeTopic<Permission> permissionTypeTopic = getTopicPermissionCompletableFuture().get(10, SECONDS);
    final TypeTopic<User> userTypeTopic = getTopicUserCompletableFuture().get(10, SECONDS);
    final TypeTopic<Role> roleTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(attachedRole).isEqualTo(NEW_ROLE);
    assertThat(attachedRole.getRoleUsers().isEmpty()).isTrue();
    assertThat(attachedRole.getRolePermissions().isEmpty()).isTrue();
    assertThat(attachedRole.getId()).isPositive();
    assertThat(attachedRole.getVersion()).isEqualTo(2);
    assertThat(attachedRole.getDtype()).isEqualTo(Role.class.getSimpleName().toLowerCase());
    assertThat(attachedRole.getCreated()).isNotNull();
    assertThat(attachedRole.getModified()).isNotNull();
    assertThat(attachedRole.getUserCreated()).isNotNull();
    assertThat(attachedRole.getUserModified()).isNotNull();
    assertThat(roleTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(roleTypeTopic.getType()).isEqualTo(NEW_ROLE);
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(userTypeTopic.getTypes().get(0)).isEqualTo(NEW_USER);
    assertThat(userTypeTopic.getTypes().get(0).getUserRoles().isEmpty()).isTrue();
    assertThat(permissionTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(permissionTypeTopic.getTypes().get(0)).isEqualTo(NEW_PERMISSION);
    assertThat(permissionTypeTopic.getTypes().get(0).getPermissionRoles().isEmpty()).isTrue();
  }

  private void setupRole() throws InterruptedException, ExecutionException, TimeoutException {
    setDown();
    NEW_ROLE.setId(1000L);
    roleService.save(NEW_ROLE);
    setup();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql", "classpath:scripts/createPermission.sql", "classpath:scripts/createComponent.sql"})
  void saveAllShouldReturnRoles() throws InterruptedException, ExecutionException, TimeoutException {
    final List<Role> attachedRoles = roleService.saveAll(newArrayList(NEW_ROLE));
    final TypeTopic<Role> roleTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(attachedRoles.get(0)).isEqualTo(NEW_ROLE);
    assertThat(attachedRoles.get(0).getRoleUsers().iterator().next().getUser()).isEqualTo(NEW_USER);
    assertThat(attachedRoles.get(0).getRolePermissions().iterator().next().getPermission()).isEqualTo(NEW_PERMISSION);
    assertThat(attachedRoles.get(0).getRolePermissions().iterator().next().getRoleComponents().iterator().next().getComponent()).isEqualTo(NEW_COMPONENT);
    assertThat(attachedRoles.get(0).getId()).isPositive();
    assertThat(attachedRoles.get(0).getVersion()).isEqualTo(1);
    assertThat(attachedRoles.get(0).getDtype()).isEqualTo(Role.class.getSimpleName().toLowerCase());
    assertThat(attachedRoles.get(0).getCreated()).isNotNull();
    assertThat(attachedRoles.get(0).getModified()).isNotNull();
    assertThat(attachedRoles.get(0).getUserCreated()).isNotNull();
    assertThat(attachedRoles.get(0).getUserModified()).isNotNull();
    assertThat(roleTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(roleTypeTopic.getTypes().get(0)).isEqualTo(NEW_ROLE);
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql"})
  void deleteShouldNotReturnRole() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteShouldNotReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql"})
  void deleteAllShouldNotReturnRoles() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql"})
  void findOneByIdShouldReturnRole() {
    super.findOneByIdShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql"})
  void findOneByExampleShouldReturnRole() {
    super.findOneByExampleShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql"})
  public void findOneShouldReturnNoResultError() {
    super.findOneShouldReturnNoResultError();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql"})
  void findAllShouldReturnRoles() {
    super.findAllShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql"})
  void findAllFilterShouldReturnRoles() {
    super.findAllFilterShouldReturnTypes();
  }
}
