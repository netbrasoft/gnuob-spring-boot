package br.com.netbrasoft.gnuob.authentication;

import static br.com.netbrasoft.gnuob.ApplicationConstants.INVALID_USERNAME_OR_PASSWORD;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.SAVE;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.SAVE_ALL;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeTopic;

@Transactional
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "stomp.authentication=false", "spring.mail.test-connection=false", "spring.liquibase.contexts:test"})
class UserServiceTests extends AbstractAuthenticationServiceTest<User> {
  static class UserTypeTopic extends TypeTopic<User> {
  }

  private final User NEW_USER = User.getInstance("bar@foo.com");
  private final Permission NEW_PERMISSION = Permission.getInstance();
  private final Component NEW_COMPONENT = Component.getInstance();
  private final Role NEW_ROLE = Role.getInstance();

  @Autowired(required = true)
  private IUserService<User> userService;

  @Override
  public User getType() {
    return NEW_USER;
  }

  @Override
  public ITypeService<User> getTypService() {
    return userService;
  }

  @Override
  public Type getTypePayloadType() {
    return UserTypeTopic.class;
  }

  @Override
  public String getTypeSimpleName() {
    return User.class.getSimpleName();
  }

  @BeforeEach
  public void setup() throws InterruptedException, ExecutionException, TimeoutException {
    super.setup();
    NEW_USER.setActive(true);
    NEW_USER.setChangePassword(true);
    NEW_USER.setCode("code");
    NEW_USER.setCookie("cookie");
    NEW_USER.setDescription("description");
    NEW_USER.setEmail("bar@foo.com");
    NEW_USER.setFirstName("Bar");
    NEW_USER.setLastName("Foo");
    NEW_USER.setUsername("bar@foo.com");
    NEW_USER.setId(0);
    NEW_USER.setMobile(true);
    NEW_USER.setMobileIpAddress("mobileIpAddress");
    NEW_USER.setMobileManufacturer("mobileManufacturer");
    NEW_USER.setMobileMaxDownload("mobileMaxDownload");
    NEW_USER.setMobileModel("mobileModel");
    NEW_USER.setMobileNetworkType("mobileNetworkType");
    NEW_USER.setMobilePackageName("mobilePackageName");
    NEW_USER.setMobilePlatform("mobilePlatform");
    NEW_USER.setMobileSerial("mobileSerial");
    NEW_USER.setMobileSignal("mobileSignal");
    NEW_USER.setMobileUuid("mobileUuid");
    NEW_USER.setMobileVersion("mobileVersion");
    NEW_USER.setMobileVersionCode("mobileVersionCode");
    NEW_USER.setMobileVersionNumber("mobileVersionNumber");
    NEW_USER.setName("Bar Foo");
    NEW_USER.setPassword("9083D070BA251593:1000:1A82A5073A126C2755904561B501F4A631C8681F");
    NEW_USER.setRoot(true);
    NEW_USER.setVersion(0);
    NEW_ROLE.setActive(true);
    NEW_ROLE.setCode("code");
    NEW_ROLE.setDescription("description");
    NEW_ROLE.setId(1000L);
    NEW_ROLE.setName("Bar Foo");
    NEW_ROLE.setVersion(0);
    NEW_PERMISSION.setActive(true);
    NEW_PERMISSION.setCode("code");
    NEW_PERMISSION.setDescription("description");
    NEW_PERMISSION.setId(1000L);
    NEW_PERMISSION.setName("Bar Foo");
    NEW_PERMISSION.setVersion(0);
    NEW_COMPONENT.setActive(true);
    NEW_COMPONENT.setCode("code");
    NEW_COMPONENT.setDescription("description");
    NEW_COMPONENT.setId(1000L);
    NEW_COMPONENT.setName("Bar Foo");
    NEW_COMPONENT.setVersion(0);
    NEW_USER.getUserRoles().add(UserRole.getInstance(NEW_ROLE));
    NEW_USER.getUserPermissions().add(UserPermission.getInstance(NEW_PERMISSION));
    NEW_USER.getUserPermissions().iterator().next().getUserComponents().add(UserComponent.getInstance(NEW_COMPONENT));
  }

  @AfterEach
  public void setDown() throws InterruptedException, ExecutionException {
    super.setDown();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql", "classpath:scripts/createPermission.sql", "classpath:scripts/createComponent.sql"})
  void saveCreateShouldReturnUser() throws InterruptedException, ExecutionException, TimeoutException {
    final User attachedUser = userService.save(NEW_USER);
    final TypeTopic<Permission> permissionTypeTopic = getTopicPermissionCompletableFuture().get(10, SECONDS);
    final TypeTopic<User> userTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    final TypeTopic<Role> roleTypeTopic = getTopicRoleCompletableFuture().get(10, SECONDS);
    assertThat(attachedUser).isEqualTo(NEW_USER);
    assertThat(attachedUser.getUserRoles().iterator().next().getRole()).isEqualTo(NEW_ROLE);
    assertThat(attachedUser.getUserPermissions().iterator().next().getPermission()).isEqualTo(NEW_PERMISSION);
    assertThat(attachedUser.getUserPermissions().iterator().next().getUserComponents().iterator().next().getComponent()).isEqualTo(NEW_COMPONENT);
    assertThat(attachedUser.getId()).isPositive();
    assertThat(attachedUser.getVersion()).isEqualTo(1);
    assertThat(attachedUser.getDtype()).isEqualTo(User.class.getSimpleName().toLowerCase());
    assertThat(attachedUser.getCreated()).isNotNull();
    assertThat(attachedUser.getModified()).isNotNull();
    assertThat(attachedUser.getUserCreated()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(userTypeTopic.getType()).isEqualTo(NEW_USER);
    assertThat(permissionTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(permissionTypeTopic.getTypes().get(0)).isEqualTo(NEW_PERMISSION);
    assertThat(permissionTypeTopic.getTypes().get(0).getPermissionUsers().iterator().next().getUser()).isEqualTo(NEW_USER);
    assertThat(roleTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(roleTypeTopic.getTypes().get(0)).isEqualTo(NEW_ROLE);
    assertThat(roleTypeTopic.getTypes().get(0).getRoleUsers().iterator().next().getUser()).isEqualTo(NEW_USER);
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql", "classpath:scripts/createRole.sql", "classpath:scripts/createPermission.sql", "classpath:scripts/createComponent.sql"})
  public void saveUpdateShouldReturnUser() throws InterruptedException, ExecutionException, TimeoutException {
    setupUser();
    NEW_USER.setId(1000L);
    NEW_USER.setVersion(1);
    NEW_USER.getUserRoles().clear();
    NEW_USER.getUserPermissions().clear();
    final User attachedUser = userService.save(NEW_USER);
    final TypeTopic<Permission> permissionTypeTopic = getTopicPermissionCompletableFuture().get(10, SECONDS);
    final TypeTopic<User> userTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    final TypeTopic<Role> roleTypeTopic = getTopicRoleCompletableFuture().get(10, SECONDS);
    assertThat(attachedUser).isEqualTo(NEW_USER);
    assertThat(attachedUser.getUserRoles().isEmpty()).isTrue();
    assertThat(attachedUser.getUserPermissions().isEmpty()).isTrue();
    assertThat(attachedUser.getId()).isPositive();
    assertThat(attachedUser.getVersion()).isEqualTo(2);
    assertThat(attachedUser.getDtype()).isEqualTo(User.class.getSimpleName().toLowerCase());
    assertThat(attachedUser.getCreated()).isNotNull();
    assertThat(attachedUser.getModified()).isNotNull();
    assertThat(attachedUser.getUserCreated()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(userTypeTopic.getType()).isEqualTo(NEW_USER);
    assertThat(permissionTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(permissionTypeTopic.getTypes().get(0)).isEqualTo(NEW_PERMISSION);
    assertThat(permissionTypeTopic.getTypes().get(0).getPermissionUsers().isEmpty()).isTrue();
    assertThat(roleTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(roleTypeTopic.getTypes().get(0)).isEqualTo(NEW_ROLE);
    assertThat(roleTypeTopic.getTypes().get(0).getRoleUsers().isEmpty()).isTrue();
  }

  private void setupUser() throws InterruptedException, ExecutionException, TimeoutException {
    setDown();
    NEW_USER.setId(1000L);
    userService.save(NEW_USER);
    setup();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql", "classpath:scripts/createRole.sql", "classpath:scripts/createPermission.sql", "classpath:scripts/createComponent.sql"})
  void newPasswordShouldReturnUser() throws InterruptedException, ExecutionException, TimeoutException {
    NEW_USER.setId(1000L);
    NEW_USER.setPassword("secret1");
    final User attachedUser = userService.save(NEW_USER);
    final TypeTopic<Permission> permissionTypeTopic = getTopicPermissionCompletableFuture().get(10, SECONDS);
    final TypeTopic<User> userTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    final TypeTopic<Role> roleTypeTopic = getTopicRoleCompletableFuture().get(10, SECONDS);
    assertThat(attachedUser).isEqualTo(NEW_USER);
    assertThat(attachedUser.getUserRoles().iterator().next().getRole()).isEqualTo(NEW_ROLE);
    assertThat(attachedUser.getUserPermissions().iterator().next().getPermission()).isEqualTo(NEW_PERMISSION);
    assertThat(attachedUser.getUserPermissions().iterator().next().getUserComponents().iterator().next().getComponent()).isEqualTo(NEW_COMPONENT);
    assertThat(attachedUser.getId()).isPositive();
    assertThat(attachedUser.getVersion()).isEqualTo(1);
    assertThat(attachedUser.getDtype()).isEqualTo(User.class.getSimpleName().toLowerCase());
    assertThat(attachedUser.getCreated()).isNotNull();
    assertThat(attachedUser.getModified()).isNotNull();
    assertThat(attachedUser.getUserCreated()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(attachedUser.getPassword()).isNotEqualTo("9083D070BA251593:1000:1A82A5073A126C2755904561B501F4A631C8681F");
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(userTypeTopic.getType()).isEqualTo(NEW_USER);
    assertThat(permissionTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(permissionTypeTopic.getTypes().get(0)).isEqualTo(NEW_PERMISSION);
    assertThat(permissionTypeTopic.getTypes().get(0).getPermissionUsers().iterator().next().getUser()).isEqualTo(NEW_USER);
    assertThat(roleTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(roleTypeTopic.getTypes().get(0)).isEqualTo(NEW_ROLE);
    assertThat(roleTypeTopic.getTypes().get(0).getRoleUsers().iterator().next().getUser()).isEqualTo(NEW_USER);
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql", "classpath:scripts/createRole.sql", "classpath:scripts/createPermission.sql", "classpath:scripts/createComponent.sql"})
  void emptyPasswordShouldReturnUser() throws InterruptedException, ExecutionException, TimeoutException {
    NEW_USER.setId(1000L);
    NEW_USER.setPassword(EMPTY);
    final User attachedUser = userService.save(NEW_USER);
    final TypeTopic<Permission> permissionTypeTopic = getTopicPermissionCompletableFuture().get(10, SECONDS);
    final TypeTopic<User> userTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    final TypeTopic<Role> roleTypeTopic = getTopicRoleCompletableFuture().get(10, SECONDS);
    assertThat(attachedUser).isEqualTo(NEW_USER);
    assertThat(attachedUser.getUserRoles().iterator().next().getRole()).isEqualTo(NEW_ROLE);
    assertThat(attachedUser.getUserPermissions().iterator().next().getPermission()).isEqualTo(NEW_PERMISSION);
    assertThat(attachedUser.getUserPermissions().iterator().next().getUserComponents().iterator().next().getComponent()).isEqualTo(NEW_COMPONENT);
    assertThat(attachedUser.getId()).isPositive();
    assertThat(attachedUser.getVersion()).isEqualTo(1);
    assertThat(attachedUser.getDtype()).isEqualTo(User.class.getSimpleName().toLowerCase());
    assertThat(attachedUser.getCreated()).isNotNull();
    assertThat(attachedUser.getModified()).isNotNull();
    assertThat(attachedUser.getUserCreated()).isNotNull();
    assertThat(attachedUser.getUserModified()).isNotNull();
    assertThat(attachedUser.getPassword()).isEqualTo("9083D070BA251593:1000:1A82A5073A126C2755904561B501F4A631C8681F");
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE);
    assertThat(userTypeTopic.getType()).isEqualTo(NEW_USER);
    assertThat(permissionTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(permissionTypeTopic.getTypes().get(0)).isEqualTo(NEW_PERMISSION);
    assertThat(permissionTypeTopic.getTypes().get(0).getPermissionUsers().iterator().next().getUser()).isEqualTo(NEW_USER);
    assertThat(roleTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(roleTypeTopic.getTypes().get(0)).isEqualTo(NEW_ROLE);
    assertThat(roleTypeTopic.getTypes().get(0).getRoleUsers().iterator().next().getUser()).isEqualTo(NEW_USER);
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createRole.sql", "classpath:scripts/createPermission.sql", "classpath:scripts/createComponent.sql"})
  void saveAllShouldReturnUsers() throws InterruptedException, ExecutionException, TimeoutException {
    final List<User> attachedUsers = userService.saveAll(newArrayList(NEW_USER));
    final TypeTopic<User> userTypeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(attachedUsers.get(0)).isEqualTo(NEW_USER);
    assertThat(attachedUsers.get(0).getUserRoles().iterator().next().getRole()).isEqualTo(NEW_ROLE);
    assertThat(attachedUsers.get(0).getUserPermissions().iterator().next().getPermission()).isEqualTo(NEW_PERMISSION);
    assertThat(attachedUsers.get(0).getUserPermissions().iterator().next().getUserComponents().iterator().next().getComponent()).isEqualTo(NEW_COMPONENT);
    assertThat(attachedUsers.get(0).getId()).isPositive();
    assertThat(attachedUsers.get(0).getVersion()).isEqualTo(1);
    assertThat(attachedUsers.get(0).getDtype()).isEqualTo(User.class.getSimpleName().toLowerCase());
    assertThat(attachedUsers.get(0).getCreated()).isNotNull();
    assertThat(attachedUsers.get(0).getModified()).isNotNull();
    assertThat(attachedUsers.get(0).getUserCreated()).isNotNull();
    assertThat(attachedUsers.get(0).getUserModified()).isNotNull();
    assertThat(userTypeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(userTypeTopic.getTypes().get(0)).isEqualTo(NEW_USER);
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  void deleteShouldNotReturnUser() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteShouldNotReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  void deleteAllShouldNotReturnUsers() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  void findOneByIdShouldReturnUser() {
    super.findOneByIdShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  void findOneByExampleShouldReturnUser() {
    super.findOneByExampleShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  public void findOneShouldReturnNoResultError() {
    super.findOneShouldReturnNoResultError();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  void findAllShouldReturnUsers() {
    super.findAllShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  void findAllFilterShouldReturnUsers() {
    super.findAllFilterShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  void loadUserByUsernameShouldReturnUser() {
    final UserDetails attachedUser = ((UserDetailsService) userService).loadUserByUsername(NEW_USER.getUsername());
    assertThat(attachedUser).isEqualTo(NEW_USER);
    assertThat(((User) attachedUser).getId()).isPositive();
    assertThat(((User) attachedUser).getVersion()).isZero();
    assertThat(((User) attachedUser).getDtype()).isEqualTo(User.class.getSimpleName().toLowerCase());
    assertThat(((User) attachedUser).getCreated()).isNotNull();
    assertThat(((User) attachedUser).getModified()).isNotNull();
    assertThat(((User) attachedUser).getUserCreated()).isNotNull();
    assertThat(((User) attachedUser).getUserModified()).isNotNull();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createUser.sql"})
  public void loadUserByUsernameShouldReturnInvalidUsernameOrPasswordError() {
    NEW_USER.setUsername("foo@bar.com");
    final Throwable thrown = assertThrows(UsernameNotFoundException.class, () -> ((UserDetailsService) userService).loadUserByUsername(NEW_USER.getUsername()));
    assertThat(thrown.getMessage()).isEqualTo(INVALID_USERNAME_OR_PASSWORD);
  }
}
