package br.com.netbrasoft.gnuob.authentication;

import static org.apache.commons.lang3.StringUtils.join;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UserRoleTests {
  private final static Role NEW_ROLE = Role.getInstance();
  private final static User NEW_USER = User.getInstance("bar@foo.com");
  private final static UserRole NEW_USER_ROLE = UserRole.getInstance(NEW_ROLE);

  @BeforeEach
  public void setup() {
    NEW_ROLE.setActive(true);
    NEW_ROLE.setCode("code");
    NEW_ROLE.setDescription("description");
    NEW_ROLE.setId(0);
    NEW_ROLE.setName("Bar Foo");
    NEW_ROLE.setVersion(0);
    NEW_USER.setActive(true);
    NEW_USER.setChangePassword(true);
    NEW_USER.setCode("code");
    NEW_USER.setCookie("cookie");
    NEW_USER.setDescription("description");
    NEW_USER.setEmail("bar@foo.com");
    NEW_USER.setId(1000L);
    NEW_USER.setMobile(true);
    NEW_USER.setMobileIpAddress("mobileIpAddress");
    NEW_USER.setMobileManufacturer("mobileManufacturer");
    NEW_USER.setMobileMaxDownload("mobileMaxDownload");
    NEW_USER.setMobileModel("mobileModel");
    NEW_USER.setMobileNetworkType("mobileNetworkType");
    NEW_USER.setMobilePackageName("mobilePackageName");
    NEW_USER.setMobilePlatform("mobilePlatform");
    NEW_USER.setMobileSerial("mobileSerial");
    NEW_USER.setMobileSignal("mobileSignal");
    NEW_USER.setMobileUuid("mobileUuid");
    NEW_USER.setMobileVersion("mobileVersion");
    NEW_USER.setMobileVersionCode("mobileVersionCode");
    NEW_USER.setMobileVersionNumber("mobileVersionNumber");
    NEW_USER.setName("Bar Foo");
    NEW_USER.setPassword("9083D070BA251593:1000:1A82A5073A126C2755904561B501F4A631C8681F");
    NEW_USER.setRoot(true);
    NEW_USER.setVersion(0);
  }

  @Test
  void testGetAuthorityt() {
    assertThat(NEW_USER_ROLE.getAuthority()).isEqualTo(join("ROLE_", NEW_ROLE.getCode()).toUpperCase());
  }
}
