package br.com.netbrasoft.gnuob.authentication;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UserTests {
  private final static User NEW_USER = User.getInstance("bar@foo.com");

  @BeforeEach
  public void setup() {
    NEW_USER.setActive(true);
    NEW_USER.setChangePassword(true);
    NEW_USER.setCode("code");
    NEW_USER.setCookie("cookie");
    NEW_USER.setDescription("description");
    NEW_USER.setEmail("bar@foo.com");
    NEW_USER.setId(0);
    NEW_USER.setMobile(true);
    NEW_USER.setMobileIpAddress("mobileIpAddress");
    NEW_USER.setMobileManufacturer("mobileManufacturer");
    NEW_USER.setMobileMaxDownload("mobileMaxDownload");
    NEW_USER.setMobileModel("mobileModel");
    NEW_USER.setMobileNetworkType("mobileNetworkType");
    NEW_USER.setMobilePackageName("mobilePackageName");
    NEW_USER.setMobilePlatform("mobilePlatform");
    NEW_USER.setMobileSerial("mobileSerial");
    NEW_USER.setMobileSignal("mobileSignal");
    NEW_USER.setMobileUuid("mobileUuid");
    NEW_USER.setMobileVersion("mobileVersion");
    NEW_USER.setMobileVersionCode("mobileVersionCode");
    NEW_USER.setMobileVersionNumber("mobileVersionNumber");
    NEW_USER.setName("Bar Foo");
    NEW_USER.setPassword("9083D070BA251593:1000:1A82A5073A126C2755904561B501F4A631C8681F");
    NEW_USER.setRoot(true);
    NEW_USER.setVersion(0);
  }

  @Test
  void testPrePersist() {
    NEW_USER.setActive(null);
    NEW_USER.setChangePassword(null);
    NEW_USER.setMobile(null);
    NEW_USER.setOnline(null);
    NEW_USER.setPassword(null);
    NEW_USER.setRoot(null);
    NEW_USER.prePersist();
    assertThat(NEW_USER.getChangePassword()).isTrue();
    assertThat(NEW_USER.getMobile()).isFalse();
    assertThat(NEW_USER.getOnline()).isFalse();
    assertThat(NEW_USER.getRoot()).isFalse();
    assertThat(NEW_USER.getActive()).isTrue();
    assertThat(NEW_USER.getPassword()).isNotBlank();
  }

  @Test
  void testPreUpdate() {
    NEW_USER.setActive(null);
    NEW_USER.setChangePassword(null);
    NEW_USER.setMobile(null);
    NEW_USER.setOnline(null);
    NEW_USER.setPassword(null);
    NEW_USER.setRoot(null);
    NEW_USER.preUpdate();
    assertThat(NEW_USER.getChangePassword()).isTrue();
    assertThat(NEW_USER.getMobile()).isFalse();
    assertThat(NEW_USER.getOnline()).isFalse();
    assertThat(NEW_USER.getRoot()).isFalse();
    assertThat(NEW_USER.getActive()).isTrue();
    assertThat(NEW_USER.getPassword()).isNotBlank();
  }
}
