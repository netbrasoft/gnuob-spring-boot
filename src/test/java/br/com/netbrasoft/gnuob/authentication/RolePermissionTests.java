package br.com.netbrasoft.gnuob.authentication;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RolePermissionTests {
  private final static RolePermission NEW_ROLE_PERMISSION = RolePermission.getInstance(null);

  @BeforeEach
  public void setup() {
    NEW_ROLE_PERMISSION.setId(0);
    NEW_ROLE_PERMISSION.setVersion(0);
  }

  @Test
  void testPrePersist() {
    NEW_ROLE_PERMISSION.setCanCreate(null);
    NEW_ROLE_PERMISSION.setCanRead(null);
    NEW_ROLE_PERMISSION.setCanUpdate(null);
    NEW_ROLE_PERMISSION.setCanDelete(null);
    NEW_ROLE_PERMISSION.prePersist();
    assertThat(NEW_ROLE_PERMISSION.getCanCreate()).isTrue();
    assertThat(NEW_ROLE_PERMISSION.getCanRead()).isTrue();
    assertThat(NEW_ROLE_PERMISSION.getCanUpdate()).isTrue();
    assertThat(NEW_ROLE_PERMISSION.getCanDelete()).isTrue();
  }

  @Test
  void testPreUpdate() {
    NEW_ROLE_PERMISSION.setCanCreate(null);
    NEW_ROLE_PERMISSION.setCanRead(null);
    NEW_ROLE_PERMISSION.setCanUpdate(null);
    NEW_ROLE_PERMISSION.setCanDelete(null);
    NEW_ROLE_PERMISSION.preUpdate();
    assertThat(NEW_ROLE_PERMISSION.getCanCreate()).isTrue();
    assertThat(NEW_ROLE_PERMISSION.getCanRead()).isTrue();
    assertThat(NEW_ROLE_PERMISSION.getCanUpdate()).isTrue();
    assertThat(NEW_ROLE_PERMISSION.getCanDelete()).isTrue();
  }
}
