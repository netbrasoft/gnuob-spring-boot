package br.com.netbrasoft.gnuob.category;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.lang.reflect.Type;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.transaction.Transactional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import br.com.netbrasoft.gnuob.type.AbstractTypeServiceTest;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeTopic;

@Transactional
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "stomp.authentication=false", "spring.mail.test-connection=false", "spring.liquibase.contexts:test"})
class CategoryServiceTest extends AbstractTypeServiceTest<Category> {
  static class CategoryTypeTopic extends TypeTopic<Category> {
  }

  @Autowired(required = true)
  private ITypeService<Category> categoryService;
  protected final Category NEW_CATEGORY = Category.getInstance();

  @Override
  public Category getType() {
    return NEW_CATEGORY;
  }

  @Override
  public ITypeService<Category> getTypService() {
    return categoryService;
  }

  @Override
  public Type getTypePayloadType() {
    return CategoryTypeTopic.class;
  }

  @Override
  public String getTypeSimpleName() {
    return Category.class.getSimpleName();
  }

  @BeforeEach
  public void setup() throws InterruptedException, ExecutionException, TimeoutException {
    super.setup();
    NEW_CATEGORY.setActive(true);
    NEW_CATEGORY.setCode("code");
    NEW_CATEGORY.setDescription("description");
    NEW_CATEGORY.setId(0);
    NEW_CATEGORY.setName("Bar Foo");
    NEW_CATEGORY.setVersion(0);
    NEW_CATEGORY.setParent(null);
  }

  @AfterEach
  public void setDown() throws InterruptedException, ExecutionException {
    super.setDown();
  }

  @Test
  void saveCreateShouldReturnCategory() throws InterruptedException, ExecutionException, TimeoutException {
    getType().setCode("Bar Foo");
    super.saveCreateShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createCategory.sql"})
  void saveUpdateShouldReturnCategory() throws InterruptedException, ExecutionException, TimeoutException {
    getType().setCode("Bar Foo");
    super.saveUpdateShouldReturnType();
  }

  @Test
  void saveAllShouldReturnCategories() throws InterruptedException, ExecutionException, TimeoutException {
    getType().setCode("Bar Foo");
    super.saveAllShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createCategory.sql"})
  void deleteShouldNotReturnCategory() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteShouldNotReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createCategory.sql"})
  void deleteAllShouldNotReturnCategories() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createCategory.sql"})
  void findOneByIdShouldReturnCategory() {
    super.findOneByIdShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createCategory.sql"})
  void findOneByExampleShouldReturnCategory() {
    super.findOneByExampleShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createCategory.sql"})
  public void findOneShouldReturnNoResultError() {
    super.findOneShouldReturnNoResultError();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createCategory.sql"})
  void findAllShouldReturnCategories() {
    super.findAllShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createCategory.sql"})
  void findAllFilterShouldReturnCategories() {
    super.findAllFilterShouldReturnTypes();
  }
}
