package br.com.netbrasoft.gnuob.category;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.com.netbrasoft.gnuob.type.AbstractTypeRestControllerTest;
import br.com.netbrasoft.gnuob.type.ITypeService;

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "spring.mail.test-connection=false"})
class CategoryRestControllerTest extends AbstractTypeRestControllerTest<Category> {
  private final static Category NEW_CATEGORY = Category.getInstance();
  @MockBean
  private CategoryService<Category> mockCategoryService;

  @Override
  @BeforeEach
  public void setup() {
    super.setup();
    NEW_CATEGORY.setActive(true);
    NEW_CATEGORY.setCode("code");
    NEW_CATEGORY.setDescription("description");
    NEW_CATEGORY.setId(0);
    NEW_CATEGORY.setName("Bar Foo");
    NEW_CATEGORY.setParent(null);
    NEW_CATEGORY.setVersion(0);
  }

  @Override
  @AfterEach
  public void setDown() {}

  @Override
  public Category getType() {
    return NEW_CATEGORY;
  }

  @Override
  public ITypeService<Category> getTypeMockService() {
    return (ITypeService<Category>) mockCategoryService;
  }

  @Override
  public Class<Category> getClassType() {
    return Category.class;
  }

  @Override
  public String getPath() {
    return "/category";
  }

  @Override
  public Class<Category[]> getClassTypes() {
    return Category[].class;
  }

  @Test
  void saveShouldReturnCategory() {
    super.saveShouldReturnType();
  }

  @Test
  public void wrongSaveShouldReturnServiceError() {
    super.wrongSaveShouldReturnServiceError();
  }

  @Test
  void saveAllShouldReturnCategorys() {
    super.saveAllShouldReturnTypes();
  }

  @Test
  public void wrongSaveAllShouldReturnServiceError() {
    super.wrongSaveAllShouldReturnServiceError();
  }

  @Test
  void deleteShouldNotReturnCategory() {
    super.deleteShouldNotReturnType();
  }

  @Test
  public void wrongDeleteShouldReturnServiceError() {
    super.wrongDeleteShouldReturnServiceError();
  }

  @Test
  void deleteAllShouldNotReturnCategories() {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  public void wrongDeleteAllShouldReturnServiceError() {
    super.wrongDeleteAllShouldReturnServiceError();
  }

  @Test
  void findOneShouldReturnCategory() {
    super.findOneShouldReturnType();
  }

  @Test
  public void wrongFindOneShouldReturnServiceError() {
    super.wrongFindOneShouldReturnServiceError();
  }

  @Test
  void findAllShouldReturnCategorys() {
    super.findAllShouldReturnTypes();
  }

  @Test
  public void wrongFindAllShouldReturnServiceError() {
    super.wrongFindAllShouldReturnServiceError();
  }

  @Test
  void findAllFilterShouldReturnCategorys() {
    super.findAllFilterShouldReturnTypes();
  }

  @Test
  public void wrongFindAllFilterShouldReturnServiceError() {
    super.wrongFindAllFilterShouldReturnServiceError();
  }
}
