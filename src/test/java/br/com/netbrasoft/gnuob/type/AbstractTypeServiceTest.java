package br.com.netbrasoft.gnuob.type;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import static com.icegreen.greenmail.util.ServerSetup.PROTOCOL_SMTP;
import static org.apache.commons.lang3.StringUtils.join;

public abstract class AbstractTypeServiceTest<T extends AbstractActiveType> implements ITypeServiceTest<T> {
  @LocalServerPort
  private int port;
  private GreenMail greenMail = new GreenMail(new ServerSetup(5025, "127.0.0.1", PROTOCOL_SMTP));
  private CompletableFuture<TypeTopic<T>> topicTypeCompletableFuture;
  private CompletableFuture<Boolean> sessionTypeCompletableFuture;
  private ListenableFuture<StompSession> topicStompSession;

  protected ListenableFuture<StompSession> createStompSession(final StompSessionHandler stompSessionHandler) {
    final WebSocketStompClient webSocketStompClient = new WebSocketStompClient(new StandardWebSocketClient());
    webSocketStompClient.setMessageConverter(new MappingJackson2MessageConverter());
    return webSocketStompClient.connect("ws://localhost:" + port + "/gs-guide-websocket", stompSessionHandler);
  }

  @Override
  public void setup() throws InterruptedException, ExecutionException, TimeoutException {
    greenMail.setUser("secret", "secret");
    greenMail.start();
    topicTypeCompletableFuture = new CompletableFuture<TypeTopic<T>>();
    sessionTypeCompletableFuture = new CompletableFuture<Boolean>();
    topicStompSession = createStompSession(createSettingStompSessionHandler());
    if (!sessionTypeCompletableFuture.get(10, SECONDS)) {
      throw new TimeoutException();
    }
  }

  public StompSessionHandler createSettingStompSessionHandler() {
    return new StompSessionHandlerAdapter() {

      @Override
      public Type getPayloadType(StompHeaders headers) {
        return getTypePayloadType();
      }

      @Override
      public void handleFrame(StompHeaders headers, Object payload) {
        topicTypeCompletableFuture.complete((TypeTopic<T>) payload);
      }

      @Override
      public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        session.subscribe(join("/topic/", getTypeSimpleName()).toLowerCase(), this);
        sessionTypeCompletableFuture.complete(session.isConnected());
      }
    };
  }

  @Override
  public void setDown() throws InterruptedException, ExecutionException {
    greenMail.stop();
    topicStompSession.get().disconnect();
  }

  @Override
  public CompletableFuture<TypeTopic<T>> getTopicTypeCompletableFuture() {
    return topicTypeCompletableFuture;
  }

  public abstract Type getTypePayloadType();

  public abstract String getTypeSimpleName();

  public GreenMail getGreenMail() {
    return greenMail;
  }
}
