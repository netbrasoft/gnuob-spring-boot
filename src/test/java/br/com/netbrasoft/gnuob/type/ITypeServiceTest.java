package br.com.netbrasoft.gnuob.type;

import static br.com.netbrasoft.gnuob.ApplicationConstants.NO_RESULTS_FOUND;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.DELETE;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.DELETE_ALL;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.SAVE;
import static br.com.netbrasoft.gnuob.type.TypeTopic.Action.SAVE_ALL;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;

public interface ITypeServiceTest<T extends AbstractActiveType> {

  public default void setup() throws InterruptedException, ExecutionException, TimeoutException {}

  public default void setDown() throws InterruptedException, ExecutionException {}

  public T getType();

  public ITypeService<T> getTypService();

  public default void saveCreateShouldReturnType() throws InterruptedException, ExecutionException, TimeoutException {
    final T type = getType();
    final T attachedType = getTypService().save(type);
    final TypeTopic<T> typeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(attachedType).isEqualTo(type);
    assertThat(attachedType.getId()).isGreaterThan(0L);
    assertThat(attachedType.getVersion()).isEqualTo(0);
    assertThat(attachedType.getDtype()).isEqualTo(type.getClass().getSimpleName().toLowerCase());
    assertThat(attachedType.getCreated()).isNotNull();
    assertThat(attachedType.getModified()).isNotNull();
    assertThat(attachedType.getUserCreated()).isNotNull();
    assertThat(attachedType.getUserModified()).isNotNull();
    assertThat(typeTopic.getAction()).isEqualTo(SAVE);
    assertThat(typeTopic.getType()).isEqualTo(type);
  }

  public default void saveUpdateShouldReturnType() throws InterruptedException, ExecutionException, TimeoutException {
    final T type = getType();
    type.setId(1000L);
    final T attachedType = getTypService().save(type);
    final TypeTopic<T> typeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(attachedType).isEqualTo(type);
    assertThat(attachedType.getId()).isGreaterThan(0L);
    assertThat(attachedType.getVersion()).isEqualTo(1);
    assertThat(attachedType.getDtype()).isEqualTo(type.getClass().getSimpleName().toLowerCase());
    assertThat(attachedType.getCreated()).isNotNull();
    assertThat(attachedType.getModified()).isNotNull();
    assertThat(attachedType.getUserCreated()).isNotNull();
    assertThat(attachedType.getUserModified()).isNotNull();
    assertThat(typeTopic.getAction()).isEqualTo(SAVE);
    assertThat(typeTopic.getType()).isEqualTo(type);
  }

  public default void saveAllShouldReturnTypes() throws InterruptedException, ExecutionException, TimeoutException {
    final T type = getType();
    final List<T> attachedTypes = getTypService().saveAll(newArrayList(type));
    final TypeTopic<T> typeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(attachedTypes.get(0)).isEqualTo(type);
    assertThat(attachedTypes.get(0).getId()).isGreaterThan(0L);
    assertThat(attachedTypes.get(0).getVersion()).isEqualTo(0);
    assertThat(attachedTypes.get(0).getDtype()).isEqualTo(type.getClass().getSimpleName().toLowerCase());
    assertThat(attachedTypes.get(0).getCreated()).isNotNull();
    assertThat(attachedTypes.get(0).getModified()).isNotNull();
    assertThat(attachedTypes.get(0).getUserCreated()).isNotNull();
    assertThat(attachedTypes.get(0).getUserModified()).isNotNull();
    assertThat(typeTopic.getAction()).isEqualTo(SAVE_ALL);
    assertThat(typeTopic.getTypes().get(0)).isEqualTo(type);
  }

  public default void deleteShouldNotReturnType() throws InterruptedException, ExecutionException, TimeoutException {
    final T type = getType();
    type.setId(1000L);
    getTypService().delete(type);
    final TypeTopic<T> typeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(typeTopic.getAction()).isEqualTo(DELETE);
    assertThat(typeTopic.getType()).isEqualTo(type);
  }

  public default void deleteAllShouldNotReturnTypes() throws InterruptedException, ExecutionException, TimeoutException {
    final T type = getType();
    type.setId(1000L);
    getTypService().deleteAll(newArrayList(type));
    final TypeTopic<T> typeTopic = getTopicTypeCompletableFuture().get(10, SECONDS);
    assertThat(typeTopic.getAction()).isEqualTo(DELETE_ALL);
    assertThat(typeTopic.getTypes().get(0)).isEqualTo(type);
  }

  public CompletableFuture<TypeTopic<T>> getTopicTypeCompletableFuture();

  public default void findOneByIdShouldReturnType() {
    final T type = getType();
    type.setId(1000L);
    final T attachedType = getTypService().findOne(type);
    assertThat(attachedType).isEqualTo(type);
    assertThat(attachedType.getId()).isGreaterThan(0L);
    assertThat(attachedType.getVersion()).isEqualTo(0);
    assertThat(attachedType.getDtype()).isEqualTo(type.getClass().getSimpleName().toLowerCase());
    assertThat(attachedType.getCreated()).isNotNull();
    assertThat(attachedType.getModified()).isNotNull();
    assertThat(attachedType.getUserCreated()).isNotNull();
    assertThat(attachedType.getUserModified()).isNotNull();
  }

  public default void findOneByExampleShouldReturnType() {
    final T type = getType();
    type.setId(0L);
    final T attachedType = getTypService().findOne(type);
    assertThat(attachedType).isEqualTo(type);
    assertThat(attachedType.getId()).isGreaterThan(0L);
    assertThat(attachedType.getVersion()).isEqualTo(0);
    assertThat(attachedType.getDtype()).isEqualTo(type.getClass().getSimpleName().toLowerCase());
    assertThat(attachedType.getCreated()).isNotNull();
    assertThat(attachedType.getModified()).isNotNull();
    assertThat(attachedType.getUserCreated()).isNotNull();
    assertThat(attachedType.getUserModified()).isNotNull();
  }

  public default void findOneShouldReturnNoResultError() {
    final T type = getType();
    type.setName("Wrong Name");
    Throwable thrown = assertThrows(TypeRuntimeException.class, () -> getTypService().findOne(type));
    assertThat(thrown.getMessage()).isEqualTo(NO_RESULTS_FOUND);
  }

  public default void findAllShouldReturnTypes() {
    final T type = getType();
    final Page<T> typePage = getTypService().findAll(0, 1, Direction.ASC, newArrayList("name"), type);
    assertThat(typePage.getContent().get(0)).isEqualTo(type);
    assertThat(typePage.getContent().get(0).getId()).isGreaterThan(0L);
    assertThat(typePage.getContent().get(0).getVersion()).isEqualTo(0);
    assertThat(typePage.getContent().get(0).getDtype()).isEqualTo(type.getClass().getSimpleName().toLowerCase());
    assertThat(typePage.getContent().get(0).getCreated()).isNotNull();
    assertThat(typePage.getContent().get(0).getModified()).isNotNull();
    assertThat(typePage.getContent().get(0).getUserCreated()).isNotNull();
    assertThat(typePage.getContent().get(0).getUserModified()).isNotNull();
  }

  public default void findAllFilterShouldReturnTypes() {
    final T type = getType();
    final TypeFilter<T> filter = new TypeFilter<T>();
    filter.setFrom(new Date(System.currentTimeMillis() - 60_000L));
    filter.setTo(new Date(System.currentTimeMillis() + 60_000L));
    filter.setType(type);
    final Page<T> typePage = getTypService().findAll(0, 1, Direction.ASC, newArrayList("name"), filter);
    assertThat(typePage.getContent().get(0)).isEqualTo(type);
    assertThat(typePage.getContent().get(0).getId()).isGreaterThan(0L);
    assertThat(typePage.getContent().get(0).getVersion()).isEqualTo(0);
    assertThat(typePage.getContent().get(0).getDtype()).isEqualTo(type.getClass().getSimpleName().toLowerCase());
    assertThat(typePage.getContent().get(0).getCreated()).isNotNull();
    assertThat(typePage.getContent().get(0).getModified()).isNotNull();
    assertThat(typePage.getContent().get(0).getUserCreated()).isNotNull();
    assertThat(typePage.getContent().get(0).getUserModified()).isNotNull();
  }
}
