package br.com.netbrasoft.gnuob.type;

import static br.com.netbrasoft.gnuob.ApplicationConstants.AUTHORIZATION;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_ALL;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_FILTER;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_OF;
import static br.com.netbrasoft.gnuob.ApplicationConstants.PATH_ONE;
import static br.com.netbrasoft.gnuob.ApplicationConstants.X_TOTAL_COUNT;
import static br.com.netbrasoft.gnuob.Utils.createToken;
import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.StringUtils.join;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpMethod.DELETE;

import java.util.Date;
import java.util.List;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import br.com.netbrasoft.gnuob.authentication.User;
import br.com.netbrasoft.gnuob.authentication.UserService;
import de.rtner.security.auth.spi.SimplePBKDF2;

public interface ITypeRestControllerTest<T extends AbstractType> {
  final static User AUTHENTICATED_USER = User.getInstance("foo@bar.com");

  public default void setup() {
    AUTHENTICATED_USER.setActive(true);
    AUTHENTICATED_USER.setChangePassword(true);
    AUTHENTICATED_USER.setCode("code");
    AUTHENTICATED_USER.setCookie("cookie");
    AUTHENTICATED_USER.setDescription("description");
    AUTHENTICATED_USER.setEmail("foo@bar.com");
    AUTHENTICATED_USER.setId(0);
    AUTHENTICATED_USER.setMobile(true);
    AUTHENTICATED_USER.setMobileIpAddress("mobileIpAddress");
    AUTHENTICATED_USER.setMobileManufacturer("mobileManufacturer");
    AUTHENTICATED_USER.setMobileMaxDownload("mobileMaxDownload");
    AUTHENTICATED_USER.setMobileModel("mobileModel");
    AUTHENTICATED_USER.setMobileNetworkType("mobileNetworkType");
    AUTHENTICATED_USER.setMobilePackageName("mobilePackageName");
    AUTHENTICATED_USER.setMobilePlatform("mobilePlatform");
    AUTHENTICATED_USER.setMobileSerial("mobileSerial");
    AUTHENTICATED_USER.setMobileSignal("mobileSignal");
    AUTHENTICATED_USER.setMobileUuid("mobileUuid");
    AUTHENTICATED_USER.setMobileVersion("mobileVersion");
    AUTHENTICATED_USER.setMobileVersionCode("mobileVersionCode");
    AUTHENTICATED_USER.setMobileVersionNumber("mobileVersionNumber");
    AUTHENTICATED_USER.setName("Foo Bar");
    AUTHENTICATED_USER.setPassword(new SimplePBKDF2().deriveKeyFormatted("secret"));
    AUTHENTICATED_USER.setRoot(true);
    AUTHENTICATED_USER.setVersion(0);
    when(((UserDetailsService) getMockUserService()).loadUserByUsername("foo@bar.com")).thenReturn(AUTHENTICATED_USER);
  }

  public UserService<User> getMockUserService();

  public default void setDown() {}

  public default void saveShouldReturnType() {
    final T type = getType();
    when(getTypeMockService().save(type)).thenReturn(type);
    final ResponseEntity<T> responseEntity = getRestTemplate().postForEntity(createPath(), createHttpEntity(type, createToken("foo@bar.com")), getClassType());
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isEqualTo(type);
    verify(getTypeMockService(), times(1)).save(type);
  }

  public T getType();

  public TestRestTemplate getRestTemplate();

  private String createPath() {
    return join("http://localhost:", getPort(), getPath());
  }

  public int getPort();

  public String getPath();

  private HttpEntity<T> createHttpEntity(final T type, final String token) {
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    httpHeaders.add(AUTHORIZATION, token);
    return new HttpEntity<T>(type, httpHeaders);
  }

  public Class<T> getClassType();

  public ITypeService<T> getTypeMockService();

  public default void wrongSaveShouldReturnServiceError() {
    final T type = getType();
    when(getTypeMockService().save(type)).thenThrow(new RuntimeException());
    final ResponseEntity<TypeRuntimeException> responseEntity = getRestTemplate().postForEntity(createPath(), createHttpEntity(type, createToken("foo@bar.com")), TypeRuntimeException.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(500);
    verify(getTypeMockService(), times(1)).save(type);
  }

  public default void saveAllShouldReturnTypes() {
    final T type = getType();
    when(getTypeMockService().saveAll(newArrayList(type))).thenReturn(newArrayList(type));
    final ResponseEntity<T[]> responseEntity = getRestTemplate().postForEntity(join(createPath(), PATH_ALL), createHttpEntity(newArrayList(type), createToken("foo@bar.com")), getClassTypes());
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()[0]).isEqualTo(type);
    verify(getTypeMockService(), times(1)).saveAll(newArrayList(type));
  }

  private HttpEntity<List<T>> createHttpEntity(final List<T> types, final String token) {
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    httpHeaders.add(AUTHORIZATION, token);
    return new HttpEntity<List<T>>(types, httpHeaders);
  }

  public abstract Class<T[]> getClassTypes();

  public default void wrongSaveAllShouldReturnServiceError() {
    final T type = getType();
    when(getTypeMockService().saveAll(newArrayList(type))).thenThrow(new RuntimeException());
    final ResponseEntity<TypeRuntimeException> responseEntity = getRestTemplate().postForEntity(join(createPath(), PATH_ALL), createHttpEntity(newArrayList(type), createToken("foo@bar.com")), TypeRuntimeException.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(500);
    verify(getTypeMockService(), times(1)).saveAll(newArrayList(type));
  }

  public default void deleteShouldNotReturnType() {
    final T type = getType();
    final ResponseEntity<Void> responseEntity = getRestTemplate().exchange(createPath(), DELETE, createHttpEntity(type, createToken("foo@bar.com")), Void.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isNull();
    verify(getTypeMockService(), times(1)).delete(type);
  }

  public default void wrongDeleteShouldReturnServiceError() {
    final T type = getType();
    doThrow(new RuntimeException()).when(getTypeMockService()).delete(type);
    final ResponseEntity<TypeRuntimeException> responseEntity = getRestTemplate().exchange(createPath(), DELETE, createHttpEntity(type, createToken("foo@bar.com")), TypeRuntimeException.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(500);
    verify(getTypeMockService(), times(1)).delete(type);
  }

  public default void deleteAllShouldNotReturnTypes() {
    final T type = getType();
    final ResponseEntity<Void> responseEntity = getRestTemplate().exchange(join(createPath(), PATH_ALL), DELETE, createHttpEntity(newArrayList(type), createToken("foo@bar.com")), Void.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isNull();
    verify(getTypeMockService(), times(1)).deleteAll(newArrayList(type));
  }

  public default void wrongDeleteAllShouldReturnServiceError() {
    final T type = getType();
    doThrow(new RuntimeException()).when(getTypeMockService()).deleteAll(newArrayList(type));
    final ResponseEntity<TypeRuntimeException> responseEntity = getRestTemplate().exchange(join(createPath(), PATH_ALL), DELETE, createHttpEntity(newArrayList(type), createToken("foo@bar.com")), TypeRuntimeException.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(500);
    verify(getTypeMockService(), times(1)).deleteAll(newArrayList(type));
  }

  public default void findOneShouldReturnType() {
    final T type = getType();
    when(getTypeMockService().findOne(type)).thenReturn(type);
    final ResponseEntity<T> responseEntity = getRestTemplate().postForEntity(join(createPath(), PATH_OF, PATH_ONE), createHttpEntity(type, createToken("foo@bar.com")), getClassType());
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()).isEqualTo(type);
    verify(getTypeMockService(), times(1)).findOne(type);
  }

  public default void wrongFindOneShouldReturnServiceError() {
    final T type = getType();
    when(getTypeMockService().findOne(type)).thenThrow(new RuntimeException());
    final ResponseEntity<TypeRuntimeException> responseEntity = getRestTemplate().postForEntity(join(createPath(), PATH_OF, PATH_ONE), createHttpEntity(type, createToken("foo@bar.com")), TypeRuntimeException.class);
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(500);
    verify(getTypeMockService(), times(1)).findOne(type);
  }

  public default void findAllShouldReturnTypes() {
    final T type = getType();
    when(getTypeMockService().findAll(0, 1, Direction.ASC, newArrayList("name"), type)).thenReturn(getPage());
    when(getPage().getTotalElements()).thenReturn(1L);
    when(getPage().getContent()).thenReturn(newArrayList(type));
    final ResponseEntity<T[]> responseEntity = getRestTemplate().postForEntity(join(createPath(), PATH_OF, PATH_ALL, "?page={page}&size={size}&direction={direction}&properties={properties}"), createHttpEntity(type, createToken("foo@bar.com")), getClassTypes(), "0", "1", "ASC", "name");
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()[0]).isEqualTo(type);
    assertThat(responseEntity.getHeaders().get(X_TOTAL_COUNT).get(0)).isEqualTo("1");
    verify(getTypeMockService(), times(1)).findAll(0, 1, Direction.ASC, newArrayList("name"), type);
  }

  public Page<T> getPage();

  public default void wrongFindAllShouldReturnServiceError() {
    final T type = getType();
    when(getTypeMockService().findAll(0, 1, Direction.ASC, newArrayList("name"), type)).thenThrow(new RuntimeException());
    when(getPage().getTotalElements()).thenReturn(1L);
    when(getPage().getContent()).thenReturn(newArrayList(type));
    final ResponseEntity<TypeRuntimeException> responseEntity = getRestTemplate().postForEntity(join(createPath(), PATH_OF, PATH_ALL, "?page={page}&size={size}&direction={direction}&properties={properties}"), createHttpEntity(type, createToken("foo@bar.com")), TypeRuntimeException.class, "0", "1", "ASC", "name");
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(500);
    verify(getTypeMockService(), times(1)).findAll(0, 1, Direction.ASC, newArrayList("name"), type);
  }

  public default void findAllFilterShouldReturnTypes() {
    final T type = getType();
    final TypeFilter<T> filter = new TypeFilter<T>();
    filter.setFrom(new Date(1_577_847_599_000L));
    filter.setTo(new Date(1_609_469_999_000L));
    filter.setType(type);
    when(getTypeMockService().findAll(0, 1, Direction.ASC, newArrayList("name"), filter)).thenReturn(getPage());
    when(getPage().getTotalElements()).thenReturn(1L);
    when(getPage().getContent()).thenReturn(newArrayList(type));
    final ResponseEntity<T[]> responseEntity = getRestTemplate().postForEntity(join(createPath(), PATH_OF, PATH_ALL, PATH_FILTER, "?page={page}&size={size}&direction={direction}&properties={properties}"), createHttpEntity(filter, createToken("foo@bar.com")), getClassTypes(), "0", "1", "ASC", "name");
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    assertThat(responseEntity.getBody()[0]).isEqualTo(type);
    assertThat(responseEntity.getHeaders().get(X_TOTAL_COUNT).get(0)).isEqualTo("1");
    verify(getTypeMockService(), times(1)).findAll(0, 1, Direction.ASC, newArrayList("name"), filter);
  }

  private HttpEntity<TypeFilter<T>> createHttpEntity(final TypeFilter<T> filter, final String token) {
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    httpHeaders.add(AUTHORIZATION, token);
    return new HttpEntity<TypeFilter<T>>(filter, httpHeaders);
  }

  public default void wrongFindAllFilterShouldReturnServiceError() {
    final T type = getType();
    final TypeFilter<T> filter = new TypeFilter<T>();
    filter.setFrom(new Date(1_577_847_599_000L));
    filter.setTo(new Date(1_609_469_999_000L));
    filter.setType(type);
    when(getTypeMockService().findAll(0, 1, Direction.ASC, newArrayList("name"), filter)).thenThrow(new RuntimeException());
    when(getPage().getTotalElements()).thenReturn(1L);
    when(getPage().getContent()).thenReturn(newArrayList(type));
    final ResponseEntity<TypeRuntimeException> responseEntity = getRestTemplate().postForEntity(join(createPath(), PATH_OF, PATH_ALL, PATH_FILTER, "?page={page}&size={size}&direction={direction}&properties={properties}"), createHttpEntity(filter, createToken("foo@bar.com")), TypeRuntimeException.class, "0", "1", "ASC", "name");
    assertThat(responseEntity.getStatusCodeValue()).isEqualTo(500);
    verify(getTypeMockService(), times(1)).findAll(0, 1, Direction.ASC, newArrayList("name"), filter);
  }
}
