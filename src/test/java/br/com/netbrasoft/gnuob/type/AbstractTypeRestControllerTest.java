package br.com.netbrasoft.gnuob.type;

import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.domain.Page;

import br.com.netbrasoft.gnuob.authentication.User;
import br.com.netbrasoft.gnuob.authentication.UserService;

public abstract class AbstractTypeRestControllerTest<T extends AbstractType> implements ITypeRestControllerTest<T> {
  @LocalServerPort
  private int port;
  @Autowired(required = true)
  private TestRestTemplate restTemplate;
  @MockBean
  private UserService<User> mockUserService;
  @Mock
  private Page<T> page;

  @Override
  public UserService<User> getMockUserService() {
    return mockUserService;
  }

  @Override
  public TestRestTemplate getRestTemplate() {
    return restTemplate;
  }

  @Override
  public int getPort() {
    return port;
  }

  @Override
  public Page<T> getPage() {
    return page;
  }
}
