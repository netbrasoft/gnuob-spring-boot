package br.com.netbrasoft.gnuob.setting;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import br.com.netbrasoft.gnuob.setting.Setting.Type;
import br.com.netbrasoft.gnuob.type.AbstractTypeRestControllerTest;
import br.com.netbrasoft.gnuob.type.ITypeService;

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "spring.mail.test-connection=false"})
class SettingRestControllerTest extends AbstractTypeRestControllerTest<Setting> {
  private final static Setting NEW_SETTING = Setting.getInstance();
  @MockBean
  private SettingService<Setting> mockSettingService;

  @Override
  @BeforeEach
  public void setup() {
    super.setup();
    NEW_SETTING.setActive(true);
    NEW_SETTING.setCode("code");
    NEW_SETTING.setDescription("description");
    NEW_SETTING.setId(0);
    NEW_SETTING.setName("Bar Foo");
    NEW_SETTING.setValue("value");
    NEW_SETTING.setType(Type.STRING);
    NEW_SETTING.setVersion(0);
  }

  @Override
  @AfterEach
  public void setDown() {
    super.setDown();
  }

  @Override
  public Setting getType() {
    return NEW_SETTING;
  }

  @Override
  public ITypeService<Setting> getTypeMockService() {
    return (ITypeService<Setting>) mockSettingService;
  }

  @Override
  public Class<Setting> getClassType() {
    return Setting.class;
  }

  @Override
  public String getPath() {
    return "/setting";
  }

  @Override
  public Class<Setting[]> getClassTypes() {
    return Setting[].class;
  }

  @Test
  void saveShouldReturnSetting() {
    super.saveShouldReturnType();
  }

  @Test
  public void wrongSaveShouldReturnServiceError() {
    super.wrongSaveShouldReturnServiceError();
  }

  @Test
  void saveAllShouldReturnSettings() {
    super.saveAllShouldReturnTypes();
  }

  @Test
  public void wrongSaveAllShouldReturnServiceError() {
    super.wrongSaveAllShouldReturnServiceError();
  }

  @Test
  void deleteShouldNotReturnSetting() {
    super.deleteShouldNotReturnType();
  }

  @Test
  public void wrongDeleteShouldReturnServiceError() {
    super.wrongDeleteShouldReturnServiceError();
  }

  @Test
  void deleteAllShouldNotReturnSettings() {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  public void wrongDeleteAllShouldReturnServiceError() {
    super.wrongDeleteAllShouldReturnServiceError();
  }

  @Test
  void findOneShouldReturnSetting() {
    super.findOneShouldReturnType();
  }

  @Test
  public void wrongFindOneShouldReturnServiceError() {
    super.wrongFindOneShouldReturnServiceError();
  }

  @Test
  void findAllShouldReturnSettings() {
    super.findAllShouldReturnTypes();
  }

  @Test
  public void wrongFindAllShouldReturnServiceError() {
    super.wrongFindAllShouldReturnServiceError();
  }

  @Test
  void findAllFilterShouldReturnSettings() {
    super.findAllFilterShouldReturnTypes();
  }

  @Test
  public void wrongFindAllFilterShouldReturnServiceError() {
    super.wrongFindAllFilterShouldReturnServiceError();
  }
}
