package br.com.netbrasoft.gnuob.setting;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.lang.reflect.Type;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.transaction.Transactional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import br.com.netbrasoft.gnuob.type.AbstractTypeServiceTest;
import br.com.netbrasoft.gnuob.type.ITypeService;
import br.com.netbrasoft.gnuob.type.TypeTopic;

@Transactional
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {"spring.cache.type=none", "stomp.authentication=false", "spring.mail.test-connection=false", "spring.liquibase.contexts:test"})
class SettingServiceTest extends AbstractTypeServiceTest<Setting> {
  protected final Setting NEW_SETTING = Setting.getInstance();
  @Autowired(required = true)
  private ITypeService<Setting> settingService;

  static class SettingTypeTopic extends TypeTopic<Setting> {
  }

  @Override
  public Setting getType() {
    return NEW_SETTING;
  }

  @Override
  public ITypeService<Setting> getTypService() {
    return settingService;
  }

  @Override
  public Type getTypePayloadType() {
    return SettingTypeTopic.class;
  }

  @Override
  public String getTypeSimpleName() {
    return Setting.class.getSimpleName();
  }

  @BeforeEach
  public void setup() throws InterruptedException, ExecutionException, TimeoutException {
    super.setup();
    NEW_SETTING.setActive(true);
    NEW_SETTING.setCode("code");
    NEW_SETTING.setDescription("description");
    NEW_SETTING.setId(0);
    NEW_SETTING.setName("Bar Foo");
    NEW_SETTING.setVersion(0);
    NEW_SETTING.setValue("value");
    NEW_SETTING.setType(Setting.Type.STRING);
  }

  @AfterEach
  public void setDown() throws InterruptedException, ExecutionException {
    super.setDown();
  }

  @Test
  void saveCreateShouldReturnSetting() throws InterruptedException, ExecutionException, TimeoutException {
    super.saveCreateShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createSetting.sql"})
  void saveUpdateShouldReturnSetting() throws InterruptedException, ExecutionException, TimeoutException {
    super.saveUpdateShouldReturnType();
  }

  @Test
  void saveAllShouldReturnSettings() throws InterruptedException, ExecutionException, TimeoutException {
    super.saveAllShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createSetting.sql"})
  void deleteShouldNotReturnSetting() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteShouldNotReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createSetting.sql"})
  void deleteAllShouldNotReturnSettings() throws InterruptedException, ExecutionException, TimeoutException {
    super.deleteAllShouldNotReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createSetting.sql"})
  void findOneByIdShouldReturnSetting() {
    super.findOneByIdShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createSetting.sql"})
  void findOneByExampleShouldReturnSetting() {
    super.findOneByExampleShouldReturnType();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createSetting.sql"})
  public void findOneShouldReturnNoResultError() {
    super.findOneShouldReturnNoResultError();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createSetting.sql"})
  void findAllShouldReturnSettings() {
    super.findAllShouldReturnTypes();
  }

  @Test
  @Sql(scripts = {"classpath:scripts/createSetting.sql"})
  void findAllFilterShouldReturnSettings() {
    super.findAllFilterShouldReturnTypes();
  }
}
