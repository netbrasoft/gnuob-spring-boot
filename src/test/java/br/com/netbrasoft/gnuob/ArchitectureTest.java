package br.com.netbrasoft.gnuob;

import static br.com.netbrasoft.gnuob.ApplicationConstants.BR_COM_NETBRASOFT_GNUOB;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchRule;

import org.junit.jupiter.api.Test;

public class ArchitectureTest {

  // @Test
  void some_architecture_rule() {
    JavaClasses importedClasses = new ClassFileImporter().importPackages(BR_COM_NETBRASOFT_GNUOB);

    ArchRule rule = classes().that().resideInAPackage("..foo..").should().onlyHaveDependentClassesThat().resideInAnyPackage("..source.one..", "..foo..");

    rule.check(importedClasses);
  }

  void layeredArchitectureRule() {
    // layeredArchitecture().layer("Controller").definedBy(packageIdentifiers)
  }

}
